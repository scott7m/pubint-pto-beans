package com.pubint.pto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table(name = "requestType")
@XmlRootElement(name = "requestType")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoRequestType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "requestTypeDescription")
	private String description;

	@Column(name = "requestTypeShortDescription")
	private String shortDescription;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "timeTypeID")
	private PtoTimeType timeType;

	@Column(name = "requireComment")
	private Boolean requireComment;

	@Column(name = "specialRouting")
	private Boolean specialRouting;

	// @Column(name = "unpaid")
	// private Boolean unpaidTime;

	@Column(name = "hrOnly")
	private Boolean hrOnly;

	public PtoRequestType() {
		super();
	}

	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShortDescription() {
		if (shortDescription == null) {
			return description;
		}

		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public PtoTimeType getTimeType() {
		return timeType;
	}

	public void setTimeType(PtoTimeType timeType) {
		this.timeType = timeType;
	}

	public PtoTimeTypeCategory getTimeTypeCategory() {
		if (timeType == null) {
			return null;
		}

		return timeType.getTimeTypeCategory();
	}

	public Long getTimeTypeCategoryID() {
		if (getTimeTypeCategory() == null) {
			return null;
		}

		return getTimeTypeCategory().getId();
	}

	public Boolean getRequireComment() {
		return requireComment;
	}

	public void setRequireComment(Boolean requireComment) {
		this.requireComment = requireComment;
	}

	public Boolean getSpecialRouting() {
		return specialRouting;
	}

	public void setSpecialRouting(Boolean specialRouting) {
		this.specialRouting = specialRouting;
	}

	// public Boolean isUnpaidTime() {
	// return unpaidTime;
	// }
	//
	// public void setUnpaidTime(Boolean unpaidTime) {
	// this.unpaidTime = unpaidTime;
	// }

	public Boolean isHrOnly() {
		return hrOnly;
	}

	public void setHrOnly(Boolean hrOnly) {
		this.hrOnly = hrOnly;
	}
}
