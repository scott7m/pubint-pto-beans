package com.pubint.pto.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.openjpa.persistence.FetchAttribute;
import org.apache.openjpa.persistence.FetchGroup;
import org.apache.openjpa.persistence.FetchGroups;
import org.apache.openjpa.persistence.jdbc.ElementJoinColumn;

@Entity
@Table(name = "calendar")
@FetchGroups({
				@FetchGroup(name = "ptoCalendar.base", attributes = {
																		@FetchAttribute(name = "years")
				})
})
@XmlRootElement(name = "calendar")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoCalendar implements Serializable {
	private static final long serialVersionUID = 3L;

	private static Logger log = Logger.getLogger("PtoCalendar");

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "calendarName")
	private String calendarName;

	@OneToMany(cascade = {
							CascadeType.PERSIST
	}, fetch = FetchType.EAGER)
	@OrderBy("calendarYear asc")
	@ElementJoinColumn(name = "calendarID", referencedColumnName = "id")
	List<PtoCalendarYear> years;

	public PtoCalendar() {
		super();

		years = new ArrayList<PtoCalendarYear>();

		log.setLevel(Level.INFO);
	}

	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCalendarName() {
		return calendarName;
	}

	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}

	@XmlElementWrapper(name = "years")
	@XmlElement(name = "year")
	public List<PtoCalendarYear> getYears() {
		return years;
	}

	public void setYears(List<PtoCalendarYear> years) {
		this.years = years;
	}

	public void addYear(PtoCalendarYear year) {
		if ((years == null) || (years.isEmpty())) {
			years = new ArrayList<PtoCalendarYear>();
		}

		years.add(year);
	}

	public void dropYear(PtoCalendarYear year) {
		if (years.contains(year)) {
			years.remove(year);
		}
	}

	public PtoCalendarYear getPtoYear(int year) {
		log.trace("Attempting to get year: " + year);

		for (PtoCalendarYear ptoYear : this.getYears()) {
			if (ptoYear.getCalendarYear() == year) {
				return ptoYear;
			}
		}

		return null;
	}
}
