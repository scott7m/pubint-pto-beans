package com.pubint.pto.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.openjpa.persistence.FetchAttribute;
import org.apache.openjpa.persistence.FetchGroup;
import org.apache.openjpa.persistence.FetchGroups;

import com.pubint.pto.base.XmlDateAdapter;

@Entity
@Table(name = "employeeHistory")
@FetchGroups({
				@FetchGroup(name = "ptoEmployeeHistory.base", attributes = {
					@FetchAttribute(name = "employee"),
					@FetchAttribute(name = "department"),
					@FetchAttribute(name = "eventType"),
					@FetchAttribute(name = "grantor")
				})
})
@XmlRootElement(name = "employeeHistory")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoEmployeeHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "departmentID")
	private PtoDepartment department;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "eventTypeID")
	private PtoEmployeeEventType eventType;

	@Column(name = "eventDate")
	private Date eventDate;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "grantorID")
	private PtoEmployee grantor;

	@Column(name = "comment")
	private String comment;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employeeID")
	private PtoEmployee employee;

	public PtoEmployeeHistory() {
		super();
	}

	public String toString() {
		return "" +
			"Employee ID: " + id + "\n" +
			"Department: " + getDepartmentName() + "\n" +
			"Event Type: " + getEventTypeName() + "\n" +
			"Event Date: " + eventDate + "\n" +
			"Grantor: " + getGrantorName() + "\n" +
			"Comment: " + comment;
	}

	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlElement
	public PtoDepartment getDepartment() {
		return department;
	}

	public void setDepartment(PtoDepartment department) {
		this.department = department;
	}

	@XmlElement
	public String getDepartmentName() {
		if (department == null) {
			return null;
		}

		return department.getDepartmentName();
	}

	@XmlElement
	public PtoEmployeeEventType getEventType() {
		return eventType;
	}

	public void setEventType(PtoEmployeeEventType eventType) {
		this.eventType = eventType;
	}

	@XmlElement
	public String getEventTypeName() {
		if (eventType == null) {
			return null;
		}

		return eventType.getEventType();
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	@XmlTransient
	public PtoEmployee getGrantor() {
		return grantor;
	}

	public void setGrantor(PtoEmployee grantor) {
		this.grantor = grantor;
	}

	@XmlElement(name = "grantorID")
	public Long getGrantorID() {
		if (grantor == null) {
			return null;
		}

		return grantor.getId();
	}

	@XmlElement(name = "grantorName")
	public String getGrantorName() {
		if (grantor == null) {
			return null;
		}

		return grantor.getFullName();
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@XmlTransient
	public PtoEmployee getEmployee() {
		return employee;
	}

	public void setEmployee(PtoEmployee employee) {
		this.employee = employee;
	}

	@XmlElement(name = "employeeID")
	public Long getEmployeeID() {
		if (employee == null) {
			return null;
		}

		return employee.getId();
	}

	@XmlElement(name = "employeeName")
	public String getEmployeeName() {
		if (employee == null) {
			return null;
		}

		return employee.getFullName();
	}
}
