package com.pubint.pto.entity;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.openjpa.persistence.jdbc.ElementJoinColumn;

@Entity
@Table(name = "notificationTemplate")
@XmlRootElement(name = "noteTemplate")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class NotificationTemplate {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "notificationKey")
	private String key;

	@Column(name = "title")
	private String title;

	@Column(name = "template")
	private String text;

	@OneToMany(cascade = {
							CascadeType.PERSIST
	}, fetch = FetchType.EAGER)
	@ElementJoinColumn(name = "templateID", referencedColumnName = "id")
	private Collection<NotificationRecipient> recipients = new ArrayList<NotificationRecipient>();

	public NotificationTemplate() {
		super();
	}

	public NotificationTemplate(String key, String text) {
		this();

		this.key = key;
		this.text = text;
	}

	@XmlAttribute(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlElement(name = "key")
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@XmlElement(name = "title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@XmlElement(name = "text")
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Collection<NotificationRecipient> getRecipients() {
		return recipients;
	}

	public void setRecipients(Collection<NotificationRecipient> recipients) {
		this.recipients = recipients;
	}

	public void addRecipient(NotificationRecipient recipient) {
		if (! recipients.contains(recipient)) {
			recipients.add(recipient);
		}
	}

	public void dropRecipient(NotificationRecipient recipient) {
		if (recipients.contains(recipient)) {
			recipients.remove(recipient);
		}
	}

	@Override
	public boolean equals(Object object) {
		if (! (object instanceof NotificationTemplate)) {
			return false;
		}

		NotificationTemplate template = (NotificationTemplate) object;

		if (! template.getKey().equals(getKey())) {
			return false;
		}

		return true;
	}
}
