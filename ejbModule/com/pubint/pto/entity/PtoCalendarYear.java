package com.pubint.pto.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.openjpa.persistence.jdbc.ElementJoinColumn;

@Entity
@Table(name = "calendarYear")
@XmlRootElement(name = "year")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoCalendarYear {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "year")
	private Integer calendarYear;

	@OneToMany(cascade = {
							CascadeType.PERSIST
	}, fetch = FetchType.EAGER)
	@OrderBy("holidayDate asc")
	@ElementJoinColumn(name = "calendarYearID", referencedColumnName = "id")
	List<PtoCalendarDetail> details = new ArrayList<PtoCalendarDetail>();

	public PtoCalendarYear() {
		super();
	}

	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlElement
	public Integer getCalendarYear() {
		return calendarYear;
	}

	public void setCalendarYear(Integer calendarYear) {
		this.calendarYear = calendarYear;
	}

	public List<PtoCalendarDetail> getDetails() {
		return details;
	}

	public void setDetails(List<PtoCalendarDetail> details) {
		this.details = details;
	}

	public void addDetail(PtoCalendarDetail detail) {
		if ((details == null) || (details.isEmpty())) {
			details = new ArrayList<PtoCalendarDetail>();
		}

		details.add(detail);
	}

	public void dropDetail(PtoCalendarDetail detail) {
		if (details.contains(detail)) {
			details.remove(detail);
		}
	}
}
