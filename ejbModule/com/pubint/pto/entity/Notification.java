package com.pubint.pto.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

@Entity
@Table(name = "notification")
@XmlRootElement(name = "note")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Notification {
	private static Logger log = Logger.getLogger("NotificationEntity");

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne
	@JoinColumn(name = "templateID")
	private NotificationTemplate template;

	@Column(name = "message")
	private String message;

	@Column(name = "processed")
	private Boolean processed;

	public Notification() {
		super();

		log.setLevel(Level.TRACE);

		processed = false;
	}

	public Notification(NotificationTemplate template, PtoEmployee employee) {
		this();

		this.template = template;

		log.trace("Grabbing the notification template...");

		String templateText = template.getText();

		try {
			log.trace("Setting the first name...");

			templateText = templateText.replaceAll("~employee.fullName~", employee.getFullName());
		} catch (Exception e) {

		}

		try {
			log.trace("Setting the title...");

			templateText = templateText.replaceAll("~employee.title~", employee.getTitle());
		} catch (Exception e) {

		}

		try {
			log.trace("Setting the location name...");

			templateText = templateText.replaceAll("~employee.location~", employee.getLocationName());
		} catch (Exception e) {

		}

		try {
			log.trace("Setting the department name...");

			templateText = templateText.replaceAll("~employee.departmentName~", employee.getDepartmentName());
		} catch (Exception e) {

		}

		try {
			log.trace("Setting the direct report name...");

			templateText = templateText.replaceAll("~employee.directReportName~", employee.getDirectReportName());
		} catch (Exception e) {

		}

		try {
			log.trace("Setting the start date...");

			templateText = templateText.replaceAll("~employee.startDate~", employee.getStartDate().toString());
		} catch (Exception e) {

		}

		message = templateText;
	}

	@XmlAttribute(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlTransient
	public NotificationTemplate getTemplate() {
		return template;
	}

	public void setTemplate(NotificationTemplate template) {
		this.template = template;
	}

	@XmlAttribute(name = "templateID")
	public Long getTemplateID() {
		if (template == null) {
			return null;
		}

		return template.getId();
	}

	@XmlElement(name = "title")
	public String getTitle() {
		if (template == null) {
			return null;
		}

		return template.getTitle();
	}

	@XmlElement(name = "messageText")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@XmlAttribute(name = "processed")
	public Boolean getProcessed() {
		if (processed == null) {
			return false;
		}

		return processed;
	}

	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}
}
