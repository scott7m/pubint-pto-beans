package com.pubint.pto.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NoResultException;
import javax.persistence.OneToOne;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table(name = "notificationRecipient")
@XmlRootElement(name = "noteTo")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class NotificationRecipient {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne
	@JoinColumn(name = "templateID")
	private NotificationTemplate template;

	@OneToOne
	@JoinColumn(name = "employeeID")
	private PtoEmployee employee;

	@OneToOne
	@JoinColumn(name = "departmentID")
	private PtoDepartment department;

	public NotificationRecipient() {
		super();
	}

	public NotificationRecipient(NotificationTemplate template, PtoEmployee employee, PtoDepartment department) {
		this();

		this.template = template;

		this.employee = employee;
		this.department = department;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlTransient
	public NotificationTemplate getTemplate() {
		return template;
	}

	public void setTemplate(NotificationTemplate template) {
		this.template = template;
	}

	@XmlAttribute(name = "templateID")
	public Long getTemplateID() {
		if (template == null) {
			return null;
		}

		return template.getId();
	}

	@XmlElement(name = "templateKey")
	public String getTemplateKey() {
		if (template == null) {
			return null;
		}

		return template.getKey();
	}

	@XmlTransient
	public PtoEmployee getEmployee() {
		return employee;
	}

	public void setEmployee(PtoEmployee employee) {
		this.employee = employee;
	}

	@XmlAttribute(name = "employeeID")
	public Long getEmployeeID() {
		if (employee == null) {
			return null;
		}

		return employee.getId();
	}

	@XmlElement(name = "employeeName")
	public String getEmployeeName() {
		if (employee == null) {
			return null;
		}

		return employee.getFullName();
	}

	@XmlTransient
	public PtoDepartment getDepartment() {
		return department;
	}

	public void setDepartment(PtoDepartment department) {
		this.department = department;
	}

	@XmlAttribute(name = "departmentID")
	public Long getDepartmentID() {
		if (department == null) {
			return null;
		}

		return department.getId();
	}

	@XmlElement(name = "departmentName")
	public String getDepartmentName() {
		if (department == null) {
			return null;
		}

		return department.getDepartmentName();
	}

//	@XmlTransient
	public static final List<String> getRecipientList(EntityManager em, NotificationRecipient recipient) throws NoResultException {
		List<String> recipients = new ArrayList<String>();

		if ((recipient.getDepartment() == null) && (recipient.getEmployee() == null)) {
			throw new NoResultException("There are no recipients for this recipient record...");
		}

		if (recipient.getDepartment() == null) {
			recipients.add(recipient.getEmployee().getEmail());
		} else {
			String sql = "" +
				"select e from PtoEmployee e " +
				"where " +
				"(e.department = :department)";

			Query query = em.createQuery(sql);
			query.setParameter("department", recipient.getDepartment());

			if (query.getResultList().isEmpty()) {
				throw new NoResultException("Recipient record points to an empty department...");
			}

			@SuppressWarnings("unchecked") List<PtoEmployee> employees = query.getResultList();

			for (PtoEmployee employee : employees) {
				recipients.add(employee.getEmail());
			}
		}

		return recipients;
	}

	@Override
	public boolean equals(Object object) {
		if (! (object instanceof NotificationRecipient)) {
			return false;
		}

		NotificationRecipient that = (NotificationRecipient) object;

		if (! that.template.equals(template)) {
			return false;
		}

		if ((that.department != null) && (! that.department.equals(department))) {
			return false;
		}

		if ((that.employee != null) && (! that.employee.equals(employee))) {
			return false;
		}

		return true;
	}
}
