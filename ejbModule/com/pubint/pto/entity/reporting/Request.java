package com.pubint.pto.entity.reporting;

import java.sql.Date;
import java.sql.Time;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.pubint.pto.base.XmlDateAdapter;
import com.pubint.pto.base.XmlTimeAdapter;
import com.pubint.pto.dataObjects.PtoRequestStatus;
import com.pubint.pto.entity.PtoRequest;
import com.pubint.pto.entity.PtoTimeTypeCategory;

@XmlRootElement(name = "request")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Request {
	private String requestType;
	private Long requestTypeCategoryID;

	private Date startDate;
	private Time startTime;
	private Date endDate;
	private Time endTime;

	private Double actualDaysTaken;

	private String comment;

	private String employeeName;
	private Integer statusID;

	private String requestCreated;
	private String creator;

	private String action;
	private String actionBy;
	private String actionDate;

	private String canceledBy;
	private String canceledDate;

	private Time employeeStartTime;
	private Time employeeEndTime;

	public Request() {
		super();
	}

	public Request(PtoRequest from) {
		requestType = from.getRequestTypeShortDescription();
		requestTypeCategoryID = from.getTimeTypeCategoryID();

		startDate = from.getStartDate();
		startTime = from.getStartTime();

		endDate = from.getEndDate();
		endTime = from.getEndTime();

		comment = from.getComment();

		employeeName = from.getEmployeeName();

		actualDaysTaken = from.getDaysTaken();
		statusID = from.getStatusID();

		creator = from.getCreatorName();
		requestCreated = from.getRequestCreated();

		action = from.getAction();
		actionBy = from.getActionByName();

		canceledBy = from.getCanceledByName();
		canceledDate = from.getCanceledDate();

		employeeStartTime = from.getEmployeeStartTime();
		employeeEndTime = from.getEmployeeEndTime();
	}

	@XmlElement
	public String getRequestType() {
		return requestType;
	}

	@XmlAttribute(name = "timeCategory")
	public Long getRequestTypeCategoryID() {
		return requestTypeCategoryID;
	}

	@XmlElement
	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getStartDate() {
		return startDate;
	}

	@XmlElement
	@XmlJavaTypeAdapter(XmlTimeAdapter.class)
	public Time getStartTime() {
		return startTime;
	}

	@XmlElement
	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getEndDate() {
		return endDate;
	}

	@XmlElement
	@XmlJavaTypeAdapter(XmlTimeAdapter.class)
	public Time getEndTime() {
		return endTime;
	}

	@XmlElement
	public String getComment() {
		return comment;
	}

	@XmlElement(name = "employeeName")
	public String getEmployeeName() {
		return employeeName;
	}

	@XmlElement(name = "daysTaken")
	public Double getDaysTaken() throws Exception {
		if (actualDaysTaken != null) {
			return actualDaysTaken;
		}

		throw new Exception("Actual Days Taken not set!!!");
	}

	@XmlAttribute
	public Integer getStatusID() {
		return statusID;
	}

	@XmlElement
	public String getRequestStatus() {
		return PtoRequestStatus.getRequestStatus(this.statusID);
	}

	@XmlAttribute
	public boolean isOneToThree() {
		return PtoRequest.isOneToThree(requestTypeCategoryID);
	}

	@XmlAttribute
	public boolean isChargeable() {
		boolean chargeable = true;

		try {
			chargeable = false ||
				// PtoTimeTypeCategory.COMP_TIME.equals(getTimeTypeCategoryID())
				// ||
				PtoTimeTypeCategory.ONE2THREE.equals(requestTypeCategoryID) ||
				PtoTimeTypeCategory.PTO_CHARGEABLE.equals(requestTypeCategoryID);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return chargeable;
	}

	@XmlAttribute
	public boolean isActive() {
		return ! isDead();
	}

	public boolean isDead() {
		return false ||
			statusID.equals(PtoRequestStatus.CANCELED) ||
			statusID.equals(PtoRequestStatus.REJECTED);
	}

	@XmlElement
	public String getRequestCreated() {
		return requestCreated;
	}

	@XmlElement(name = "requestEnteredBy")
	public String getCreator() {
		return creator;
	}

	@XmlElement
	public String getAction() {
		return action;
	}

	@XmlElement
	public String getActionBy() {
		return actionBy;
	}

	@XmlElement
	public String getActionDate() {
		return actionDate;
	}

	@XmlElement
	public String getCanceledBy() {
		return canceledBy;
	}

	@XmlElement
	public String getCanceledDate() {
		return canceledDate;
	}

	@XmlElement
	@XmlJavaTypeAdapter(XmlTimeAdapter.class)
	public Time getEmployeeStartTime() {
		return employeeStartTime;
	}

	@XmlElement
	@XmlJavaTypeAdapter(XmlTimeAdapter.class)
	public Time getEmployeeEndTime() {
		return employeeEndTime;
	}
}
