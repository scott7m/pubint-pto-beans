package com.pubint.pto.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.pubint.pto.base.XmlDateAdapter;

@Entity
@Table(name = "timeType")
@XmlRootElement(name = "timeType")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoTimeType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "timeTypeCategoryID")
	private PtoTimeTypeCategory timeTypeCategory;

	@Column(name = "description")
	private String description;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "officeID")
	private PtoLocation office;

	@Column(name = "yearBreak1")
	private Long yearBreak1;

	@Column(name = "break1Max")
	private Long year1MaxPTO;

	@Column(name = "yearBreak2")
	private Long yearBreak2;

	@Column(name = "break2Max")
	private Long year2MaxPTO;

	@Column(name = "yearBreak3")
	private Long yearBreak3;

	@Column(name = "maxDaysAllowed")
	private Long maxDaysAllowed;

	@Column(name = "maxRollover")
	private Long maxRollover;

	@Column(name = "break3Max")
	private Long year3MaxPTO;

	@Column(name = "year1Break1")
	private Date year1Break1;

	@Column(name = "year1Break1Days")
	private Long year1Break1Days;

	@Column(name = "year1Break2")
	private Date year1Break2;

	@Column(name = "year1Break2Days")
	private Long year1Break2Days;

	@Column(name = "year1Break3")
	private Date year1Break3;

	@Column(name = "year1Break3Days")
	private Long year1Break3Days;

	@Column(name = "year1Break4")
	private Date year1Break4;

	@Column(name = "year1Break4Days")
	private Long year1Break4Days;

	@Column(name = "year1Break5")
	private Date year1Break5;

	@Column(name = "year1Break5Days")
	private Long year1Break5Days;

	@Column(name = "year1Break6")
	private Date year1Break6;

	@Column(name = "year1Break6Days")
	private Long year1Break6Days;

	@Column(name = "year1Break7")
	private Date year1Break7;

	@Column(name = "year1Break7Days")
	private Long year1Break7Days;

	@Column(name = "year1Break8")
	private Date year1Break8;

	@Column(name = "year1Break8Days")
	private Long year1Break8Days;

	@Column(name = "year1Break9")
	private Date year1Break9;

	@Column(name = "year1Break9Days")
	private Long year1Break9Days;

	@Column(name = "year1Break10")
	private Date year1Break10;

	@Column(name = "year1Break10Days")
	private Long year1Break10Days;

	@Column(name = "year1Break11")
	private Date year1Break11;

	@Column(name = "year1Break11Days")
	private Long year1Break11Days;

	@Column(name = "year1Break12")
	private Date year1Break12;

	@Column(name = "year1Break12Days")
	private Long year1Break12Days;

	public PtoTimeType() {
		super();
	}

	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PtoTimeTypeCategory getTimeTypeCategory() {
		return timeTypeCategory;
	}

	public void setTimeTypeCategory(PtoTimeTypeCategory timeTypeCategory) {
		this.timeTypeCategory = timeTypeCategory;
	}

	@XmlElement
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement
	public String getSelectDesc() {
		return (office == null ? "Default" : office.getName()) + " - " + this.getDescription();
	}

	public PtoLocation getOffice() {
		return office;
	}

	public void setOffice(PtoLocation office) {
		this.office = office;
	}

	public Long getYearBreak1() {
		return yearBreak1;
	}

	public void setYearBreak1(Long yearBreak1) {
		this.yearBreak1 = yearBreak1;
	}

	public Long getYear1MaxPTO() {
		return year1MaxPTO;
	}

	public void setYear1MaxPTO(Long year1MaxPTO) {
		this.year1MaxPTO = year1MaxPTO;
	}

	public Long getYearBreak2() {
		return yearBreak2;
	}

	public void setYearBreak2(Long yearBreak2) {
		this.yearBreak2 = yearBreak2;
	}

	public Long getYear2MaxPTO() {
		return year2MaxPTO;
	}

	public void setYear2MaxPTO(Long year2MaxPTO) {
		this.year2MaxPTO = year2MaxPTO;
	}

	public Long getYearBreak3() {
		return yearBreak3;
	}

	public void setYearBreak3(Long yearBreak3) {
		this.yearBreak3 = yearBreak3;
	}

	public Long getYear3MaxPTO() {
		return year3MaxPTO;
	}

	public void setYear3MaxPTO(Long year3MaxPTO) {
		this.year3MaxPTO = year3MaxPTO;
	}

	public Long getMaxDaysAllowed() {
		return maxDaysAllowed;
	}

	public void setMaxDaysAllowed(Long maxDaysAllowed) {
		this.maxDaysAllowed = maxDaysAllowed;
	}

	public Long getMaxRollover() {
		return maxRollover;
	}

	public void setMaxRollover(Long maxRollover) {
		this.maxRollover = maxRollover;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getYear1Break1() {
		return year1Break1;
	}

	public void setYear1Break1(Date year1Break1) {
		this.year1Break1 = year1Break1;
	}

	public Long getYear1Break1Days() {
		return year1Break1Days;
	}

	public void setYear1Break1Days(Long year1Break1Days) {
		this.year1Break1Days = year1Break1Days;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getYear1Break2() {
		return year1Break2;
	}

	public void setYear1Break2(Date year1Break2) {
		this.year1Break2 = year1Break2;
	}

	public Long getYear1Break2Days() {
		return year1Break2Days;
	}

	public void setYear1Break2Days(Long year1Break2Days) {
		this.year1Break2Days = year1Break2Days;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getYear1Break3() {
		return year1Break3;
	}

	public void setYear1Break3(Date year1Break3) {
		this.year1Break3 = year1Break3;
	}

	public Long getYear1Break3Days() {
		return year1Break3Days;
	}

	public void setYear1Break3Days(Long year1Break3Days) {
		this.year1Break3Days = year1Break3Days;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getYear1Break4() {
		return year1Break4;
	}

	public void setYear1Break4(Date year1Break4) {
		this.year1Break4 = year1Break4;
	}

	public Long getYear1Break4Days() {
		return year1Break4Days;
	}

	public void setYear1Break4Days(Long year1Break4Days) {
		this.year1Break4Days = year1Break4Days;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getYear1Break5() {
		return year1Break5;
	}

	public void setYear1Break5(Date year1Break5) {
		this.year1Break5 = year1Break5;
	}

	public Long getYear1Break5Days() {
		return year1Break5Days;
	}

	public void setYear1Break5Days(Long year1Break5Days) {
		this.year1Break5Days = year1Break5Days;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getYear1Break6() {
		return year1Break6;
	}

	public void setYear1Break6(Date year1Break6) {
		this.year1Break6 = year1Break6;
	}

	public Long getYear1Break6Days() {
		return year1Break6Days;
	}

	public void setYear1Break6Days(Long year1Break6Days) {
		this.year1Break6Days = year1Break6Days;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getYear1Break7() {
		return year1Break7;
	}

	public void setYear1Break7(Date year1Break7) {
		this.year1Break7 = year1Break7;
	}

	public Long getYear1Break7Days() {
		return year1Break7Days;
	}

	public void setYear1Break7Days(Long year1Break7Days) {
		this.year1Break7Days = year1Break7Days;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getYear1Break8() {
		return year1Break8;
	}

	public void setYear1Break8(Date year1Break8) {
		this.year1Break8 = year1Break8;
	}

	public Long getYear1Break8Days() {
		return year1Break8Days;
	}

	public void setYear1Break8Days(Long year1Break8Days) {
		this.year1Break8Days = year1Break8Days;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getYear1Break9() {
		return year1Break9;
	}

	public void setYear1Break9(Date year1Break9) {
		this.year1Break9 = year1Break9;
	}

	public Long getYear1Break9Days() {
		return year1Break9Days;
	}

	public void setYear1Break9Days(Long year1Break9Days) {
		this.year1Break9Days = year1Break9Days;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getYear1Break10() {
		return year1Break10;
	}

	public void setYear1Break10(Date year1Break10) {
		this.year1Break10 = year1Break10;
	}

	public Long getYear1Break10Days() {
		return year1Break10Days;
	}

	public void setYear1Break10Days(Long year1Break10Days) {
		this.year1Break10Days = year1Break10Days;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getYear1Break11() {
		return year1Break11;
	}

	public void setYear1Break11(Date year1Break11) {
		this.year1Break11 = year1Break11;
	}

	public Long getYear1Break11Days() {
		return year1Break11Days;
	}

	public void setYear1Break11Days(Long year1Break11Days) {
		this.year1Break11Days = year1Break11Days;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getYear1Break12() {
		return year1Break12;
	}

	public void setYear1Break12(Date year1Break12) {
		this.year1Break12 = year1Break12;
	}

	public Long getYear1Break12Days() {
		return year1Break12Days;
	}

	public void setYear1Break12Days(Long year1Break12Days) {
		this.year1Break12Days = year1Break12Days;
	}
}
