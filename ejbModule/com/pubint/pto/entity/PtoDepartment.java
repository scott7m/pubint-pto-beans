package com.pubint.pto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

@Entity
@Table(name = "department")
@XmlRootElement(name = "department")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoDepartment implements Serializable {
	private static final long serialVersionUID = 3L;

	private static Logger log = Logger.getLogger("PtoDepartment_Entity");

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "departmentName")
	private String departmentName;

	@Column(name = "departmentShortName")
	private String shortName;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "departmentHead")
	private PtoEmployee departmentHead;

	@Column(name = "dailyReports")
	private Boolean dailyReport;

	@Column(name = "active")
	private Boolean active;

	public PtoDepartment() {
		super();

		log.setLevel(Level.TRACE);
	}

	@Override
	public String toString() {
		return "Department (" + id + "): " + departmentName;
	}

	@Override
	public boolean equals(Object compareTo) {
		if (! (compareTo instanceof PtoDepartment)) {
			return false;
		}

		PtoDepartment compare = (PtoDepartment) compareTo;

		return compare.getId().equals(getId());
	}

	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	@XmlTransient
	public PtoEmployee getDepartmentHead() {
		return departmentHead;
	}

	public void setDepartmentHead(PtoEmployee departmentHead) {
		this.departmentHead = departmentHead;
	}

	@XmlElement(name = "deptHeadID")
	public Long getDeptHeadID() {
		if (departmentHead == null) {
			return null;
		}

		return departmentHead.getId();
	}

	@XmlElement(name = "departmentHeadName")
	public String getDepartmentHeadName() {
		if (departmentHead == null) {
			return null;
		}

		return departmentHead.getFullName();
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Boolean isDailyReport() {
		if (dailyReport == null) {
			return true;
		}

		return dailyReport;
	}

	public void setDailyReport(boolean dailyReport) {
		this.dailyReport = dailyReport;
	}

	@XmlElement(name = "active")
	public Boolean isActive() {
		if (active == null) {
			return true;
		}

		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
