package com.pubint.pto.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.pubint.pto.base.XmlDateAdapter;
import com.pubint.pto.base.XmlTimeAdapter;
import com.pubint.pto.dataObjects.PtoRequestStatus;

@Entity
@Table(name = "request")
@XmlRootElement(name = "request")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger("PtoRequest_Entity");

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "requestTypeID")
	private PtoRequestType requestType;

	@Column(name = "startDate")
	private Date startDate;

	@Column(name = "startTime")
	private Time startTime;

	@Column(name = "endDate")
	private Date endDate;

	@Column(name = "endTime")
	private Time endTime;

	@Column(name = "comment")
	private String comment;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employeeID")
	private PtoEmployee employee;

	@Transient
	String accessLevel;

	@Column(name = "daysTaken")
	private Double actualDaysTaken;

	@Column(name = "requestStatus")
	private Integer statusID;

	@Column(name = "createdDate")
	private String requestCreated;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "createdBy")
	private PtoEmployee creator;

	@Column(name = "action")
	private String action;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "actionBy")
	private PtoEmployee actionBy;

	@Column(name = "actionDate")
	private String actionDate;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "canceledBy")
	private PtoEmployee canceledBy;

	@Column(name = "canceledDate")
	private String canceledDate;

	@Column(name = "employeeStartTime")
	private Time employeeStartTime;

	@Column(name = "employeeEndTime")
	private Time employeeEndTime;

	public PtoRequest() {
		super();

		log.setLevel(Level.INFO);
	}

	public PtoRequest(PtoRequest from) {
		requestType = from.getRequestType();
		startDate = from.getStartDate();
		startTime = from.getStartTime();

		endDate = from.getEndDate();
		endTime = from.getEndTime();

		comment = from.getComment();

		employee = from.getEmployee();
		actualDaysTaken = from.getActualDaysTaken();
		statusID = from.getStatusID();

		creator = from.getCreator();
		requestCreated = from.getRequestCreated();

		action = from.getAction();
		actionBy = from.getActionBy();

		canceledBy = from.getCanceledBy();
		canceledDate = from.getCanceledDate();

		employeeStartTime = from.getEmployeeStartTime();
		employeeEndTime = from.getEmployeeEndTime();
	}

	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PtoRequestType getRequestType() {
		return requestType;
	}

	public void setRequestType(PtoRequestType requestType) {
		this.requestType = requestType;
	}

	public String getRequestTypeDescription() {
		if (requestType == null) {
			return null;
		}

		return requestType.getDescription();
	}

	public String getRequestTypeShortDescription() {
		if (requestType == null) {
			return null;
		}

		return requestType.getShortDescription();
	}

	public PtoTimeType getTimeType() {
		if (requestType == null) {
			return null;
		}

		return requestType.getTimeType();
	}

	public Long getTimeTypeID() {
		if (getTimeType() == null) {
			return null;
		}

		return getTimeType().getId();
	}

	public String getTimeTypeDescription() {
		if (getTimeType() == null) {
			return null;
		}

		return getTimeType().getDescription();
	}

	public PtoTimeTypeCategory getTimeTypeCategory() {
		if (getTimeType() == null) {
			return null;
		}

		return getTimeType().getTimeTypeCategory();
	}

	public Long getTimeTypeCategoryID() {
		if (getTimeTypeCategory() == null) {
			return null;
		}

		return getTimeTypeCategory().getId();
	}

	public String getTimeTypeCategoryDescription() {
		if (getTimeTypeCategory() == null) {
			return null;
		}

		return getTimeTypeCategory().getDescription();
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@XmlJavaTypeAdapter(XmlTimeAdapter.class)
	public Time getStartTime() {
		return startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@XmlJavaTypeAdapter(XmlTimeAdapter.class)
	public Time getEndTime() {
		return endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public PtoEmployee getEmployee() {
		return employee;
	}

	public void setEmployee(PtoEmployee employee) {
		this.employee = employee;
	}

	public String getEmployeeName() {
		if (employee == null) {
			return null;
		}

		return employee.getFullName();
	}

	public String getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(String accessLevel) {
		this.accessLevel = accessLevel;
	}

	@XmlTransient
	public Double getActualDaysTaken() {
		return actualDaysTaken;
	}

	public void setActualDaysTaken(Double daysTaken) {
		this.actualDaysTaken = daysTaken;
	}

	@XmlElement(name = "daysTaken")
	public Double getDaysTaken() {
		if (actualDaysTaken != null) {
			return actualDaysTaken;
		}

		return calculateNumDays();
	}

	private Double calculateNumDays() {
		return PtoRequest.calculateNumDays(getEmployee(), isOneToThree(), startDate, endDate, startTime, endTime, employeeStartTime, employeeEndTime);
	}

	public static double calculateNumDays(PtoEmployee employee, boolean isOneToThree, Date startDate, Date endDate, Time startTime, Time endTime) {
		return calculateNumDays(employee, isOneToThree, startDate, endDate, startTime, endTime, employee.getStartTime(), employee.getEndTime());
	}

	public static double calculateNumDays(PtoEmployee employee, boolean isOneToThree, Date startDate, Date endDate, Time startTime, Time endTime, Time empStartTime, Time empEndTime) {
		if (empStartTime == null) {
			empStartTime = employee.getStartTime();
		}

		if (empEndTime == null) {
			empEndTime = employee.getEndTime();
		}

		double daysRequested = 0;

		if (isOneToThree) {
			return 1.0;
		}

		GregorianCalendar workCal = new GregorianCalendar();
		GregorianCalendar startCal = new GregorianCalendar();
		GregorianCalendar endCal = new GregorianCalendar();

		workCal.setTime(startDate);
		startCal.setTime(startDate);
		endCal.setTime(endDate);
		
		/**
		 * If they are coming back on the first working 
		 * day of the year, treat it as if they are coming 
		 * back new years eve. 
		 */
		log.trace("Checking to see if employee is returning first working day of the year.");
		if ((endCal.get(Calendar.MONTH) == 0) && (endCal.get(Calendar.DAY_OF_MONTH) == 4)) {
			log.trace("Verified employee returning first day of year.");
			endCal.set(Calendar.YEAR, startCal.get(Calendar.YEAR));
			endCal.set(Calendar.MONTH, 11);
			endCal.set(Calendar.DAY_OF_MONTH, 31);
			log.trace("Set end date to 12/31/" + startCal.get(Calendar.YEAR));
			
		}

		daysRequested = endCal.get(GregorianCalendar.DAY_OF_YEAR) - startCal.get(GregorianCalendar.DAY_OF_YEAR);

		if (empStartTime.compareTo(startTime) < 0) {
			if (startTime.getTime() - empStartTime.getTime() > 3600000 * 2) {
				daysRequested = daysRequested - 0.5;
			}
		}

		if (empStartTime.compareTo(endTime) < 0) {
			long msDifference = endTime.getTime() - empStartTime.getTime();

			// > 5 hours away = full day, > 1 hour away = half day
			if (msDifference >= 3600000 * 5.5) {
				daysRequested = daysRequested + 1;
			} else if (msDifference >= 3600000 * 1) {
				daysRequested = daysRequested + 0.5;
			}
		}

		PtoLocation loc = employee.getLocation();
		PtoCalendar ptoCal = loc.getCalendar();
		PtoCalendarYear ptoYear = ptoCal.getPtoYear(workCal.get(Calendar.YEAR));

		GregorianCalendar ptoHoliday = new GregorianCalendar();

		// Subtract weekends
		while (workCal.compareTo(endCal) < 0) {
			workCal.add(GregorianCalendar.DAY_OF_YEAR, 1);

			if ((workCal.get(Calendar.DAY_OF_WEEK) == 1) || (workCal.get(Calendar.DAY_OF_WEEK) == 7)) {
				// log.trace("Removing a day for weekend.");

				daysRequested--;
			}
		}

		// Subtract Holidays
		for (PtoCalendarDetail detail : ptoYear.getDetails()) {
			ptoHoliday.setTime(detail.getHolidayDate());

			if ((startCal.compareTo(ptoHoliday) <= 0) && (endCal.compareTo(ptoHoliday) > 0)) {
				if (detail.isSummerHours()) {
					if (employee.takesSummerHours()) {
						// If they are taking 5 days in the first week, summer
						// hours discount does not apply.
						int diff = ptoHoliday.get(GregorianCalendar.DAY_OF_YEAR) - startCal.get(GregorianCalendar.DAY_OF_YEAR);

						log.trace("" +
							"Attempting holidayDOY: " +
							ptoHoliday.get(GregorianCalendar.DAY_OF_YEAR) +
							" minus startCalDOY: " + startCal.get(GregorianCalendar.DAY_OF_YEAR) +
							" equals: " + diff);

						if ((diff < 4) /** && (diff > 0) */
						) {
							daysRequested = daysRequested - .5;
						}
					}
				} else {
					daysRequested--;
				}
			}
		}

		if (daysRequested < 0) {
			daysRequested = 0;
		}

		return daysRequested;
	}

	public static double calculateNumHours(PtoEmployee employee, Date startDate, Date endDate, Time startTime, Time endTime) {
		double hoursAway = 0.0;

		@SuppressWarnings("unused") GregorianCalendar workCal = new GregorianCalendar();

		GregorianCalendar startCal = new GregorianCalendar();
		GregorianCalendar endCal = new GregorianCalendar();
		GregorianCalendar empStart = new GregorianCalendar();
		GregorianCalendar empEnd = new GregorianCalendar();

		String[] startSplit = startTime.toString().split(":");
		String[] endSplit = endTime.toString().split(":");

		@SuppressWarnings("unused") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		startCal.setTime(startDate);
		startCal.set(GregorianCalendar.HOUR_OF_DAY, Integer.parseInt(startSplit[0]));
		startCal.set(GregorianCalendar.MINUTE, Integer.parseInt(startSplit[1]));

		endCal.setTime(endDate);
		endCal.set(GregorianCalendar.HOUR_OF_DAY, Integer.parseInt(endSplit[0]));
		endCal.set(GregorianCalendar.MINUTE, Integer.parseInt(endSplit[1]));

		String[] empStartSplit = employee.getStartTime().toString().split(":");
		String[] empEndSplit = employee.getEndTime().toString().split(":");

		empStart.setTime(endDate);
		empStart.set(GregorianCalendar.HOUR_OF_DAY, Integer.parseInt(empStartSplit[0]));
		empStart.set(GregorianCalendar.MINUTE, Integer.parseInt(empStartSplit[1]));

		empEnd.setTime(startDate);
		empEnd.set(GregorianCalendar.HOUR_OF_DAY, Integer.parseInt(empEndSplit[0]));
		empEnd.set(GregorianCalendar.MINUTE, Integer.parseInt(empEndSplit[1]));

		@SuppressWarnings("unused") Time empStartTime = employee.getStartTime();

		@SuppressWarnings("unused") Time empEndTime = employee.getEndTime();

		long millisAway = endCal.getTimeInMillis() - startCal.getTimeInMillis();

		if ((! endDate.equals(startDate)) && (endTime.equals(employee.getStartTime()))) {
			millisAway = ((long) millisAway - (empStart.getTimeInMillis() - empEnd.getTimeInMillis()));
		}

		hoursAway = ((double) millisAway / (60 * 60 * 1000));

		return hoursAway;
	}

	@XmlAttribute
	public Integer getStatusID() {
		return statusID;
	}

	public void setStatusID(Integer statusID) {
		this.statusID = statusID;
	}

	@XmlElement
	public String getRequestStatus() {
		return PtoRequestStatus.getRequestStatus(this.statusID);
	}

	@XmlAttribute
	public boolean isOneToThree() {
		return PtoRequest.isOneToThree(getTimeTypeCategoryID());
	}

	public static boolean isOneToThree(PtoRequestType requestType) {
		if (requestType == null) {
			return false;
		}

		return PtoRequest.isOneToThree(requestType.getTimeTypeCategoryID());
	}

	public static boolean isOneToThree(Long timeTypeCategoryID) {
		boolean oneToThree = false;

		try {
			oneToThree = false ||
				PtoTimeTypeCategory.ONE2THREE.equals(timeTypeCategoryID) ||
				PtoTimeTypeCategory.UNPAID_ONE2THREE.equals(timeTypeCategoryID);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return oneToThree;
	}

	@XmlAttribute
	public boolean isChargeable() {
		boolean chargeable = true;

		try {
			chargeable = false ||
				// PtoTimeTypeCategory.COMP_TIME.equals(getTimeTypeCategoryID())
				// ||
				PtoTimeTypeCategory.ONE2THREE.equals(getTimeTypeCategoryID()) ||
				PtoTimeTypeCategory.PTO_CHARGEABLE.equals(getTimeTypeCategoryID());
		} catch (Exception e) {
			log.error("Unable to determine the chargeability of the request: " + e.getMessage(), e);
		}

		return chargeable;
	}

	@XmlAttribute
	public boolean isUnpaid() {
		boolean unpaid = true;

		try {
			unpaid = false ||
				PtoTimeTypeCategory.UNPAID.equals(getTimeTypeCategoryID()) ||
				PtoTimeTypeCategory.UNPAID_ONE2THREE.equals(getTimeTypeCategoryID());
		} catch (Exception e) {
			log.error("Unable to determine whether the request is unpaid: " + e.getMessage(), e);
		}

		return unpaid;
	}

	public boolean isActive() {
		return ! isDead();
	}

	public boolean isDead() {
		return false ||
			statusID.equals(PtoRequestStatus.CANCELED) ||
			statusID.equals(PtoRequestStatus.REJECTED);
	}

	@XmlElement
	public String getRequestCreated() {
		return requestCreated;
	}

	public void setRequestCreated(String requestCreated) {
		this.requestCreated = requestCreated;
	}

	public PtoEmployee getCreator() {
		return creator;
	}

	public void setCreator(PtoEmployee creator) {
		this.creator = creator;
	}

	public String getCreatorName() {
		if (creator == null) {
			return null;
		}

		return creator.getFullName();
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public PtoEmployee getActionBy() {
		return actionBy;
	}

	public void setActionBy(PtoEmployee actionBy) {
		this.actionBy = actionBy;
	}

	public String getActionByName() {
		if (actionBy == null) {
			return null;
		}

		return actionBy.getFullName();
	}

	public String getActionDate() {
		return actionDate;
	}

	public void setActionDate(String actionDate) {
		this.actionDate = actionDate;
	}

	public PtoEmployee getCanceledBy() {
		return canceledBy;
	}

	public void setCanceledBy(PtoEmployee canceledBy) {
		this.canceledBy = canceledBy;
	}

	public String getCanceledByName() {
		if (canceledBy == null) {
			return null;
		}

		return canceledBy.getFullName();
	}

	public String getCanceledDate() {
		return canceledDate;
	}

	public void setCanceledDate(String canceledDate) {
		this.canceledDate = canceledDate;
	}

	@XmlJavaTypeAdapter(XmlTimeAdapter.class)
	public Time getEmployeeStartTime() {
		return employeeStartTime;
	}

	public void setEmployeeStartTime(Time employeeStartTime) {
		this.employeeStartTime = employeeStartTime;
	}

	@XmlJavaTypeAdapter(XmlTimeAdapter.class)
	public Time getEmployeeEndTime() {
		return employeeEndTime;
	}

	public void setEmployeeEndTime(Time employeeEndTime) {
		this.employeeEndTime = employeeEndTime;
	}
}
