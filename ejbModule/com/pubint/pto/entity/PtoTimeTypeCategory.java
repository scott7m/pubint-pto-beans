package com.pubint.pto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@Table(name = "timeTypeCategory")
@XmlRootElement(name = "timeTypeCategory")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoTimeTypeCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	// ID's of seed data
	public static Long PTO_CHARGEABLE = Long.valueOf(1);
	public static Long UNPAID = Long.valueOf(2);
	public static Long ONE2THREE = Long.valueOf(3);
	public static Long UNPAID_ONE2THREE = Long.valueOf(4);
	public static Long COMP_TIME = Long.valueOf(5);
	public static Long PTO_NONCHARGEABLE = Long.valueOf(6);

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "description")
	private String description;

	@Column(name = "maxHours")
	private Integer maxHours;

	@Column(name = "maxConsecutive")
	private Integer maxConsecutive;

	@Column(name = "chargeable")
	private Boolean chargeable;

	@Column(name = "oneToThree")
	private Boolean oneToThree;

	public PtoTimeTypeCategory() {
		super();
	}

	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMaxHours() {
		return maxHours;
	}

	public void setMaxHours(Integer maxHours) {
		this.maxHours = maxHours;
	}

	public Integer getMaxConsecutive() {
		return maxConsecutive;
	}

	public void setMaxConsecutive(Integer maxConsecutive) {
		this.maxConsecutive = maxConsecutive;
	}

	public Boolean isChargeable() {
		if (chargeable == null) {
			return true;
		}

		return chargeable;
	}

	public void setChargeable(boolean chargeable) {
		this.chargeable = chargeable;
	}

	public Boolean isOneToThree() {
		if (oneToThree == null) {
			return false;
		}

		return oneToThree;
	}

	public void setOneToThree(boolean oneToThree) {
		this.oneToThree = oneToThree;
	}
}
