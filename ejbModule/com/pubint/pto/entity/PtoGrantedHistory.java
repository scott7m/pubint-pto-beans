package com.pubint.pto.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.pubint.pto.base.XmlDateAdapter;

@Entity
@Table(name = "grantedPtoHistory")
@XmlRootElement(name = "grantedPtoHistory")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoGrantedHistory {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employeeID")
	private PtoEmployee employee;

	@Column(name = "year")
	private Integer year;

	@Column(name = "basePTO")
	private Double basePTO;

	@Column(name = "baseOneToThree")
	private Integer baseOneToThree;

	@Column(name = "rolloverDays")
	private Double rolloverDays;

	@Column(name = "compTimeGranted")
	private Double compTimeGranted;

	@Column(name = "compTimeExpireDate")
	private Date compTimeExpireDate;

	@Column(name = "ptoAdjustment")
	private Double ptoAdjustment;

	@Column(name = "ptoAdjustmentExpireDate")
	private Date ptoAdjustmentExpireDate;

	@Column(name = "bonusOneToThree")
	private Integer bonusOneToThree;

	@Column(name = "bonusOneToThreeExpireDate")
	private Date bonusOneToThreeExpireDate;

	public PtoGrantedHistory() {
		super();

		// log.setLevel(Level.INFO);
	}

	public PtoGrantedHistory(PtoEmployee employee, Integer year) {
		this();

		this.employee = employee;
		this.year = year;
	}

	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlTransient
	public PtoEmployee getEmployee() {
		return employee;
	}

	public void setEmployee(PtoEmployee employee) {
		this.employee = employee;
	}

	@XmlElement
	public Long getEmployeeID() {
		if (employee == null) {
			return null;
		}

		return employee.getId();
	}

	@XmlElement(name = "fullName")
	public String getEmployeeFullName() {
		if (employee == null) {
			return null;
		}

		return employee.getFullName();
	}

	@XmlElement
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	@XmlElement(name = "startingDaysPTO")
	public Double getBasePTO() {
		if (basePTO == null) {
			return 0.0;
		}

		return basePTO;
	}

	public void setBasePTO(Double basePTO) {
		this.basePTO = basePTO;
	}

	@XmlElement(name = "totalAvailablePTO")
	public Double getTotalAvailablePTO() {
		return getBasePTO() + getPtoAdjustment() + getRolloverDays();
	}

	@XmlElement(name = "oneTo3Granted")
	public Integer getBaseOneToThree() {
		return baseOneToThree == null ? 0 : baseOneToThree;
	}

	public void setBaseOneToThree(Integer baseOneToThree) {
		this.baseOneToThree = baseOneToThree;
	}

	@XmlElement(name = "totalAvailableOneToThree")
	public Integer getTotalAvailableOneToThree() {
		return getBaseOneToThree() + getBonusOneToThree();
	}

	@XmlElement
	public Double getRolloverDays() {
		if (rolloverDays == null) {
			return 0.0;
		}

		return rolloverDays;
	}

	public void setRolloverDays(Double rolloverDays) {
		this.rolloverDays = rolloverDays;
	}

	@XmlElement
	public Double getCompTimeGranted() {
		if (compTimeGranted == null) {
			return 0.0;
		}

		return compTimeGranted;
	}

	public void setCompTimeGranted(Double compTimeGranted) {
		this.compTimeGranted = compTimeGranted;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getCompTimeExpireDate() {
		return compTimeExpireDate;
	}

	public void setCompTimeExpireDate(Date compTimeExpireDate) {
		this.compTimeExpireDate = compTimeExpireDate;
	}

	@XmlElement
	public Double getPtoAdjustment() {
		if (ptoAdjustment == null) {
			return 0.0;
		}

		return ptoAdjustment;
	}

	public void setPtoAdjustment(Double ptoAdjustment) {
		this.ptoAdjustment = ptoAdjustment;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getPtoAdjustmentExpireDate() {
		return ptoAdjustmentExpireDate;
	}

	public void setPtoAdjustmentExpireDate(Date ptoAdjustmentExpireDate) {
		this.ptoAdjustmentExpireDate = ptoAdjustmentExpireDate;
	}

	@XmlElement
	public Integer getBonusOneToThree() {
		if (bonusOneToThree == null) {
			return 0;
		}

		return bonusOneToThree;
	}

	public void setBonusOneToThree(Integer bonusOneToThree) {
		this.bonusOneToThree = bonusOneToThree;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getBonusOneToThreeExpireDate() {
		return bonusOneToThreeExpireDate;
	}

	public void setBonusOneToThreeExpireDate(Date bonusOneToThreeExpireDate) {
		this.bonusOneToThreeExpireDate = bonusOneToThreeExpireDate;
	}

	@XmlElement
	public Double getTotalPTO() {
		return getBasePTO() + getRolloverDays() + getPtoAdjustment();
	}

	@XmlElement
	public Integer getTotalOneToThree() {
		return getBaseOneToThree() + getBonusOneToThree();
	}
}
