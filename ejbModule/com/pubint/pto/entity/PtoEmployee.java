package com.pubint.pto.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.openjpa.persistence.FetchAttribute;
import org.apache.openjpa.persistence.FetchGroup;
import org.apache.openjpa.persistence.FetchGroups;
import org.apache.openjpa.persistence.jdbc.ElementJoinColumn;

import com.pubint.pto.base.XmlDateAdapter;
import com.pubint.pto.base.XmlTimeAdapter;

@Entity
@Table(name = "employee")
@FetchGroups({
				@FetchGroup(name = "ptoEmployee.base", attributes = {
					@FetchAttribute(name = "department"),
					@FetchAttribute(name = "location"),
					@FetchAttribute(name = "history")
				})
})
@XmlRootElement(name = "employee")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoEmployee implements Serializable, Comparable<PtoEmployee> {
	private static final long serialVersionUID = 3L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "firstName")
	private String firstName;

	@Column(name = "middleInitial")
	private String middleInitial;

	@Column(name = "lastName")
	private String lastName;

	@Column(name = "password")
	private String password;

	@Column(name = "title")
	private String title;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "location")
	private PtoLocation location;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "department")
	private PtoDepartment department;

	@Column(name = "email")
	private String email;

	@Column(name = "extension")
	private String extension;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "directReport")
	private PtoEmployee directReport;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ptoReport")
	private PtoEmployee ptoReport;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "backupPtoReport")
	private PtoEmployee backupPtoReport;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employeeType")
	private PtoEmployeeType employeeType;

	@Column(name = "hourly")
	private Boolean hourly;

	@Column(name = "startTime")
	private Time startTime;

	@Column(name = "endTime")
	private Time endTime;

	@Column(name = "hireDate")
	private Date hireDate;

	@Column(name = "startDate")
	private Date startDate;

	@Column(name = "terminationDate")
	private Date terminationDate;

	@Column(name = "reviewDate")
	private Date reviewDate;

	@Column(name = "supervisor")
	private Boolean supervisor;

	@Column(name = "summerHours")
	private Boolean summerHours;

	@Column(name = "canRequestForOthers")
	private Boolean receptionRole;

	@OneToMany(cascade = {
							CascadeType.PERSIST
	}, fetch = FetchType.EAGER)
	@ElementJoinColumn(name = "employeeID", referencedColumnName = "id")
	@OrderBy(value="eventDate")
	private Collection<PtoEmployeeHistory> history = new ArrayList<PtoEmployeeHistory>();

	@Transient
	private PtoGrantedHistory grantedHistory;

	@Column(name = "newHireNotified")
	private Boolean newHireNoticeSent;

	public PtoEmployee() {
		super();
	}

	@Override
	public String toString() {
		return "Employee Name: " + getFullName();
	}

	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String newPassword) {
		password = newPassword;
	}

	@XmlElement
	public String getFullName() {
		return getFirstName() + " " + getLastName();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@XmlElement
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@XmlElement
	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	@XmlTransient
	public PtoLocation getLocation() {
		return location;
	}

	public void setLocation(PtoLocation location) {
		this.location = location;
	}

	@XmlElement(name = "locationID")
	public Long getLocationID() {
		if (location == null) {
			return null;
		}

		return location.getId();
	}

	@XmlElement(name = "locationName")
	public String getLocationName() {
		if (location == null) {
			return null;
		}

		return location.getName();
	}

	@XmlTransient
	public PtoDepartment getDepartment() {
		return department;
	}

	public void setDepartment(PtoDepartment department) {
		this.department = department;
	}

	@XmlElement(name = "departmentID")
	public Long getDepartmentID() {
		if (department == null) {
			return null;
		}

		return department.getId();
	}

	@XmlElement(name = "departmentName")
	public String getDepartmentName() {
		if (department == null) {
			return null;
		}

		return department.getDepartmentName();
	}

	@XmlTransient
	public PtoEmployee getDirectReport() {
		return directReport;
	}

	public void setDirectReport(PtoEmployee directReport) {
		this.directReport = directReport;
	}

	@XmlElement(name = "directReportID")
	public Long getDirectReportID() {
		if (directReport == null) {
			return null;
		}

		return directReport.getId();
	}

	@XmlElement(name = "directReportName")
	public String getDirectReportName() {
		if (directReport == null) {
			return null;
		}

		return directReport.getFullName();
	}

	@XmlTransient
	public PtoEmployee getPtoReport() {
		return ptoReport;
	}

	public void setPtoReport(PtoEmployee ptoReport) {
		this.ptoReport = ptoReport;
	}

	@XmlElement(name = "ptoReportID")
	public Long getPtoReportID() {
		if (ptoReport == null) {
			return null;
		}

		return ptoReport.getId();
	}

	@XmlElement(name = "ptoReportName")
	public String getPtoReportName() {
		if (ptoReport == null) {
			return null;
		}

		return ptoReport.getFullName();
	}

	@XmlTransient
	public PtoEmployee getBackupPtoReport() {
		return backupPtoReport;
	}

	public void setBackupPtoReport(PtoEmployee backupPtoReport) {
		this.backupPtoReport = backupPtoReport;
	}

	@XmlElement(name = "ptoBackupID")
	public Long getBackupPtoID() {
		if (backupPtoReport == null) {
			return null;
		}

		return backupPtoReport.getId();
	}

	@XmlElement(name = "ptoBackupName")
	public String getPtoBackupName() {
		if (backupPtoReport == null) {
			return null;
		}

		return backupPtoReport.getFullName();
	}

	@XmlTransient
	public PtoEmployeeType getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(PtoEmployeeType employeeType) {
		this.employeeType = employeeType;
	}

	@XmlElement(name = "employeeTypeID")
	public Long getEmployeeTypeID() {
		if (employeeType == null) {
			return null;
		}

		return employeeType.getId();
	}

	@XmlElement(name = "employeeTypeDescription")
	public String getEmployeeTypeDescription() {
		if (employeeType == null) {
			return null;
		}

		return employeeType.getDescription();
	}

	@XmlElement
	public Boolean isHourly() {
		if (hourly == null) {
			return false;
		}

		return hourly;
	}

	public void setHourly(Boolean hourly) {
		this.hourly = hourly;
	}

	@XmlJavaTypeAdapter(XmlTimeAdapter.class)
	public Time getStartTime() {
		return startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	@XmlJavaTypeAdapter(XmlTimeAdapter.class)
	public Time getEndTime() {
		return endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}

	@XmlElement
	public Double getRolloverDays() {
		if (grantedHistory == null) {
			return null;
		}

		return grantedHistory.getRolloverDays();
	}

	@XmlElement
	public Double getCompTimeGranted() {
		if (grantedHistory == null) {
			return null;
		}

		return grantedHistory.getCompTimeGranted();
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getCompTimeExpireDate() {
		if (grantedHistory == null) {
			return null;
		}

		return grantedHistory.getCompTimeExpireDate();
	}

	@XmlElement
	public Double getPtoAdjustment() {
		if (grantedHistory == null) {
			return null;
		}

		return grantedHistory.getPtoAdjustment();
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getPtoAdjustmentExpireDate() {
		if (grantedHistory == null) {
			return null;
		}

		return grantedHistory.getPtoAdjustmentExpireDate();
	}

	@XmlElement
	public Integer getBonusOneToThree() {
		if (grantedHistory == null) {
			return null;
		}

		return grantedHistory.getBonusOneToThree();
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getBonusOneToThreeExpireDate() {
		if (grantedHistory == null) {
			return null;
		}

		return grantedHistory.getBonusOneToThreeExpireDate();
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getReviewDate() {
		return reviewDate;
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

	@XmlElement
	public Boolean isSupervisor() {
		if (supervisor == null) {
			return false;
		}

		return supervisor;
	}

	public void setSupervisor(Boolean supervisor) {
		this.supervisor = supervisor;
	}

	public Boolean takesSummerHours() {
		if (summerHours == null) {
			return false;
		}

		return summerHours;
	}

	@XmlElement
	public Boolean isSummerHours() {
		return takesSummerHours();
	}

	public void setSummerHours(Boolean summerHours) {
		this.summerHours = summerHours;
	}

	@XmlElement
	public Boolean isReceptionRole() {
		if (receptionRole == null) {
			return false;
		}

		return receptionRole;
	}

	public Boolean hasReceptionRole() {
		if (receptionRole == null) {
			return false;
		}

		return receptionRole;
	}

	public void setReceptionRole(boolean isReceptionist) {
		receptionRole = isReceptionist;
	}

	public Collection<PtoEmployeeHistory> getHistory() {
		return history;
	}

	public void setHistory(Collection<PtoEmployeeHistory> history) {
		this.history = history;
	}

	public void addHistory(PtoEmployeeHistory history) {
		this.history.add(history);
	}

	@XmlTransient
	public PtoGrantedHistory getGrantedHistory() {
		return grantedHistory;
	}

	public void setGrantedHistory(PtoGrantedHistory grantedHistory) {
		this.grantedHistory = grantedHistory;
	}

	public boolean wasNewHireNoticeSent() {
		if (newHireNoticeSent == null) {
			return false;
		}

		return newHireNoticeSent;
	}

	public void setNewHireNoticeSent(boolean noticeSent) {
		newHireNoticeSent = noticeSent;
	}

	@Override
	public int compareTo(PtoEmployee emp) {
		return (this.lastName + " " + this.firstName).compareTo(emp.lastName + " " + emp.firstName);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PtoEmployee) {
			return this.equals((PtoEmployee) obj);
		}

		return false;
	}

	public boolean equals(PtoEmployee emp) {
		return this.getFullName().equals(emp.getFullName());
	}
	
	public void generatePassword() {
		this.password = createRandomPassword();
	}
	
	private String createRandomPassword() {
		StringBuilder retVal = new StringBuilder();
		Random rand = new Random();

		for (int i = 0; i <= 8; i++) {
			int r = rand.nextInt(60) + 63;

			if (r == 92) {
				r = 90;
			}

			retVal.append((char) r);
		}

		// this is the encoded equivalent of 'apassword'
		// return "Ru05KssO7JVoiautuqJhxQ";

		return retVal.toString();
	}
}
