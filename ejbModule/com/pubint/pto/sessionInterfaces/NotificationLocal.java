package com.pubint.pto.sessionInterfaces;

import java.io.Serializable;

import javax.xml.xpath.XPath;

import org.w3c.dom.Element;

public interface NotificationLocal {
	public String sendXML(long id);
	public String selectList();
	
	public String update(Element element, XPath xpath);
	
	public String addRecipient(Element element, XPath xpath);
	public String dropRecipient(Element element, XPath xpath);
	
	public void sendNewHireNotifications(Serializable parameters);
}
