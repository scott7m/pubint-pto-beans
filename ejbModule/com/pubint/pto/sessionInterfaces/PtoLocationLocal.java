package com.pubint.pto.sessionInterfaces;

import javax.xml.xpath.XPath;

import org.w3c.dom.Element;

public interface PtoLocationLocal {
	public String sendXML(long locID);
	public String selectList();
	
	public String update(Element element, XPath xpath);
	public String remove(Element element, XPath xpath);
}
