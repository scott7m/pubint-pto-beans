package com.pubint.pto.sessionInterfaces;

import javax.xml.xpath.XPath;

import org.w3c.dom.Element;

public interface PtoRequestTypeLocal {
	public String sendXML(long deptID);
	public String selectList(Element element, XPath xpath);
	
	public String update(Element element, XPath xpath);
	public String remove(Element element, XPath xpath);
}
