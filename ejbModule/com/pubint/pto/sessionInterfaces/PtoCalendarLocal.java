package com.pubint.pto.sessionInterfaces;

import javax.xml.xpath.XPath;

import org.w3c.dom.Element;

public interface PtoCalendarLocal {
	public String sendXML(Element element, XPath xpath);
	public String selectList();
	
	public String sendDetails(Element element, XPath xpath);
	
	public String update(Element element, XPath xpath);
	public String remove(Element element, XPath xpath);
//	public String removeYear(Element element, XPath xpath);
	
	public String addHoliday(Element element, XPath xpath);
	public String removeHoliday(Element element, XPath xpath);
}
