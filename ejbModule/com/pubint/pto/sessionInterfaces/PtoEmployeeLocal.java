package com.pubint.pto.sessionInterfaces;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.xml.xpath.XPath;

import org.w3c.dom.Element;

import com.pubint.pto.entity.PtoEmployee;

public interface PtoEmployeeLocal {
	public String sendXML(Element element, XPath xpath);

	public String sendXML(long empID);
	public String sendXML(long employeeID, java.sql.Date date);

	public PtoEmployee getEmployee(long employeeID);
	public PtoEmployee getEmployee(String user, String password);

	public String receptionistList(long currentEmployeeID);

	public String selectList(long currentEmployeeID);

	public String supervisorSelectList();

	public String activeEmployeeXML();

	public String update(Element element, XPath xpath);

	public String remove(Element element, XPath xpath);

	public String empIDToEmployeeName(long empID);

	public String getSupervisor(Element element, XPath xpath);
	public String getSupervisor(long serfID, long reportID);

	public String changePassword(Element element, XPath xpath);

	public List<PtoEmployee> getReportees(long supervisorID);
	public List<PtoEmployee> getReportees(long supervisorID, boolean includeDirect, boolean includePto, boolean includeBackup);

	public List<PtoEmployee> activeEmployeeListing();
	public List<PtoEmployee> activeEmployeeListing(Date asOfDate);

	public List<PtoEmployee> allEmployeeListing(int year);

	public List<PtoEmployee> getDepartmentList(long departmentID);

	public List<PtoEmployee> getTerminations(Date fromDate, Date toDate);

	public String typeSelectList();

	public String eventTypeSelectList();

	public String addEvent(Element element, XPath xpath);

	public String deleteEvent(Element element, XPath xpath);

	public double getTotalPTO(long employeeID);
	public double getBasePTO(long employeeID);
	public double getBasePTO(long employeeID, long timeTypeCategoryID);
	public double getBasePTO(long employeeID, long timeTypeCategoryID, int year);
	public int getBaseOneTo3(long empID);

	public void sendDailyAttendance();
	public void sendDailyAttendance(Serializable parameters);

	public boolean isHR(long employeeID);
	public boolean isHR(PtoEmployee employee);
	public boolean isIT(long employeeID);
	public boolean isReception(long employeeID);

	public String escalationPrivileges(Element element, XPath xpath) throws Exception;

	public void rollover2015PTO();

	public void sendNewSystemEmails();
	public String resetPassword(Element element, XPath xpath);
	public String sendNewPasswordEmail(PtoEmployee employee);

	public void populateGrantedHistory(Date effectiveDate);
}
