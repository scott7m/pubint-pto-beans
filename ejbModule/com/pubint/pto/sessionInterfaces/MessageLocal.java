package com.pubint.pto.sessionInterfaces;

import java.util.List;

import com.pubint.pto.entity.Notification;
import com.pubint.pto.entity.PtoRequest;
import com.pubint.pto.exceptions.InvitationException;

public interface MessageLocal {
	public void createEmail(String email, String title, String link, String message, List<String> fileList);
	
	public void createEmail(Notification notice);
	
	public void notifyIT(String title, String message, String link);
	
	public void sendInvitation(PtoRequest request) throws InvitationException;
}