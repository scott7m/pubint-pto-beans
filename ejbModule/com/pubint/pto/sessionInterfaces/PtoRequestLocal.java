package com.pubint.pto.sessionInterfaces;

import java.sql.Date;
import java.util.List;

import javax.xml.xpath.XPath;

import org.w3c.dom.Element;

import com.pubint.pto.dataObjects.PtoSummary;
import com.pubint.pto.entity.PtoEmployee;
import com.pubint.pto.entity.PtoGrantedHistory;
import com.pubint.pto.entity.PtoRequest;
import com.pubint.pto.entityList.PtoRequestList;

public interface PtoRequestLocal {
	public String sendXML(Element element, XPath xpath);
	public PtoRequest getRequest(long requestID);
	public String sendRequest(Element element, XPath xpath);
	
	public String processRequest(Element element, XPath xpath);
	
	public String calculateNumDays(Element element, XPath xpath);
	
	public String calculateDates(Element element, XPath xpath);
	public String calculateEndDate(Element element, XPath xpath);
	public String calculateStartDate(Element element, XPath xpath);
	public String calculateEndTime(Element element, XPath xpath);
	
	public String validateDate(Element element, XPath xpath);
	
	public PtoRequestList getEmployeeRequests(long empID, int year);
	public String getEmployeeHistory(Element element, XPath xpath);
	
	public String getPendingRequests(long employeeID);
	
	public PtoRequestList getAbsenceList(Date date);
	public PtoRequestList getRequestException(Date startDate, Date endDate);
	public List<PtoSummary> getUnusedPtoList(Date reportDate);
	public List<PtoSummary> getUnusedPtoList(Date reportDate, boolean allEmployees);
	public PtoSummary getPtoSummary(PtoEmployee employee, Date reportDate);
	public PtoGrantedHistory getGrantedHistory(PtoEmployee emp, Integer year);
	public String createAttendancePDF(Date reportDate);
	
	public String getRequestDetail(long supervisorID, Date endingDate);
	public String getRequestDetail(long supervisorID, Date endingDate, int levels);
	
	public String getRequestDetail(long supervisorID, Date startingDate, Date endingDate);
	public String getRequestDetail(long supervisorID, Date startingDate, Date endingDate, int levels);
	
	public String getDepartmentalAttendance(Element element, XPath xpath);
	public List<PtoRequest> findSummerHoursViolations();
}
