package com.pubint.pto.sessionInterfaces;

import javax.xml.xpath.XPath;

import org.w3c.dom.Element;

import com.pubint.pto.entity.PtoLocation;
import com.pubint.pto.entity.PtoTimeType;
import com.pubint.pto.entity.PtoTimeTypeCategory;

public interface PtoTimeTypeLocal {
	public String sendXML(long deptID);
	public String selectList();
	
	public String update(Element element, XPath xpath);
	public String remove(Element element, XPath xpath);
	
	public PtoTimeType getTimeType(PtoTimeTypeCategory type, PtoLocation loc);
	
	public String typeSelectList();
}
