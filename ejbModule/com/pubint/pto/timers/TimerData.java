package com.pubint.pto.timers;

import java.io.Serializable;

public class TimerData implements Serializable {
	private static final long serialVersionUID = 1L;

	public String userID;
	public String timerType;
	public String message;
	public boolean flag;

	public TimerData() {

	}

	public TimerData(String message) {
		this.message = message;
	}

	public TimerData(String userID, String message) {
		this.userID = userID;
		this.message = message;
	}

	public TimerData(String userID, String message, boolean flag) {
		this.userID = userID;
		this.message = message;
		this.flag = flag;
	}

	public String toString() {
		return "Timer for: " + userID + " -> " + message;
	}
}
