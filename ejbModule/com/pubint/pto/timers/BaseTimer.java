package com.pubint.pto.timers;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Timer;

import org.apache.log4j.Level;

import com.pubint.pto.base.PTOBase;

public class BaseTimer extends PTOBase {
	private static final long serialVersionUID = 3L;

	public static final boolean CHECK_TIME = true;
	public static final boolean ANY_TIME = false;

	public static final Level loggingLevel = Level.INFO;

	public void skip(Timer timer, String nextStep) {
		DroneInterface next = (DroneInterface) getBean(nextStep);

		if (next != null) {
			next.setSchedule((TimerData) timer.getInfo());
			next = null;
		}
	}

	public java.sql.Date now() {
		return new java.sql.Date((new java.util.Date()).getTime());
	}

	@PostConstruct
	public void sayHello() {
		System.out.println(this.getClass().getName() + " has been created!");
	}

	@PreDestroy
	public void sayGoodBye() {
		System.out.println(this.getClass().getName() + " is about to be destroyed!");
	}

	@Override
	protected void finalize() throws Throwable {
		System.out.println(this.getClass().getName() + " is being destroyed right now!");
		super.finalize();
	}
}