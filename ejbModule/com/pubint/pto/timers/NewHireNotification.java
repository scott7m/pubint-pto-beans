package com.pubint.pto.timers;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.pubint.pto.sessionInterfaces.NotificationLocal;

@Stateless
@Local(DroneInterface.class)
public class NewHireNotification extends BaseTimer {
	private static final long serialVersionUID = 1L;

	@Resource
	private javax.ejb.TimerService timerService;

	private static Logger log = Logger.getLogger("NewHires");

	@EJB
	private NotificationLocal notificationBean;

	private String hourly = "hourly run of new hire notifications";

	public NewHireNotification() {
		super();

		log.setLevel(Level.TRACE);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void setSchedule(TimerData data) {
		data.message = this.getClass().getName();
		data.timerType = "NewHireNotifications";

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		@SuppressWarnings("unchecked") Collection<Timer> timerList = timerService.getTimers();

		log.trace("Clear all of the old timers...");

		for (Timer timer : timerList) {
			try {
				if (hourly.equals((String) timer.getInfo())) {
					log.trace("Clearing a daily timer: " + timer.getNextTimeout());

					timer.cancel();
				}
			} catch (Exception e) {

			}
		}

		try {
			long hourlyPeriod = 60 * 60 * 1000;

			String firstRun = getTodaySQL().toString() + " 07:00:00";

			Date first = dateFormat.parse(firstRun);

			boolean skipTodayIfMissed = true;

			if (skipTodayIfMissed) {
				log.trace("Testing to see whether or not to skip the alert for today...");

				if (first.before(now())) {
					log.trace("Skipping the alert for today...too late...");

					GregorianCalendar shift = new GregorianCalendar();

					shift.setTime(now());
					shift.set(GregorianCalendar.MINUTE, 0);
					shift.add(GregorianCalendar.HOUR_OF_DAY, 1);

					first = shift.getTime();
				}
			}

			log.trace("Creating timer that should go off hourly at: " + first);

			timerService.createTimer(first, hourlyPeriod, hourly);
		} catch (Exception e) {
			log.error("Exception creating timers: " + e.getMessage(), e);
		}
	}

	@Timeout
	public void work(Timer timer) {
		// send out the new hire emails
		notificationBean.sendNewHireNotifications(timer.getInfo());
	}
}