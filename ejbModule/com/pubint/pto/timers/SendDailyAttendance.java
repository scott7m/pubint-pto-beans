package com.pubint.pto.timers;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.pubint.pto.sessionInterfaces.PtoEmployeeLocal;

@Stateless
@Local(DroneInterface.class)
public class SendDailyAttendance extends BaseTimer {
	private static final long serialVersionUID = 1L;

	@Resource
	private javax.ejb.TimerService timerService;

	private static Logger log = Logger.getLogger("PaLM_PClean2");

	@EJB
	private PtoEmployeeLocal employeeBean;

	private String morning = "automatic at 10:00 AM";
	private String afternoon = "automatic at 02:00 PM";

	public SendDailyAttendance() {
		super();

		log.setLevel(Level.TRACE);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void setSchedule(TimerData data) {
		data.message = this.getClass().getName();
		data.timerType = "SendDailyAttendance";

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		@SuppressWarnings("unchecked") Collection<Timer> timerList = timerService.getTimers();

		log.trace("Clear all of the old timers...");

		for (Timer timer : timerList) {
			try {
				if (morning.equals((String) timer.getInfo())) {
					log.trace("Clearing a morning timer: " + timer.getNextTimeout());

					timer.cancel();
				}

				if (afternoon.equals((String) timer.getInfo())) {
					log.trace("Clearing an afternoon timer: " + timer.getNextTimeout());

					timer.cancel();
				}
			} catch (Exception e) {

			}
		}

		try {
			long twentyFourHours = 24 * 60 * 60 * 1000;

			String todayAt10 = getTodaySQL().toString() + " 10:00:00";
			String todayAt2 = getTodaySQL().toString() + " 14:00:00";

			// todayAt10 = getTodaySQL().toString() + " 17:00:00";
			// todayAt2 = getTodaySQL().toString() + " 17:02:00";

			Date at10 = dateFormat.parse(todayAt10);
			Date at2 = dateFormat.parse(todayAt2);

			if (at10.before(now())) {
				GregorianCalendar shift = new GregorianCalendar();

				shift.setTime(at10);

				shift.add(GregorianCalendar.DAY_OF_WEEK, 1);

				at10 = shift.getTime();
			}

			if (at2.before(now())) {
				GregorianCalendar shift = new GregorianCalendar();

				shift.setTime(at2);

				shift.add(GregorianCalendar.DAY_OF_WEEK, 1);

				at2 = shift.getTime();
			}

			log.trace("Creating timer that should go off at: " + at10);

			timerService.createTimer(at10, twentyFourHours, morning);

			log.trace("Creating timer that should go off at: " + at2);

			timerService.createTimer(at2, twentyFourHours, afternoon);
		} catch (Exception e) {
			log.error("Exception creating timers: " + e.getMessage(), e);
		}
	}

	@Timeout
	public void work(Timer timer) {
		// send out the daily attendance emails
		employeeBean.sendDailyAttendance(timer.getInfo());
	}
}