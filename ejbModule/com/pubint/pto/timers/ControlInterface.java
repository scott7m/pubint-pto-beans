package com.pubint.pto.timers;

public interface ControlInterface {
	public void clearSchedule();
	public void setSchedule(TimerData data);
	public void now(TimerData data);
}
