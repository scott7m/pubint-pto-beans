package com.pubint.pto.session;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.openjpa.meta.FetchGroup;
import org.apache.openjpa.persistence.OpenJPAEntityManager;
import org.apache.openjpa.persistence.OpenJPAPersistence;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.pubint.pto.base.PTOBase;

public class PuppetBean extends PTOBase {
	private static final long serialVersionUID = 1L;

	// one day - in milliseconds
	public static long ONE_DAY = ((long) 24 * 60 * 60 * 1000);

	public List<String> parseComment(String commentText, int maxLength) {
		List<String> victor = new ArrayList<String>();

		String rowText;

		int whiteSpace = 0;
		int newLine = 0;

		boolean rowReady;

		while ((commentText.length() > 0)) {
			rowText = "";
			rowReady = false;

			while (! rowReady) {
				whiteSpace = commentText.indexOf(" ");
				newLine = commentText.indexOf("\n");

				if ((newLine >= 0) && (newLine < whiteSpace)) {
					if (rowText.length() > 0) {
						rowText += " ";
					}

					rowText += commentText.substring(0, newLine);
					commentText = commentText.substring(newLine + 1);

					rowReady = true;
				} else {
					if (whiteSpace >= 0) {
						if (rowText.length() + whiteSpace < maxLength) {
							if (rowText.length() > 0) {
								rowText += " ";
							}

							rowText += commentText.substring(0, whiteSpace);
							commentText = commentText.substring(whiteSpace + 1);
						} else {
							rowReady = true;
						}
					} else { // no whitespace
						rowReady = true;

						if (rowText.length() > 0) {
							if (rowText.length() + commentText.length() + 1 < maxLength) {
								if (rowText.length() > 0) {
									rowText += " ";
								}

								rowText += commentText;
								commentText = "";
							}

							victor.add(rowText.trim());
						}

						rowText = commentText;
						commentText = "";
					}
				}
			}

			victor.add(rowText.trim());

			if (whiteSpace > maxLength) {
				victor.add(commentText.trim());

				commentText = "";
			}
		}

		if (victor.size() % 2 != 0) {
			String blankRow = "";
			victor.add(blankRow);
		}

		return victor;
	}

	protected Element createElement(Document document, String elementName, String elementValue) {
		Element element = null;

		element = document.createElement(elementName);
		element.appendChild(document.createTextNode(elementValue));

		return element;
	}

	protected OpenJPAEntityManager getPM(EntityManager em) {
		OpenJPAEntityManager pm = OpenJPAPersistence.cast(em);
		pm.pushFetchPlan();

		pm.getFetchPlan().resetFetchGroups();
		pm.getFetchPlan().addFetchGroup(FetchGroup.NAME_DEFAULT);

		return pm;
	}

	protected String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "";
		}
	}

	protected String getHostAddress() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			return "";
		}
	}

	protected String stackTraceToString(Throwable e) {
		StringWriter sw = new StringWriter();

		if (e != null) {
			e.printStackTrace(new PrintWriter(sw));
		}

		return sw.toString();
	}
}