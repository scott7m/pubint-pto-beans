package com.pubint.pto.session;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.xpath.XPath;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import com.pubint.pto.entity.PtoDepartment;
import com.pubint.pto.entity.PtoEmployee;
import com.pubint.pto.entityList.PtoDepartmentList;
import com.pubint.pto.sessionInterfaces.PtoDepartmentLocal;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PtoDepartmentBean extends PuppetBean implements PtoDepartmentLocal, Serializable {
	private static final long serialVersionUID = 3L;

	private static Logger log = Logger.getLogger("ptoDepartmentBean");

	@PersistenceContext(unitName = "ptoJPA")
	private EntityManager entityManager;

	public PtoDepartmentBean() {
		super();

		log.setLevel(Level.TRACE);
	}

	@Override
	public String sendXML(long deptID) {
		StringWriter xml = new StringWriter();

		PtoDepartment department = entityManager.find(PtoDepartment.class, deptID);

		if (department != null) {
			try {
				JAXBContext context = JAXBContext.newInstance(department.getClass());

				Marshaller m = context.createMarshaller();

				m.marshal(department, xml);
			} catch (JAXBException je) {
				log.fatal("JAXB Exception: " + je.getMessage(), je);
			}
		}

		// log.trace("XML: " + fixXML(xml));

		return fixXML(xml);
	}

	@Override
	public String selectList() {
		String sql = "" +
			"select d from PtoDepartment d " +
			"order by d.departmentName";

		Query query = entityManager.createQuery(sql);

		@SuppressWarnings("unchecked") List<PtoDepartment> results = query.getResultList();

		PtoDepartmentList departments = new PtoDepartmentList();

		for (PtoDepartment dept : results) {
			departments.addDepartment(dept);
		}

		StringWriter xml = new StringWriter();

		try {
			JAXBContext context = JAXBContext.newInstance(departments.getClass());

			Marshaller m = context.createMarshaller();

			m.marshal(departments, xml);
		} catch (JAXBException je) {
			log.fatal("JAXB Exception: " + je.getMessage(), je);
		}

		// log.info("XML: " + fixXML(xml));

		return fixXML(xml);
	}

	@Override
	public String update(Element element, XPath xpath) {
		long deptID = this.getLong(element, "deptID", xpath);

		PtoDepartment department = entityManager.find(PtoDepartment.class, deptID);

		if (department == null) {
			log.trace("Creating new department");

			department = new PtoDepartment();

			entityManager.persist(department);
		} else {
			log.trace("Updating department: " + deptID);
		}

		long headID = this.getLong(element, "deptHeadID", xpath);

		PtoEmployee deptHead = entityManager.find(PtoEmployee.class, headID);

		log.trace("HeadID: " + headID);

		if (deptHead != null) {
			log.trace("Returned employee: " + deptHead.getFirstName() + " " + deptHead.getLastName());
		}

		department.setDepartmentHead(deptHead);

		department.setDepartmentName(this.getString(element, "deptName", xpath));
		department.setShortName(this.getString(element, "shortName", xpath));
		department.setDailyReport(this.getBoolean(element, "sendReport", xpath));

		return ""; // sendXML(department.getId());
	}

	@Override
	public String remove(Element element, XPath xpath) {
		String retVal = "fail";

		long deptID = this.getLong(element, "deptID", xpath);

		PtoDepartment department = entityManager.find(PtoDepartment.class, deptID);

		if (department != null) {
			entityManager.remove(department);

			retVal = "done";
		}

		return retVal;
	}
}
