package com.pubint.pto.session;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.xpath.XPath;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfBorderDictionary;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.pubint.pto.dataObjects.AttendanceRange;
import com.pubint.pto.dataObjects.AttendanceSingle;
import com.pubint.pto.dataObjects.PaLM;
import com.pubint.pto.dataObjects.PtoRequestStatus;
import com.pubint.pto.dataObjects.PtoSummary;
import com.pubint.pto.entity.PtoCalendar;
import com.pubint.pto.entity.PtoCalendarDetail;
import com.pubint.pto.entity.PtoCalendarYear;
import com.pubint.pto.entity.PtoEmployee;
import com.pubint.pto.entity.PtoGrantedHistory;
import com.pubint.pto.entity.PtoLocation;
import com.pubint.pto.entity.PtoRequest;
import com.pubint.pto.entity.PtoRequestType;
import com.pubint.pto.entity.PtoTimeTypeCategory;
import com.pubint.pto.entityList.PtoRequestList;
import com.pubint.pto.sessionInterfaces.MessageLocal;
import com.pubint.pto.sessionInterfaces.PtoEmployeeLocal;
import com.pubint.pto.sessionInterfaces.PtoRequestLocal;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PtoRequestBean extends PuppetBean implements PtoRequestLocal {
	private static final long serialVersionUID = 3L;

	private static Logger log = Logger.getLogger("ptoRequestBean");

	@PersistenceContext(unitName = "ptoJPA")
	private EntityManager entityManager;

	@EJB
	private MessageLocal messageBean;

	@EJB
	PtoEmployeeLocal employeeBean;

	public static final String server = "pilpto.pubint.com";
	public static final String port = ""; // include the colon :

	public PtoRequestBean() {
		super();

		log.setLevel(Level.INFO);
	}

	@Override
	public String sendXML(Element element, XPath xpath) {
		log.info("Sending XML!");

		StringWriter xml = new StringWriter();

		long requestID = getLong(element, "requestID", xpath);
		long currentEmployeeID = getLong(element, "currentEmployeeID", xpath);

		try {
			PtoRequest request = entityManager.find(PtoRequest.class, requestID);

			if (request != null) {
				PtoEmployeeLocal employeeBean = (PtoEmployeeLocal) getBean("PtoEmployeeBean");

				request.setAccessLevel(employeeBean.getSupervisor(request.getEmployee().getId(), currentEmployeeID));

				try {
					JAXBContext context = JAXBContext.newInstance(request.getClass());

					Marshaller m = context.createMarshaller();

					m.marshal(request, xml);
				} catch (JAXBException je) {
					log.fatal("JAXB Exception: " + je.getMessage(), je);
				}
			} else {
				log.error("Request " + requestID + " is null!");
			}
		} catch (NullPointerException npe) {
			log.error("Could not find specified pto requestID: " + requestID);
		} catch (Exception e) {
			log.error("Unexpected error getting pto request: " + e.getMessage(), e);
		}

		log.trace("XML: " + fixXML(xml));

		return fixXML(xml);
	}

	@Override
	public PtoRequest getRequest(long requestID) {
		return entityManager.find(PtoRequest.class, requestID);
	}
	
	/**
	 * IF FIRST DAY OF NEW YEAR IS NOT Jan 2nd,
	 * You have to change three places. sendRequest
	 * and calculateDates methods in this class,
	 * as well as calculateNumDays in PtoRequest entity.
	 * 
	 */

	@SuppressWarnings("deprecation")
	@Override
	public String sendRequest(Element element, XPath xpath) {
		boolean noEmails = false;
		
		try {
			long requestID = this.getLong(element, "requestID", xpath);
			PtoRequest request;
			
			if (requestID > 0) {
				request = getRequest(requestID);
			} else {
				request = new PtoRequest();
			}

			long reqTypeID = this.getLong(element, "requestTypeID", xpath);

			PtoRequestType requestType;

			if (reqTypeID > 0) {
				log.info("reqID > 0, looking up request type!");

				requestType = entityManager.find(PtoRequestType.class, reqTypeID);

				log.info("Found request type! " + requestType.getDescription());

				request.setRequestType(requestType);
			} else {
				return "fail";
			}

			PtoEmployee requester = entityManager.find(PtoEmployee.class, this.getLong(element, "currentEmployeeID", xpath));
			PtoEmployee employee = entityManager.find(PtoEmployee.class, this.getLong(element, "employeeID", xpath));

			log.trace("Employee found: " + employee.getFullName());

			@SuppressWarnings("unused") Date today = getTodaySQL();

			java.sql.Date startDate = this.getDate(element, "beginDate", xpath);

			log.trace("Start Date: " + startDate);

			java.sql.Date endDate = this.getDate(element, "endDate", xpath);

			log.trace("End Date: " + endDate);

			Time startTime = stringToTime(this.getString(element, "beginTime", xpath));

			log.trace("Start Time: " + startTime);

			Time endTime = stringToTime(this.getString(element, "endTime", xpath));

			log.trace("End Time: " + endTime);

			double daysTaken = this.getReal(element, "daysTaken", xpath);

			double calcTimeTaken;

			String daysMessage;

			GregorianCalendar reqBegin = new GregorianCalendar();
			reqBegin.setTimeInMillis(startDate.getTime());
			reqBegin.set(GregorianCalendar.HOUR, startTime.getHours());
			reqBegin.set(GregorianCalendar.MINUTE, startTime.getMinutes());

			GregorianCalendar reqEnd = new GregorianCalendar();
			reqEnd.setTimeInMillis(endDate.getTime());
			reqEnd.set(GregorianCalendar.HOUR, endTime.getHours());
			reqEnd.set(GregorianCalendar.MINUTE, endTime.getMinutes());
			
			GregorianCalendar endOfYear = new GregorianCalendar();
			endOfYear.set(GregorianCalendar.MONTH, 11);
			endOfYear.set(GregorianCalendar.DAY_OF_MONTH, 31);
			endOfYear.set(GregorianCalendar.YEAR, reqBegin.get(Calendar.YEAR));
			
			PtoRequestList pastRequests = getEmployeeRequests(employee.getId(), endOfYear.get(GregorianCalendar.YEAR));
			
			/**
			 * If they are coming back on the first working 
			 * day of the year, treat it as if they are coming 
			 * back new years eve. 
			 */
			if ((reqEnd.get(Calendar.MONTH) == 0) && (reqEnd.get(Calendar.DAY_OF_MONTH) == 4)) {
				reqEnd.set(Calendar.YEAR, reqBegin.get(Calendar.YEAR));
				reqEnd.set(Calendar.MONTH, 11);
				reqEnd.set(Calendar.DAY_OF_MONTH, 31);
			}

			GregorianCalendar prBegin = null;
			GregorianCalendar prEnd = null;

			for (PtoRequest pastReq : pastRequests.getRequests()) {
				// check if this request conflicts with a previously entered
				// request.
				// Skip canceled and rejected requests.
				if ((pastReq.getStatusID().equals(PtoRequestStatus.CANCELED)) ||
					(pastReq.getStatusID().equals(PtoRequestStatus.REJECTED))) {
					continue;
				}
				
				//Skip check if this is HR entering for someone else
				if ((employeeBean.isHR(requester)) && (requester.getId() != employee.getId())) {
					continue;
				}

				prBegin = new GregorianCalendar();
				prBegin.setTimeInMillis(pastReq.getStartDate().getTime());

				prBegin.set(GregorianCalendar.HOUR, pastReq.getStartTime().getHours());
				prBegin.set(GregorianCalendar.MINUTE, pastReq.getStartTime().getMinutes());

				prEnd = new GregorianCalendar();
				prEnd.setTimeInMillis(pastReq.getEndDate().getTime());

				prEnd.set(GregorianCalendar.HOUR, pastReq.getEndTime().getHours());
				prEnd.set(GregorianCalendar.MINUTE, pastReq.getEndTime().getMinutes());

				if (((reqBegin.compareTo(prBegin) >= 0)
					&& (reqBegin.compareTo(prEnd) < 0))
					||
					((reqEnd.compareTo(prBegin) > 0)
					&& (reqEnd.compareTo(prEnd) <= 0))) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd h:mm");

					return "Request failed.\nThis conflicts with a prior request:\n" +
						sdf.format(new Date(prBegin.getTimeInMillis())) + " - " + sdf.format(new Date(prEnd.getTimeInMillis()));
				}
			}

			// PtoGrantedHistory summ = getGrantedHistory(employee,
			// endOfYear.get(GregorianCalendar.YEAR));
			PtoSummary summ = getPtoSummary(employee, new java.sql.Date(endOfYear.getTime().getTime()));

			if (request.isOneToThree()) {
				log.trace("Request is a one-to-three.");
				calcTimeTaken = PtoRequest.calculateNumHours(employee, startDate, endDate, startTime, endTime);
				daysTaken = PtoRequest.calculateNumDays(employee, false, startDate, endDate, startTime, endTime);
				
				log.trace("Hours taken: " + calcTimeTaken + " Days taken: " + daysTaken);
				
				if ((daysTaken >= 1.0) || (calcTimeTaken > 3.0)) {
					// Cannot request more than 3 hours for a 1-3.
					return "Request failed. Cannot request more than 3 hours on a One-to-Three.";
				}
				
				log.trace("Hours: " + calcTimeTaken + " is less than 3.0");

				if ((daysTaken < .5) || (calcTimeTaken < 0.25)) {
					return "Request failed. Cannot enter negative time request.";
				}

				if (summ.getOneTo3Remaining() == 0) {
					return "fail";
				}

				daysMessage = "One to Three hours away: " + calcTimeTaken + "\n\n";
			} else {
				// calcTimeTaken = calculateNumDays(startDate, endDate,
				// startTime, endTime, employee.getId(), timeCatID);
				calcTimeTaken = PtoRequest.calculateNumDays(employee, request.isOneToThree(), startDate, endDate, startTime, endTime, request.getEmployeeStartTime(), request.getEmployeeEndTime());

				double considerAs = (daysTaken != calcTimeTaken ? daysTaken : calcTimeTaken);

				if (considerAs < 0.5) {
					return "Request failed. Cannot enter negative or zero day request.";
				}

				if ((request.isChargeable()) && (summ.getDaysRemaining() < considerAs)) {
					return "Request failed. Requested time exceeds remaining PTO.";
				}

				if (daysTaken != calcTimeTaken) {
					request.setActualDaysTaken(considerAs);
				}

				daysMessage = "Number of PTO days: " + (daysTaken == 0 ? calcTimeTaken : daysTaken) + "\n\n";
			}

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			request.setStartDate(startDate);
			request.setStartTime(startTime);
			request.setEndDate(endDate);
			request.setEndTime(endTime);
			request.setComment(this.getString(element, "comment", xpath));
			request.setEmployee(employee);
			request.setStatusID(PtoRequestStatus.PENDING);
			request.setCreator(requester);
			request.setRequestCreated(sdf.format(new java.util.Date()));
			request.setEmployeeStartTime(employee.getStartTime());
			request.setEmployeeEndTime(employee.getEndTime());

			entityManager.persist(request);

			String link = server + port + "/pto/requests.jsp?requestID=" + request.getId();

			String title = "" +
				"A PTO request has been entered for: " + request.getEmployee().getFullName() + " " +
				"starting: " + startDate;

			String message;

			if (requester.equals(employee)) {
				message = "" +
					"This is an automated message. Please do not reply.\n\n" +
					"A PTO Request has been entered for: " + employee.getFullName() + "\n" +
					"Request type: " + request.getRequestType().getTimeType().getDescription() + "\n" +
					"Date of Request: " + request.getRequestCreated() + " By: " + request.getCreator().getFullName() + "\n" +
					"Date leaving: " + startDate + " " + startTime + "\n" +
					"Date returning: " + endDate + " " + endTime + "\n\n" +
					"This time is counted as: " + requestType.getDescription() + " \n\n" +
					daysMessage +
					"Comment: " + request.getComment() + "\n\n";
			} else {
				message = "" +
					"This is an automated message. Please do not reply.\n\n" +
					"A PTO Request has been entered for: " + employee.getFullName() + "\n" +
					"Request type: " + request.getRequestType().getTimeType().getDescription() + "\n" +
					"Date of Request: " + request.getRequestCreated() + " By: " + request.getCreator().getFullName() + "\n" +
					"Date leaving: " + startDate + " " + startTime + "\n" +
					"Date returning: " + endDate + " " + endTime + "\n\n" +
					"This time is counted as: " + requestType.getDescription() + " \n\n" +
					daysMessage +
					"Comment: " + request.getComment() + "\n\n" +
					"Please follow this link to view the request " +
					"or contact Human Resources with any questions you may have: ";
			}

			String superMessage;

			if (requester.equals(employee)) {
				superMessage = message + "" +
					"Please follow this link to approve or reject the request, " +
					"or contact Human Resources with any questions you may have: ";
			} else {
				superMessage = message;
			}

			PtoEmployee dirReport = employee.getDirectReport();
			PtoEmployee ptoReport = employee.getPtoReport();
			PtoEmployee backupReport = employee.getBackupPtoReport();

			String email = employee.getEmail();

			messageBean.createEmail(email, title, link, message, PaLM.NO_ATTACHMENTS);

			boolean limitEmails = true;

			if (!limitEmails) {
				if ((dirReport != null) && (!dirReport.equals(requester))) {
					email = dirReport.getEmail();

					messageBean.createEmail(email, title, link, superMessage, PaLM.NO_ATTACHMENTS);
				}
			}

			if (!noEmails) {
				if ((ptoReport != null) && (! ptoReport.equals(requester))) {
					email = ptoReport.getEmail();

					messageBean.createEmail(email, title, link, superMessage, PaLM.NO_ATTACHMENTS);
				}
			}
			if (!limitEmails) {
				if ((backupReport != null) && (!backupReport.equals(requester))) {
					email = backupReport.getEmail();

					messageBean.createEmail(email, title, link, superMessage, PaLM.NO_ATTACHMENTS);
				}
			}

			if (request.getRequestType().getSpecialRouting()) {
				// Notify HR if request requires special routing.
				/**
				 * Now only HR can enter these types,
				 * so do not notify them when they enter.
				 */
				// messageBean.createHREmail(employee, title, link, message,
				// PaLM.NO_ATTACHMENTS);
			}
		} catch (Exception e) {
			log.fatal(e.getMessage(), e);

			return "fail";
		}

		return "done";
	}

	@Override
	public String processRequest(Element element, XPath xpath) {
		long ptoRequestID = this.getLong(element, "requestID", xpath);
		long currentEmployeeID = getLong(element, "currentEmployeeID", xpath);

		String processType = this.getString(element, "processType", xpath);

		PtoEmployee currentEmployee = entityManager.find(PtoEmployee.class, currentEmployeeID);

		PtoRequest request = entityManager.find(PtoRequest.class, ptoRequestID);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date today = getTodaySQL();
		Date startDate = request.getStartDate();

		String retVal = "failed";

		try {
			PtoEmployee emp = request.getEmployee();

			log.trace("Checking if user: " + currentEmployee.getFullName() + " is HR or a supervisor of: " + emp.getFullName());

			String superStatus = employeeBean.getSupervisor(emp.getId(), currentEmployeeID);

			if (("cancel").equals(processType)) {
				/**
				 * If request is AFTER today anyone can cancel,
				 * if request is TODAY HR or super can cancel,
				 * if request was BEFORE today, only HR can cancel
				 */
				log.trace("Super status: " + superStatus + " Start date: " + startDate + " today: " + today);

				if ((superStatus.equals("HR")) ||
					((superStatus.equals("super")) && ((startDate.after(today)) || (startDate.equals(today)))) ||
					(startDate.after(today))) {

					log.trace("Canceling request.");

					request.setStatusID(PtoRequestStatus.CANCELED);

					String link = server + port + "/pto/requests.jsp?requestID=" + request.getId();
					String title = "PTO Request Canceled for: " + request.getStartDate();
					String message = "A PTO Request has been canceled. Click the link to view the request.";

					messageBean.createEmail(request.getEmployee().getEmail(), title, link, message, PaLM.NO_ATTACHMENTS);

					// request.setAction("Canceled");
					// request.setActionBy(currentEmployee);
					// request.setActionDate(sdf.format(new java.util.Date()));

					request.setCanceledBy(currentEmployee);
					request.setCanceledDate(sdf.format(new java.util.Date()));

					retVal = "Request Canceled";
				}
			} else if (superStatus.equals("super") || superStatus.equals("HR")) {
				log.trace("User passes the sniff test.");

				if (("approve").equals(processType)) {
					log.trace("Approving request.");

					request.setStatusID(PtoRequestStatus.APPROVED);

					String link = server + port + "/pto/requests.jsp?requestID=" + request.getId();
					String title = "PTO Request Approved for: " + request.getStartDate();
					String message = "A PTO Request has been approved. Click the link to view the request.";

					messageBean.createEmail(request.getEmployee().getEmail(), title, link, message, PaLM.NO_ATTACHMENTS);

					// messageBean.sendInvitation(request);
					request.setAction("Approved");
					request.setActionBy(currentEmployee);
					request.setActionDate(sdf.format(new java.util.Date()));

					retVal = "Request Approved";
				} else if (("reject").equals(processType)) {
					log.trace("Rejecting request.");

					request.setStatusID(PtoRequestStatus.REJECTED);

					String link = server + port + "/pto/requests.jsp?requestID=" + request.getId();
					String title = "PTO Request Rejected for: " + request.getStartDate();
					String message = "A PTO Request has been rejected. Click the link to view the request.";

					messageBean.createEmail(request.getEmployee().getEmail(), title, link, message, PaLM.NO_ATTACHMENTS);

					request.setAction("Rejected");
					request.setActionBy(currentEmployee);
					request.setActionDate(sdf.format(new java.util.Date()));

					retVal = "Request Rejected";
				} else {
					log.fatal("Unprocessed request! " + ptoRequestID);
				}
			}
		} catch (Exception e) {
			log.fatal("Exception processing request action: " + e.getMessage(), e);
		}

		return retVal;
	}

	@Override
	public String calculateNumDays(Element element, XPath xpath) {
		Date startDate = this.getDate(element, "startDate", xpath);
		Date endDate = this.getDate(element, "endDate", xpath);

		Time startTime = stringToTime(this.getString(element, "startTime", xpath));
		Time endTime = stringToTime(this.getString(element, "endTime", xpath));

		log.trace("Incoming start time string: " + this.getString(element, "startTime", xpath));

		long empID = this.getLong(element, "employeeID", xpath);

		long requestTypeID = getLong(element, "requestType", xpath);

		PtoRequestType requestType = entityManager.find(PtoRequestType.class, requestTypeID);

		PtoEmployee employee = entityManager.find(PtoEmployee.class, empID);

		boolean isOneToThree = PtoRequest.isOneToThree(requestType);

		double numDays = PtoRequest.calculateNumDays(
			employee,
			isOneToThree,
			startDate,
			endDate,
			startTime,
			endTime,
			employee.getStartTime(),
			employee.getEndTime());

		return String.valueOf(numDays);
	}

	@Deprecated
	@Override
	public String calculateDates(Element element, XPath xpath) {
		/**
		 * Determine employee, their location and location specific calendar,
		 * then find the nearest date to startDate that is not a weekend,
		 * holiday, or outside current PTO year.
		 * return xml with 'endDate' and/or any applicable errors in 'errorMsg'.
		 * 
		 * THIS NEEDS FURTHER TESTING! DO NOT USE TODAY DATE IN GREG CALENDAR!
		 */

		StringBuilder retVal = new StringBuilder();

		try {
			retVal.append("<dateReply>");

			String eventType = this.getString(element, "eventType", xpath);

			Date startDate = this.getDate(element, "startDate", xpath);
			Date endDate = this.getDate(element, "endDate", xpath);

			Time startTime = stringToTime(this.getString(element, "startTime", xpath));
			Time endTime = stringToTime(this.getString(element, "endTime", xpath));

			long empID = this.getLong(element, "employeeID", xpath);

			long requestTypeID = getLong(element, "requestType", xpath);

			PtoRequestType requestType = entityManager.find(PtoRequestType.class, requestTypeID);

			String errorMsg = "";

			Date today = new Date(new java.util.Date(0).getTime());

			GregorianCalendar outputCal = new GregorianCalendar();
			GregorianCalendar startCal = new GregorianCalendar();
			GregorianCalendar endCal = new GregorianCalendar();
			GregorianCalendar todayCal = new GregorianCalendar();
			GregorianCalendar ptoHoliday = new GregorianCalendar();

			todayCal.setTime(today);

			PtoEmployee emp = entityManager.find(PtoEmployee.class, empID);
			PtoLocation loc = emp.getLocation();
			PtoCalendar ptoCal = loc.getCalendar();
			PtoCalendarYear ptoYear = ptoCal.getPtoYear(startCal.get(Calendar.YEAR));

			if (endDate == null) {
				endDate = startDate;
			}

			if (PtoRequest.isOneToThree(requestType)) {
				// process a 1-3 -- Max one consecutive, If ending time >= emp
				// end time then set
				// return to employee start time of following day.
				outputCal.setTime(startTime);
				endCal.setTime(startDate);

				GregorianCalendar empEndTime = new GregorianCalendar();
				empEndTime.setTime(emp.getEndTime());

				outputCal.add(Calendar.HOUR_OF_DAY, 3);
				empEndTime.add(Calendar.MINUTE, -1);

				if (outputCal.after(empEndTime)) {
					log.trace("Setting output to employee start time: " + emp.getStartTime());
					outputCal.setTime(emp.getStartTime());

					boolean dateValid = false;

					while (!dateValid) {
						endCal.add(Calendar.DAY_OF_MONTH, 1);

						if ((endCal.get(Calendar.DAY_OF_WEEK) != 1) && (endCal.get(Calendar.DAY_OF_WEEK) != 7)) {
							dateValid = true;
						}

						// check if its a holiday
						for (PtoCalendarDetail detail : ptoYear.getDetails()) {
							ptoHoliday.setTime(detail.getHolidayDate());

							if ((endCal.compareTo(ptoHoliday) == 0) && (!detail.isSummerHours())) {
								log.trace("Skipping a day for holiday: " + detail.getHolidayName());
								dateValid = false;
								break;
							}
						}
					}
				}

				endTime.setTime(outputCal.getTimeInMillis());
				endDate.setTime(endCal.getTimeInMillis());
			} else {
				// process PTO
				if ((("startDate").equals(eventType)) && (startDate.after(endDate))) {
					// Find a suitable ending date.
					outputCal.setTime(startDate);
					startCal.setTime(startDate);

					boolean dateValid = false;

					if (PtoRequest.isOneToThree(requestType)) {
						log.error("A non-one to three is somehow also showing up as a one to three...");
						log.error("This should never happen!!!!!!!!!!!");

						dateValid = true;
					}

					while (!dateValid) {
						outputCal.add(Calendar.DAY_OF_YEAR, 1);

						if (startCal.getTimeInMillis() < todayCal.getTimeInMillis()) {
							errorMsg = "Start date cannot be in the past.";
							break;
						}

						if (outputCal.get(Calendar.YEAR) != startCal.get(Calendar.YEAR)) {
							errorMsg = "Cannot find a valid end date this year.";
							break;
						}

						// check if its a weekend
						if ((outputCal.get(Calendar.DAY_OF_WEEK) != 1) && (outputCal.get(Calendar.DAY_OF_WEEK) != 7)) {
							dateValid = true;
						}

						// check if its a holiday
						for (PtoCalendarDetail detail : ptoYear.getDetails()) {
							ptoHoliday.setTime(detail.getHolidayDate());

							/**
							 * Perhaps this should not allow employee to return
							 * after their start time on a summer hours day
							 * -- if they participate in summer hours.
							 */
							if ((startCal.compareTo(ptoHoliday) == 0) && (!detail.isSummerHours())) {
								log.trace("Skipping a day for holiday: " + detail.getHolidayName());
								dateValid = false;
								break;
							}
						}

						// If its the last day of the year, let them return
					}

					endDate = new Date(outputCal.getTimeInMillis());
				} else if (("endDate").equals(eventType) && (startDate.after(endDate))) {
					outputCal.setTime(endDate);
					endCal.setTime(endDate);
					
					/**
					 * If they are coming back on the first working 
					 * day of the year, treat it as if they are coming 
					 * back new years eve. 
					 */
					log.trace("Checking to see if employee is returning first working day of the year.");
					if ((endCal.get(Calendar.MONTH) == 0) && (endCal.get(Calendar.DAY_OF_MONTH) == 4)) {
						log.trace("Verified employee returning first day of year.");
						endCal.set(Calendar.YEAR, startCal.get(Calendar.YEAR));
						endCal.set(Calendar.MONTH, 11);
						endCal.set(Calendar.DAY_OF_MONTH, 31);
						log.trace("Set end date to 12/31/" + startCal.get(Calendar.YEAR));
						
					}

					boolean dateValid = false;

					while (!dateValid) {
						outputCal.add(Calendar.DAY_OF_YEAR, -1);

						if (endCal.getTimeInMillis() < todayCal.getTimeInMillis()) {
							errorMsg = "End date cannot be in the past.";

							break;
						}

						if (outputCal.get(Calendar.YEAR) != endCal.get(Calendar.YEAR)) {
							errorMsg = "Cannot find a valid start date this year.";

							break;
						}

						// check if its a weekend
						if ((outputCal.get(Calendar.DAY_OF_WEEK) != 1) && (outputCal.get(Calendar.DAY_OF_WEEK) != 7)) {
							dateValid = true;
						}

						// check if its a holiday
						for (PtoCalendarDetail detail : ptoYear.getDetails()) {
							ptoHoliday.setTime(detail.getHolidayDate());

							if ((endCal.compareTo(ptoHoliday) == 0) && (!detail.isSummerHours())) {
								log.trace("Skipping a day for holiday: " + detail.getHolidayName());

								dateValid = false;

								break;
							}
						}
					}

					startDate = new Date(outputCal.getTimeInMillis());
				}
			}

			boolean isOneToThree = PtoRequest.isOneToThree(requestType);

			double numDays = PtoRequest.calculateNumDays(
				emp,
				isOneToThree,
				startDate,
				endDate,
				startTime,
				endTime,
				emp.getStartTime(),
				emp.getEndTime());

			retVal.append("<startDate>" + startDate.toString() + "</startDate>");
			retVal.append("<endDate>" + endDate.toString() + "</endDate>");
			retVal.append("<startTime>" + startTime.toString() + "</startTime>");
			retVal.append("<endTime>" + endTime.toString() + "</endTime>");
			retVal.append("<numDays>" + numDays + "</numDays>");
			retVal.append("<errorMsg>" + errorMsg + "</errorMsg>");
			retVal.append("</dateReply>");

		} catch (Exception e) {
			log.error("Unexpected Exception: " + e.getMessage(), e);
		}

		log.trace("XML: " + retVal.toString());

		return retVal.toString();
	}

	@Override
	public String calculateEndDate(Element element, XPath xpath) {
		// determine employee, their location and location specific calendar.
		// then find the nearest date to startDate that is not a weekend,
		// holiday, or outside current PTO year.
		// return xml with 'endDate' and/or any applicable errors in 'errorMsg'.

		StringBuilder retVal = new StringBuilder();

		try {
			retVal.append("<dateReply>");

			Date startDate = this.getDate(element, "startDate", xpath);
			long empID = this.getLong(element, "employeeID", xpath);

			long requestTypeID = getLong(element, "requestType", xpath);

			PtoRequestType requestType = entityManager.find(PtoRequestType.class, requestTypeID);

			Date today = new Date(new java.util.Date(0).getTime());
			Date endDate = null;

			GregorianCalendar workCal = new GregorianCalendar();
			GregorianCalendar startCal = new GregorianCalendar();
			GregorianCalendar todayCal = new GregorianCalendar();
			GregorianCalendar ptoHoliday = new GregorianCalendar();

			workCal.setTime(startDate);
			startCal.setTime(startDate);
			todayCal.setTime(today);

			PtoEmployee emp = entityManager.find(PtoEmployee.class, empID);
			PtoLocation loc = emp.getLocation();
			PtoCalendar ptoCal = loc.getCalendar();

			log.trace("Got calendarID: " + ptoCal.getId());

			PtoCalendarYear ptoYear = ptoCal.getPtoYear(startCal.get(Calendar.YEAR));

			String errorMsg = "";
			boolean dateValid = false;

			if (PtoRequest.isOneToThree(requestType)) {
				// leave it today
				dateValid = true;
			}

			while (!dateValid) {
				workCal.add(Calendar.DAY_OF_YEAR, 1);

				if (startCal.getTimeInMillis() < todayCal.getTimeInMillis()) {
					errorMsg = "Start date cannot be in the past.";

					break;
				}

				if (workCal.get(Calendar.YEAR) != startCal.get(Calendar.YEAR)) {
					errorMsg = "Cannot find a valid end date this year.";

					break;
				}

				// check if its a weekend
				if ((workCal.get(Calendar.DAY_OF_WEEK) != 1) && (workCal.get(Calendar.DAY_OF_WEEK) != 7)) {
					dateValid = true;
				}

				// check if its a holiday
				for (PtoCalendarDetail detail : ptoYear.getDetails()) {
					ptoHoliday.setTime(detail.getHolidayDate());

					if ((startCal.compareTo(ptoHoliday) == 0) && (!detail.isSummerHours())) {
						log.trace("Skipping a day for holiday: " + detail.getHolidayName());

						dateValid = false;

						break;
					}
				}
			}

			endDate = new Date(workCal.getTimeInMillis());

			retVal.append("<endDate>" + endDate.toString() + "</endDate>");
			retVal.append("<errorMsg>" + errorMsg + "</errorMsg>");
			retVal.append("</dateReply>");
		} catch (Exception e) {
			log.error(e);

			e.printStackTrace();
		}

		log.trace("XML: " + retVal.toString());

		return retVal.toString();
	}

	@Override
	public String calculateStartDate(Element element, XPath xpath) {
		// determine employee, their location and location specific calendar.
		// then find the nearest date to startDate that is not a weekend,
		// holiday, or outside current PTO year.
		// return xml with 'endDate' and/or any applicable errors in 'errorMsg'.

		StringBuilder retVal = new StringBuilder();

		try {
			retVal.append("<dateReply>");

			Date endDate = this.getDate(element, "endDate", xpath);

			long empID = this.getLong(element, "employeeID", xpath);

			Date today = new Date(new java.util.Date(0).getTime());
			Date startDate = null;

			GregorianCalendar workCal = new GregorianCalendar();
			GregorianCalendar endCal = new GregorianCalendar();
			GregorianCalendar todayCal = new GregorianCalendar();
			GregorianCalendar ptoHoliday = new GregorianCalendar();

			workCal.setTime(endDate);
			endCal.setTime(endDate);
			todayCal.setTime(today);

			PtoEmployee emp = entityManager.find(PtoEmployee.class, empID);
			PtoLocation loc = emp.getLocation();
			PtoCalendar ptoCal = loc.getCalendar();
			PtoCalendarYear ptoYear = ptoCal.getPtoYear(endCal.get(Calendar.YEAR));

			String errorMsg = "";

			boolean dateValid = false;

			while (!dateValid) {
				workCal.add(Calendar.DAY_OF_YEAR, -1);

				if (endCal.getTimeInMillis() < todayCal.getTimeInMillis()) {
					errorMsg = "End date cannot be in the past.";

					break;
				}

				if (workCal.get(Calendar.YEAR) != endCal.get(Calendar.YEAR)) {
					errorMsg = "Cannot find a valid start date this year.";

					break;
				}

				// check if its a weekend
				if ((workCal.get(Calendar.DAY_OF_WEEK) != 1) && (workCal.get(Calendar.DAY_OF_WEEK) != 7)) {
					dateValid = true;
				}

				// check if its a holiday
				for (PtoCalendarDetail detail : ptoYear.getDetails()) {
					ptoHoliday.setTime(detail.getHolidayDate());

					if ((endCal.compareTo(ptoHoliday) == 0) && (!detail.isSummerHours())) {
						log.trace("Skipping a day for holiday: " + detail.getHolidayName());

						dateValid = false;

						break;
					}
				}
			}

			startDate = new Date(workCal.getTimeInMillis());

			retVal.append("<startDate>" + startDate.toString() + "</startDate>");
			retVal.append("<errorMsg>" + errorMsg + "</errorMsg>");
			retVal.append("</dateReply>");
		} catch (Exception e) {
			log.error("Unexpected exception: " + e.getMessage(), e);
		}

		log.trace("XML: " + retVal.toString());

		return retVal.toString();
	}

	@Deprecated
	@Override
	public String calculateEndTime(Element element, XPath xpath) {
		StringBuilder retVal = new StringBuilder();

		Time startTime = stringToTime(this.getString(element, "startTime", xpath));
		GregorianCalendar workCal = new GregorianCalendar();
		GregorianCalendar empEndTime = new GregorianCalendar();

		long empID = this.getLong(element, "employeeID", xpath);

		long requestTypeID = getLong(element, "requestType", xpath);

		PtoRequestType requestType = entityManager.find(PtoRequestType.class, requestTypeID);

		PtoEmployee emp = entityManager.find(PtoEmployee.class, empID);

		workCal.setTime(startTime);
		empEndTime.setTime(emp.getEndTime());

		String errorMsg = "";

		retVal.append("<dateReply>");

		if (PtoRequest.isOneToThree(requestType)) {
			workCal.add(Calendar.HOUR_OF_DAY, 3);
		}

		if (workCal.after(empEndTime)) {
			workCal.setTime(emp.getStartTime());
		}

		retVal.append("" +
			"<endTime>" +
			workCal.get(Calendar.HOUR_OF_DAY) + ":" +
			workCal.get(Calendar.MINUTE) + ":00" +
			"</endTime>");

		retVal.append("<errorMsg>" + errorMsg + "</errorMsg>");
		retVal.append("</dateReply>");

		log.trace("Returning End Time XML: " + retVal.toString());

		return retVal.toString();
	}

	@Override
	public String validateDate(Element element, XPath xpath) {
		// Date today = new Date(new java.util.Date(0).getTime());
		GregorianCalendar todayCal = new GregorianCalendar();
		todayCal.set(Calendar.HOUR_OF_DAY, 0);
		todayCal.set(Calendar.MINUTE, 0);
		todayCal.set(Calendar.SECOND, 0);

		Date theDate = this.getDate(element, "theDate", xpath);
		GregorianCalendar workCal = new GregorianCalendar();
		workCal.setTime(theDate);
		workCal.set(Calendar.HOUR_OF_DAY, 6);

		GregorianCalendar ptoHoliday = new GregorianCalendar();

		StringBuilder retVal = new StringBuilder();
		String errorMsg = "";

		try {
			long empID = this.getLong(element, "empID", xpath);

			log.trace("Employee ID: " + empID);

			PtoEmployee emp = entityManager.find(PtoEmployee.class, empID);

			log.trace("Found employee: " + emp.getFullName());

			PtoLocation loc = emp.getLocation();

			log.trace("Found location: " + loc.getName());

			PtoCalendar ptoCal = loc.getCalendar();

			log.trace("Found calendar: " + ptoCal.getCalendarName());

			PtoCalendarYear ptoYear = ptoCal.getPtoYear(workCal.get(Calendar.YEAR));
			
			long currentUser = this.getLong(element, "currentEmployeeID", xpath);
			
			log.info("Got current user ID: " + currentUser);

			retVal.append("<dateReply>");

			boolean dateValid = false;
			boolean notWeekend = false;
			boolean notHoliday = false;
			boolean notPast = false;

			log.trace("Beginning while loop!");

			// log.trace("WorkCal: " + workCal + " Today: " + todayCal);

			while (!dateValid) {
				notPast = true;

				if (workCal.get(Calendar.YEAR) < todayCal.get(Calendar.YEAR)) {
					if (("").equals(errorMsg)) {
						errorMsg = " date cannot be in a previous year.";
						notPast = false;

						workCal.setTimeInMillis(todayCal.getTimeInMillis() - (1000 * 60 * 60 * 24));
					}
				}

				if ((workCal.before(todayCal)) && (!employeeBean.isHR(currentUser))) {
					if (("").equals(errorMsg)) {
						errorMsg = " date cannot be in the past.";
						notPast = false;
					}

					workCal.setTimeInMillis(todayCal.getTimeInMillis() - (1000 * 60 * 60 * 24));
				}

				if (workCal.get(Calendar.DAY_OF_WEEK) == 7) {
					if (("").equals(errorMsg)) {
						errorMsg = " date cannot be on Saturday.";
					}
				}

				if (workCal.get(Calendar.DAY_OF_WEEK) == 1) {
					if (("").equals(errorMsg)) {
						errorMsg = " date cannot be on Sunday.";
					}
				}

				if ((workCal.get(Calendar.DAY_OF_WEEK) != 1) && (workCal.get(Calendar.DAY_OF_WEEK) != 7)) {
					notWeekend = true;
				}

				notHoliday = true;

				if (ptoYear != null) {
					for (PtoCalendarDetail detail : ptoYear.getDetails()) {
						ptoHoliday.setTime(detail.getHolidayDate());

						if ((workCal.compareTo(ptoHoliday) == 0)
							&& (!detail.isSummerHours())) {
							if (("").equals(errorMsg)) {
								errorMsg = " date cannot be on a company holiday.";
							}
							notHoliday = false;
							break;
						}
					}
				} else {
					errorMsg = " PTO Calendar has not been created, contact Human Resources.";
				}

				if (notWeekend && notHoliday && notPast) {
					dateValid = true;
				} else {
					workCal.add(GregorianCalendar.DAY_OF_YEAR, 1);
				}
			}

			Date validDate = new Date(workCal.getTimeInMillis());

			retVal.append("<validDate>" + validDate.toString() + "</validDate>");
			retVal.append("<errorMsg>" + errorMsg + "</errorMsg>");
			retVal.append("</dateReply>");
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}

		log.trace("XML: " + retVal.toString());

		return retVal.toString();
	}

	@Override
	public PtoRequestList getEmployeeRequests(long empID, int year) {
		PtoEmployee employee = entityManager.find(PtoEmployee.class, empID);

		GregorianCalendar firstOfYear = new GregorianCalendar();
		GregorianCalendar endOfYear = new GregorianCalendar();

		firstOfYear.set(GregorianCalendar.MONTH, 0);
		firstOfYear.set(GregorianCalendar.DAY_OF_MONTH, 1);

		endOfYear.set(GregorianCalendar.MONTH, 11);
		endOfYear.set(GregorianCalendar.DAY_OF_MONTH, 31);

		if (year != 0) {
			firstOfYear.set(GregorianCalendar.YEAR, year);
			endOfYear.set(GregorianCalendar.YEAR, year);
		}

		String sql = "" +
			"SELECT r FROM PtoRequest r " +
			"WHERE (r.employee.id = :empID) AND " +
			"((r.startDate > :firstOfYear) AND " +
			"(r.startDate < :endOfYear)) " +
			"ORDER BY r.startDate";

		log.trace("Getting request history for: " + employee.getFullName());

		Query query = entityManager.createQuery(sql);

		query.setParameter("empID", empID);
		query.setParameter("firstOfYear", new Date(firstOfYear.getTimeInMillis()));
		query.setParameter("endOfYear", new Date(endOfYear.getTimeInMillis()));

		@SuppressWarnings("unchecked") List<PtoRequest> results = query.getResultList();

		log.trace("Query complete!");

		PtoRequestList requests = new PtoRequestList();

		for (PtoRequest req : results) {
			log.trace("Adding a request!");

			requests.addRequest(req);
		}

		return requests;
	}

	@Override
	public String getEmployeeHistory(Element element, XPath xpath) {
		long employeeID = getLong(element, "employeeID", xpath);

		int year = getInt(element, "year", xpath);

		GregorianCalendar today = new GregorianCalendar();

		log.trace("Getting employee PTO Request history.");

		PtoEmployee employee = entityManager.find(PtoEmployee.class, employeeID);

		StringWriter xml = null;

		GregorianCalendar firstOfYear = new GregorianCalendar();
		GregorianCalendar endOfYear = new GregorianCalendar();

		firstOfYear.set(GregorianCalendar.MONTH, 0);
		firstOfYear.set(GregorianCalendar.DAY_OF_MONTH, 1);

		endOfYear.set(GregorianCalendar.MONTH, 11);
		endOfYear.set(GregorianCalendar.DAY_OF_MONTH, 31);

		if (year != 0) {
			firstOfYear.set(GregorianCalendar.YEAR, year);
			endOfYear.set(GregorianCalendar.YEAR, year);
		}

		log.trace("" +
			"First of this year: " + new Date(firstOfYear.getTimeInMillis()) +
			" End of this year: " + new Date(endOfYear.getTimeInMillis()));

		try {
			PtoRequestList requests = getEmployeeRequests(employeeID, year);

			xml = new StringWriter();

			if (year == 0) {
				year = today.get(GregorianCalendar.YEAR);
			}

			/**
			 * This should just use granted history as the reference.
			 * Get rid of PtoSummary, and remove the fields from Employee.
			 * When they update Employee record, it will update the current year
			 * of History.
			 * Past years cannot be altered. XML will just have History record
			 * for use in populating time available for this year, as well as
			 * for others.
			 * 
			 */
			try {
				PtoGrantedHistory grantedHist = getGrantedHistory(employee, year);

				if (grantedHist == null) {
					log.info("" +
						"Granted history does not exist for employee: " +
						employee.getFullName() + " and year: " + year);

					grantedHist = new PtoGrantedHistory(employee, year);
				}

				if ((year == 0) || (year == today.get(GregorianCalendar.YEAR))) {
					grantedHist.setBasePTO(employeeBean.getBasePTO(employeeID));
					grantedHist.setBaseOneToThree(employeeBean.getBaseOneTo3(employeeID));
				} else {
//					grantedHist.setBasePTO(5.0);
//					grantedHist.setBaseOneToThree(8);
				}

				requests.setGrantedHistory(grantedHist);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}

			log.trace("Done adding requests, marshalling the XML.");

			try {
				JAXBContext context = JAXBContext.newInstance(requests.getClass());

				Marshaller m = context.createMarshaller();

				m.marshal(requests, xml);
			} catch (JAXBException je) {
				log.fatal("JAXB Exception: " + je.getMessage(), je);
			}

			// log.trace("Request History XML: " + fixXML(xml));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fixXML(xml);
	}

	public String getPendingRequests(long employeeID) {
		log.trace("Getting pending requests for user reportees.");

		PtoEmployee emp = entityManager.find(PtoEmployee.class, employeeID);

		StringWriter xml = null;

		GregorianCalendar firstOfYear = new GregorianCalendar();
		GregorianCalendar endOfYear = new GregorianCalendar();

		firstOfYear.set(GregorianCalendar.MONTH, 0);
		firstOfYear.set(GregorianCalendar.DAY_OF_MONTH, 1);

		endOfYear.set(GregorianCalendar.MONTH, 11);
		endOfYear.set(GregorianCalendar.DAY_OF_MONTH, 31);
		endOfYear.add(GregorianCalendar.YEAR, 1); // Look at next year, too

		log.trace("First of this year: " + new Date(firstOfYear.getTimeInMillis()) +
			" End of next year: " + new Date(endOfYear.getTimeInMillis()));

		List<PtoEmployee> subjects = employeeBean.getReportees(emp.getId());

		// Set to true to show user's own requests on home page.
		boolean showSelf = false;

		if (showSelf) {
			subjects.add(emp);
		}

		try {
			String sql = "" +
				"SELECT r FROM PtoRequest r " +
				"WHERE (r.employee.id = :empID) AND " +
				"((r.startDate > :firstOfYear) AND " +
				"(r.startDate < :endOfYear)) AND " +
				"(r.statusID = 1) " + // Pending ID
				"ORDER BY r.startDate";

			log.trace("Attempting the query.");

			PtoRequestList requests = new PtoRequestList();

			for (PtoEmployee employee : subjects) {
				Query query = entityManager.createQuery(sql);

				query.setParameter("empID", employee.getId());
				query.setParameter("firstOfYear", new Date(firstOfYear.getTimeInMillis()));
				query.setParameter("endOfYear", new Date(endOfYear.getTimeInMillis()));

				@SuppressWarnings("unchecked") List<PtoRequest> results = query.getResultList();

				log.trace("Query complete!");

				// double totalTaken = 0;
				// int oneTo3Taken = 0;

				for (PtoRequest req : results) {
					log.trace("Adding a request!");

					requests.addRequest(req);
				}
			}

			xml = new StringWriter();

			log.trace("Done adding requests, marshalling the XML.");

			try {
				JAXBContext context = JAXBContext.newInstance(requests.getClass());

				Marshaller m = context.createMarshaller();

				m.marshal(requests, xml);
			} catch (JAXBException je) {
				log.fatal("JAXB Exception: " + je.getMessage(), je);
			}

			log.info("Request History XML: " + fixXML(xml));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fixXML(xml);
	}

	public PtoRequestList getAbsenceList(Date date) {
		log.trace("Generating report for date: " + date.getTime());

		PtoRequestList requests = new PtoRequestList();

		try {
			String sql = "" +
				"SELECT r FROM PtoRequest r " +
				"WHERE " +
				"((r.startDate <= :reportDate) AND " +
				"(r.endDate >= :reportDate)) " +
				"ORDER BY r.employee.firstName, r.startDate, r.startTime";

			Query query = entityManager.createQuery(sql);

			query.setParameter("reportDate", date);

			@SuppressWarnings("unchecked") List<PtoRequest> results = query.getResultList();

			log.trace("Query complete!");

			for (PtoRequest req : results) {
				if ((req.getStatusID() == PtoRequestStatus.CANCELED) || (req.getStatusID() == PtoRequestStatus.REJECTED)) {
					continue;
				}

				if (req.getEmployee().getTerminationDate() != null) {
					continue;
				}

				log.trace("Adding a request!");

				try {
					log.trace("Testing end date: " + req.getEndDate() + " and end time: " + req.getEndTime());

					if (req.getEndDate().getTime() == date.getTime() &&
						req.getEmployee().getStartTime().getTime() == req.getEndTime().getTime()) {

						log.trace("Skipping request " + req.getId() + ", it doesn't belong on this day's report.");

						continue;
					}

					log.info("Adding request: " + req.getId());

					requests.addRequest(req);
				} catch (NullPointerException npe) {
					log.error("Null Pointer Exception: " + npe.getMessage(), npe);
				} catch (Exception e) {
					log.error("Unexpected exception: " + e.getMessage(), e);
				}
			}
		} catch (Exception e) {
			log.error("Unexpected exception: " + e.getMessage(), e);
		}

		log.trace("Returning: " + requests.getRequests().size() + " requests.");

		return requests;
	}

	public PtoRequestList getRequestException(Date startDate, Date endDate) {
		PtoRequestList requests = new PtoRequestList();

		String sql = "" +
			"SELECT r FROM PtoRequest r " +
			"WHERE " +
			"((r.startDate >= :startDate) AND " +
			"(r.startDate <= :endDate)) OR " +
			"((r.endDate >= :startDate) AND " +
			"(r.endDate <= :endDate)) OR" +
			"((r.endDate >= :startDate) AND " +
			"(r.startDate <= :endDate)) " +
			"ORDER BY r.employee.lastName";

		Query query = entityManager.createQuery(sql);

		query.setParameter("startDate", startDate);
		query.setParameter("endDate", endDate);

		@SuppressWarnings("unchecked") List<PtoRequest> results = query.getResultList();

		log.trace("Query complete!");

		// Filter out Unpaid types.
		log.trace("Filter out the unpaid types (include only unpaid)...");
		log.trace("Got " + results.size() + " results. ");

		for (PtoRequest req : results) {
			log.trace("Request (" + req.getId() + ") - Category: " + req.getTimeTypeCategoryDescription());

			// if (req.isChargeable()) {
			// log.trace("Chargeable request...skip...");
			//
			// continue;
			// }

			if (!req.isUnpaid()) {
				log.trace("Not an unpaid request...skip...");

				continue;
			}

			if (req.isDead()) {
				continue;
			}

			log.trace("Testing to make sure that the request is in the date range...");

			if ((req.getStartDate() == null) || (req.getEndDate() == null)) {
				log.trace("The request date range is corrupt...skip...");

				continue;
			}

			if (endDate.before(req.getStartDate())) {
				log.trace("Report date ends before request...skip...");

				continue;
			}

			if (startDate.after(req.getEndDate())) {
				log.trace("Report starts after request ends...skip...");

				continue;
			}

			log.trace("Adding a request!");

			requests.addRequest(req);
		}

		log.trace("Returning: " + requests.getRequests().size() + " requests.");

		return requests;
	}
	
	@Override
	public List<PtoSummary> getUnusedPtoList(Date reportDate) {
		return getUnusedPtoList(reportDate, false);
	}

	@Override
	public List<PtoSummary> getUnusedPtoList(Date reportDate, boolean allEmployees) {
		List<PtoSummary> summaries = new ArrayList<PtoSummary>();
		List<PtoEmployee> employees;
		
		if (allEmployees) {
			Calendar d = Calendar.getInstance();
			d.setTime(reportDate);
			employees = employeeBean.allEmployeeListing(d.get(Calendar.YEAR));
		} else {
			employees = employeeBean.activeEmployeeListing(reportDate);
		}

		for (PtoEmployee employee : employees) {
			summaries.add(getPtoSummary(employee, reportDate));
		}

		return summaries;
	}

	@Override
	public PtoSummary getPtoSummary(PtoEmployee employee, Date reportDate) {
		PtoSummary summ = new PtoSummary();

		GregorianCalendar dateCal = new GregorianCalendar();

		if (reportDate != null) {
			dateCal.setTimeInMillis(reportDate.getTime());
		}

		PtoGrantedHistory grantedHistory = getGrantedHistory(employee, dateCal.get(GregorianCalendar.YEAR));

		employee.setGrantedHistory(grantedHistory);

		summ.setEmployee(employee);

		GregorianCalendar firstOfYear = new GregorianCalendar();
		GregorianCalendar endOfYear = new GregorianCalendar();

		firstOfYear.setTimeInMillis(reportDate.getTime());
		firstOfYear.set(GregorianCalendar.MONTH, 0);
		firstOfYear.set(GregorianCalendar.DAY_OF_MONTH, 1);

		endOfYear.setTimeInMillis(reportDate.getTime());

		log.info("Comapring report date: " + reportDate + " to todaySQL: " + getTodaySQL());

		if (reportDate.compareTo(getTodaySQL()) == 0) {
			log.info("Dates match!");

			endOfYear.set(GregorianCalendar.MONTH, 11);
			endOfYear.set(GregorianCalendar.DAY_OF_MONTH, 31);
		}

		log.trace("First of report year: " + new Date(firstOfYear.getTimeInMillis()) +
			" End of this report: " + new Date(endOfYear.getTimeInMillis()));

		try {
			String sql = "" +
				"SELECT r FROM PtoRequest r " +
				"WHERE (r.employee.id = :empID) AND " +
				"((r.startDate > :firstOfYear) AND " +
				"(r.startDate < :endOfYear)) " +
				"ORDER BY r.startDate";

			log.trace("Attempting the query.");

			Query query = entityManager.createQuery(sql);

			query.setParameter("empID", employee.getId());
			query.setParameter("firstOfYear", new Date(firstOfYear.getTimeInMillis()));
			query.setParameter("endOfYear", new Date(endOfYear.getTimeInMillis()));

			@SuppressWarnings("unchecked") List<PtoRequest> results = query.getResultList();

			log.trace("Query complete!");

			double totalTaken = 0.0;
			double totalUnpaidTaken = 0.0;
			double totalCompTaken = 0.0;
			double totalNonCharge = 0.0;

			int oneTo3Taken = 0;
			int unpaidOneTo3Taken = 0;

			for (PtoRequest req : results) {
				log.trace("Adding a request!");

				try {
					long timeCat = req.getRequestType().getTimeType().getTimeTypeCategory().getId();

					log.trace("Time category ID: " + timeCat);

					if (timeCat == PtoTimeTypeCategory.ONE2THREE) {
						log.trace("Adding a 1-3.");

						if ((req.getStatusID() != PtoRequestStatus.CANCELED) &&
							(req.getStatusID() != PtoRequestStatus.REJECTED)) {
							oneTo3Taken++;
						}
					} else if (timeCat == PtoTimeTypeCategory.PTO_CHARGEABLE) {
						log.trace("Checking request status: " + req.getStatusID());

						if ((req.getStatusID() != PtoRequestStatus.CANCELED) &&
							(req.getStatusID() != PtoRequestStatus.REJECTED)) {
							totalTaken += req.getDaysTaken();
						}
					} else if (timeCat == PtoTimeTypeCategory.PTO_NONCHARGEABLE) {
						log.trace("Checking request status: " + req.getStatusID());

						if ((req.getStatusID() != PtoRequestStatus.CANCELED) &&
							(req.getStatusID() != PtoRequestStatus.REJECTED)) {
							totalNonCharge += req.getDaysTaken();
						}
					} else if (timeCat == PtoTimeTypeCategory.COMP_TIME) {
						log.trace("Checking request status: " + req.getStatusID());

						if ((req.getStatusID() != PtoRequestStatus.CANCELED) &&
							(req.getStatusID() != PtoRequestStatus.REJECTED)) {
							totalCompTaken += req.getDaysTaken();
						}
					} else if (timeCat == PtoTimeTypeCategory.UNPAID) {
						log.trace("Checking request status: " + req.getStatusID());

						if ((req.getStatusID() != PtoRequestStatus.CANCELED) &&
							(req.getStatusID() != PtoRequestStatus.REJECTED)) {
							totalUnpaidTaken += req.getDaysTaken();
						}
					} else if (timeCat == PtoTimeTypeCategory.UNPAID_ONE2THREE) {
						log.trace("Checking request status: " + req.getStatusID());

						if ((req.getStatusID() != PtoRequestStatus.CANCELED) &&
							(req.getStatusID() != PtoRequestStatus.REJECTED)) {
							unpaidOneTo3Taken += req.getDaysTaken();
						}
					}
				} catch (NullPointerException npe) {
					log.error("Null pointer exception retrieving request summary: " + npe.getMessage(), npe);
				} catch (Exception e) {
					log.error("Unexpected exception retrieving request summary: " + e.getMessage(), e);
				}
			}

			// double employeeDays = employeeBean.getBasePTO(employee.getId()) +
			// employee.getRolloverDays() + employee.getPtoAdjustment();
			// // double employeeCompTime = employee.getCompTimeGranted();
			// int employeeOneTo3 = employeeBean.getBaseOneTo3(employee.getId())
			// + employee.getBonusOneToThree();

			log.trace("Days requested: " + totalTaken + " One-to-Threes: " + oneTo3Taken);

			// summ.setStartingDaysPTO(employeeDays);
			summ.setDaysRequested(totalTaken);
			// summ.setDaysRemaining(employeeDays - totalTaken);

			summ.setOneToThreeTaken(oneTo3Taken);
			// summ.setOneTo3Granted(employeeOneTo3);
			// summ.setOneTo3Remaining(employeeOneTo3 - oneTo3Taken);

			// summ.setCompTimeGranted(employeeCompTime);
			summ.setCompTimeTaken(totalCompTaken);
			// summ.setCompTimeRemaining(employeeCompTime - totalCompTaken);

			summ.setNonChargeableTaken(totalNonCharge);
			summ.setUnpaidTimeTaken(totalUnpaidTaken);
			summ.setUnpaidOneToThreeTaken(unpaidOneTo3Taken);
		} catch (Exception e) {
			log.error("Unexpected exception: " + e.getMessage(), e);
		}

		return summ;
	}

	@Override
	public PtoGrantedHistory getGrantedHistory(PtoEmployee emp, Integer year) {
		PtoGrantedHistory grantedHistory = null;

		if ((year == null) || (year.equals(0))) {
			Calendar today = GregorianCalendar.getInstance();

			year = today.get(GregorianCalendar.YEAR);
		}

		log.trace("Attempting to grab granted history for employee: " + emp.getFullName());
		log.trace("And for year: " + year);

		String sql = "" +
			"SELECT g FROM PtoGrantedHistory g WHERE " +
			"(g.employee = :emp) AND " +
			"(g.year = :year)";

		Query query = entityManager.createQuery(sql);

		query.setParameter("emp", emp);
		query.setParameter("year", year);
		query.setMaxResults(1);

		if (query.getResultList().isEmpty()) {
			log.error("No granted history found for that employee/year...");

			grantedHistory = new PtoGrantedHistory(emp, year);
			entityManager.persist(grantedHistory);
		} else {
			grantedHistory = (PtoGrantedHistory) query.getSingleResult();
		}

		GregorianCalendar cal = new GregorianCalendar();
//		cal.set(GregorianCalendar.YEAR, year);
		cal.set(GregorianCalendar.MONTH, 11);
		cal.set(GregorianCalendar.DAY_OF_MONTH, 31);

		if (year == cal.get(GregorianCalendar.YEAR)) {
			grantedHistory.setBasePTO(employeeBean.getBasePTO(emp.getId()));
			grantedHistory.setBaseOneToThree(employeeBean.getBaseOneTo3(emp.getId()));
		} else { // Requesting for next year before the rollover has happened, give them some base.
//			grantedHistory.setBasePTO(15.0);
//			grantedHistory.setBaseOneToThree(8);
		}

		return grantedHistory;
	}

	public String createAttendancePDF(Date reportDate) {
		String fileName = "/dailyAttendanceReport.pdf";
		File file = new File(fileName);

		if (file.exists()) {
			file.delete();
		}

		Phrase workPhrase = new Phrase();
		String work = "";

		int tableCols = 17;

		try {
			Document document = new Document(PageSize.LETTER, 10, 10, 70, 70);
			Rectangle page = document.getPageSize();

			FileOutputStream fos = new FileOutputStream(fileName);

			@SuppressWarnings("unused") PdfWriter writer = PdfWriter.getInstance(document, fos);

			// writer.setPageEvent(new MakePDF(session));

			document.open();

			// set up an empty cell for filler
			PdfPCell emptyCell = new PdfPCell();
			emptyCell.setBorder(Rectangle.NO_BORDER);

			// set up a blank line cell for filler
			PdfPCell blankLine = new PdfPCell();
			blankLine.setBorder(Rectangle.NO_BORDER);
			blankLine.setColspan(tableCols);

			// set up the body as a five column table
			PdfPTable requestTable = new PdfPTable(tableCols);

			// category header
			PdfPCell headerRow = null;

			@SuppressWarnings("unused") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			SimpleDateFormat sdf2 = new SimpleDateFormat("MM-dd-yyyy");

			headerRow = new PdfPCell(new Paragraph("Date: " + sdf2.format(reportDate), FontFactory.getFont(FontFactory.HELVETICA, 14, Font.BOLD)));
			headerRow.setBorder(Rectangle.NO_BORDER);
			headerRow.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
			headerRow.setColspan(tableCols);
			requestTable.addCell(headerRow);
			requestTable.addCell(blankLine);

			PtoRequestList requests = getAbsenceList(reportDate);

			headerRow = new PdfPCell(new Paragraph("Name", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD)));
			headerRow.setBorder(PdfBorderDictionary.STYLE_BEVELED);
			headerRow.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
			headerRow.setColspan(5);
			requestTable.addCell(headerRow);
			requestTable.addCell(emptyCell);

			headerRow = new PdfPCell(new Paragraph("Dates away", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD)));
			headerRow.setBorder(PdfBorderDictionary.STYLE_BEVELED);
			headerRow.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
			headerRow.setColspan(5);
			requestTable.addCell(headerRow);
			requestTable.addCell(emptyCell);

			headerRow = new PdfPCell(new Paragraph("Number Days", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD)));
			headerRow.setBorder(PdfBorderDictionary.STYLE_BEVELED);
			headerRow.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
			headerRow.setColspan(5);
			requestTable.addCell(headerRow);

			for (PtoRequest ptoRequest : requests.getRequests()) {
				work = ptoRequest.getEmployee().getFullName() + " - " + ptoRequest.getEmployee().getDepartment().getDepartmentName();
				workPhrase = new Phrase(work, FontFactory.getFont(FontFactory.HELVETICA, 9));

				PdfPCell workCell = new PdfPCell(workPhrase);
				workCell.setBorder(Rectangle.NO_BORDER);
				workCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_LEFT);
				workCell.setColspan(5);

				requestTable.addCell(workCell);
				requestTable.addCell(emptyCell);

				SimpleDateFormat df = new SimpleDateFormat("M-d");
				SimpleDateFormat tf = new SimpleDateFormat("h:mm a");

				work = "" +
					df.format(ptoRequest.getStartDate()) +
					" " + tf.format(ptoRequest.getStartTime()) +
					" - " +
					df.format(ptoRequest.getEndDate()) +
					" " + tf.format(ptoRequest.getEndTime());

				workPhrase = new Phrase(work, FontFactory.getFont(FontFactory.HELVETICA, 9));

				workCell = new PdfPCell(workPhrase);
				workCell.setBorder(Rectangle.NO_BORDER);
				workCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_CENTER);
				workCell.setColspan(5);

				requestTable.addCell(workCell);
				requestTable.addCell(emptyCell);

				work = String.valueOf(ptoRequest.getDaysTaken());

				if (work == null) {
					double numDays = PtoRequest.calculateNumDays(
						ptoRequest.getEmployee(),
						ptoRequest.isOneToThree(),
						ptoRequest.getStartDate(),
						ptoRequest.getEndDate(),
						ptoRequest.getStartTime(),
						ptoRequest.getEndTime(),
						ptoRequest.getEmployeeStartTime(),
						ptoRequest.getEmployeeEndTime());

					work = String.valueOf(numDays);
				}

				work = work + " - " + ptoRequest.getRequestType().getDescription();

				workPhrase = new Phrase(work, FontFactory.getFont(FontFactory.HELVETICA, 9));

				workCell = new PdfPCell(workPhrase);
				workCell.setBorder(Rectangle.NO_BORDER);
				workCell.setHorizontalAlignment(com.lowagie.text.Element.ALIGN_RIGHT);
				workCell.setColspan(5);

				requestTable.addCell(workCell);
			}

			requestTable.setTotalWidth(page.getWidth() - document.leftMargin() - document.rightMargin());
			document.add(requestTable);

			document.close();
			fos.close();

		} catch (DocumentException de) {
			de.printStackTrace();
		} catch (FileNotFoundException fnf) {
			fnf.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fileName;
	}

	public String getRequestDetail(long supervisorID, Date endingDate) {
		return null;
	}

	public String getRequestDetail(long supervisorID, Date endingDate, int levels) {
		return null;
	}

	public String getRequestDetail(long supervisorID, Date startingDate, Date endingDate) {
		return null;
	}

	public String getRequestDetail(long supervisorID, Date startingDate, Date endingDate, int levels) {
		return null;
	}

	public String getDepartmentalAttendance(Element element, XPath xpath) {
		log.trace("Build the list of employees in my department (including myself) plus all of my reports...");
		log.trace("Then find all requests for those people within the specified date range...");

		Calendar cal = GregorianCalendar.getInstance();

		long supervisorID = getLong(element, "currentEmployeeID", xpath);

		Date startDate = getDate(element, "startDate", xpath);
		Date endDate = getDate(element, "endDate", xpath);

		PtoEmployee supervisor = entityManager.find(PtoEmployee.class, supervisorID);

		Map<String, PtoEmployee> reports = new HashMap<String, PtoEmployee>();

		log.trace("Grab the list of employees in the department...");

		List<PtoEmployee> departmentMembers = employeeBean.getDepartmentList(supervisor.getDepartment().getId());

		log.trace("Grab the direct reports...");

		List<PtoEmployee> directReports = employeeBean.getReportees(supervisorID);

		reports.put(supervisor.getFullName(), supervisor);

		for (PtoEmployee employee : departmentMembers) {
			if (reports.containsKey(employee.getFullName())) {
				continue;
			}

			reports.put(employee.getFullName(), employee);
		}

		for (PtoEmployee employee : directReports) {
			if (reports.containsKey(employee.getFullName())) {
				continue;
			}

			reports.put(employee.getFullName(), employee);
		}

		log.trace("Create the attendance object...");

		AttendanceRange attendance = new AttendanceRange();

		Date testDate = startDate;

		while (endDate.after(testDate) || endDate.equals(testDate)) {
			AttendanceSingle day = new AttendanceSingle(testDate);

			log.trace("Running for: " + testDate + "...");

			PtoRequestList requests = getAbsenceList(testDate);

			log.trace("Back from getting the daily for " + testDate);

			log.trace("Spin through requests...");

			for (PtoRequest request : requests.getRequests()) {
				log.trace("Testing request for: " + request.getEmployee().getFullName());

				if (reports.containsKey(request.getEmployee().getFullName())) {
					log.trace("Employee is in the list...Add to the day...");

					day.addRequest(request);
				}
			}

			if (!day.getRequests().isEmpty()) {
				log.trace("There are requests for applicable employees for this day...include...");

				attendance.addDate(day);
			} else {
				log.trace("There are no requests to include...skip to next day...");
			}

			log.trace("Increment test date...");

			cal.setTime(testDate);

			do {
				cal.add(GregorianCalendar.DATE, 1);
			} while ((cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) || (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY));

			testDate = new Date(cal.getTimeInMillis());
		}

		log.trace("Build the XML...");

		StringWriter xml = new StringWriter();

		try {
			JAXBContext context = JAXBContext.newInstance(attendance.getClass());

			Marshaller m = context.createMarshaller();

			m.marshal(attendance, xml);
		} catch (JAXBException je) {
			log.fatal("JAXB Exception: " + je.getMessage(), je);
		}

		log.info("Here is the report XML: " + fixXML(xml));

		return fixXML(xml);
	}

	public List<PtoRequest> findSummerHoursViolations() {
		GregorianCalendar today = new GregorianCalendar();
		Integer currYear = today.get(GregorianCalendar.YEAR);

		String sql = "SELECT c from PtoCalendar c WHERE (c.calendarName like 'PIL%')";
		Query query = entityManager.createQuery(sql);

		query = entityManager.createQuery(sql);

		PtoCalendar pilCal = null;

		try {
			pilCal = (PtoCalendar) query.getSingleResult();
		} catch (Exception e) {
			log.error("Current pil calendar not found! ", e);
			return null;
		}

		PtoCalendarYear calYear = pilCal.getPtoYear(currYear);
		log.info("Calendar year: " + calYear.getCalendarYear());

		List<PtoRequest> violations = new ArrayList<PtoRequest>();
		List<PtoEmployee> employees = employeeBean.activeEmployeeListing();

		log.info("Got " + employees.size() + " employees.");

		for (PtoEmployee emp : employees) {
			for (PtoCalendarDetail calDetail : calYear.getDetails()) {
				if (!calDetail.isSummerHours()) {
					continue;
				}

				GregorianCalendar holiday = new GregorianCalendar();
				holiday.setTimeInMillis(calDetail.getHolidayDate().getTime());
				holiday.set(Calendar.HOUR_OF_DAY, 0);
				holiday.set(Calendar.MINUTE, 0);
				holiday.set(Calendar.SECOND, 0);

				GregorianCalendar monday = new GregorianCalendar();
				monday.setTimeInMillis(holiday.getTimeInMillis());
				monday.add(Calendar.DAY_OF_YEAR, -4);

				log.info("Holiday Date: " + holiday.getTime() + " that week's Monday: " + monday.getTime());

				log.info("Getting requests for: " + emp.getFullName());

				sql = "" +
					"SELECT r FROM PtoRequest r WHERE " +
					"(r.requestType.id = 9) AND " +
					"(r.employee.id = :empID) AND " +
					"(((r.startDate >= :monday) AND " +
					"(r.startDate <= :holiday)) OR " +
					"((r.endDate >= :monday) AND " +
					"(r.endDate <= :holiday)) OR " +
					"((r.endDate >= :monday) AND " +
					"(r.startDate <= :holiday))) " +
					"order by r.startDate";

				Query query2 = entityManager.createQuery(sql);
				query2.setParameter("empID", emp.getId());
				query2.setParameter("monday", new java.sql.Date(monday.getTimeInMillis()));
				query2.setParameter("holiday", new java.sql.Date(holiday.getTimeInMillis()));

				@SuppressWarnings("unchecked") List<PtoRequest> results2 = query2.getResultList();

				double daysTakenDuringWeek = 0;
				int week = 0;

				for (PtoRequest req : results2) {
					if ((req.getStatusID() == PtoRequestStatus.CANCELED) || (req.getStatusID() == PtoRequestStatus.REJECTED)) {
						continue;
					}

					if (violations.contains(req)) {
						continue;
					}

					if (req.getDaysTaken() >= 5.0) {
						continue;
					}

					GregorianCalendar startCal = new GregorianCalendar();
					startCal.setTimeInMillis(req.getStartDate().getTime());

					if (week != startCal.get(Calendar.WEEK_OF_YEAR)) {
						week = startCal.get(Calendar.WEEK_OF_YEAR);
						daysTakenDuringWeek = req.getDaysTaken();
					} else {
						daysTakenDuringWeek += req.getDaysTaken();
					}

					log.info("Request: " + req + " Days during week: " + daysTakenDuringWeek);

					if (daysTakenDuringWeek > 3.5) {
						log.info("Adding a request: " +
							req.getId() + " " +
							req.getAction() + " " +
							req.getRequestType().getId() + " " +
							req.getRequestType().getDescription());

						violations.add(req);
					}
				}
			}
		}

		return violations;
	}
}
