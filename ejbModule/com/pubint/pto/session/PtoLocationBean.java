package com.pubint.pto.session;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.xpath.XPath;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import com.pubint.pto.entity.PtoCalendar;
import com.pubint.pto.entity.PtoLocation;
import com.pubint.pto.entityList.PtoLocationList;
import com.pubint.pto.sessionInterfaces.PtoLocationLocal;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PtoLocationBean extends PuppetBean implements PtoLocationLocal, Serializable {
	private static final long serialVersionUID = 3L;

	private static Logger log = Logger.getLogger("ptoLocationBean");

	@PersistenceContext(unitName = "ptoJPA")
	private EntityManager entityManager;

	public PtoLocationBean() {
		super();

		log.setLevel(Level.INFO);
	}

	@Override
	public String sendXML(long locID) {
		System.out.println("Sending XML!");

		StringWriter xml = new StringWriter();

		PtoLocation location = entityManager.find(PtoLocation.class, locID);

		if (location != null) {
			try {
				JAXBContext context = JAXBContext.newInstance(location.getClass());

				Marshaller m = context.createMarshaller();

				m.marshal(location, xml);
			} catch (JAXBException je) {
				log.fatal("JAXB Exception: " + je.getMessage(), je);
			}
		} else {
			System.out.println("Location is null!");
		}

		log.trace("XML: " + fixXML(xml));

		return fixXML(xml);
	}

	@Override
	public String selectList() {
		String sql = "select l from PtoLocation l ";

		Query query = entityManager.createQuery(sql);

		@SuppressWarnings("unchecked") List<PtoLocation> results = query.getResultList();

		PtoLocationList locations = new PtoLocationList();

		for (PtoLocation loc : results) {
			locations.addLocation(loc);
		}

		StringWriter xml = new StringWriter();

		try {
			JAXBContext context = JAXBContext.newInstance(locations.getClass());

			Marshaller m = context.createMarshaller();

			m.marshal(locations, xml);
		} catch (JAXBException je) {
			log.fatal("JAXB Exception: " + je.getMessage(), je);
		}

		log.info("XML: " + fixXML(xml));

		return fixXML(xml);
	}

	@Override
	public String update(Element element, XPath xpath) {
		long locID = this.getLong(element, "locID", xpath);

		PtoLocation location = entityManager.find(PtoLocation.class, locID);

		if (location == null) {
			location = new PtoLocation();

			entityManager.persist(location);
		}

		location.setName(this.getString(element, "name", xpath));
		location.setAddress1(this.getString(element, "address1", xpath));
		location.setAddress2(this.getString(element, "address2", xpath));
		location.setAddress3(this.getString(element, "address3", xpath));
		location.setCity(this.getString(element, "city", xpath));
		location.setState(this.getString(element, "state", xpath));
		location.setPostalCode(this.getString(element, "zip", xpath));
		location.setCountry(this.getString(element, "country", xpath));
		location.setPhone(this.getString(element, "phone", xpath));

		PtoCalendar cal = entityManager.find(PtoCalendar.class, this.getLong(element, "calID", xpath));

		location.setCalendar(cal);

		return sendXML(location.getId());
	}

	@Override
	public String remove(Element element, XPath xpath) {
		long locID = this.getLong(element, "locID", xpath);

		PtoLocation location = entityManager.find(PtoLocation.class, locID);

		if (location != null) {
			entityManager.remove(location);

			return "done";
		}

		return "fail";
	}
}
