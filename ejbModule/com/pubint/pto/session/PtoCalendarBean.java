package com.pubint.pto.session;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.xpath.XPath;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import com.pubint.pto.entity.PtoCalendar;
import com.pubint.pto.entity.PtoCalendarDetail;
import com.pubint.pto.entity.PtoCalendarYear;
import com.pubint.pto.entityList.PtoCalendarList;
import com.pubint.pto.sessionInterfaces.PtoCalendarLocal;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PtoCalendarBean extends PuppetBean implements PtoCalendarLocal, Serializable {
	private static final long serialVersionUID = 3L;

	private static Logger log = Logger.getLogger("ptoCalendarBean");

	@PersistenceContext(unitName = "ptoJPA")
	private EntityManager entityManager;

	public PtoCalendarBean() {
		super();

		log.setLevel(Level.TRACE);
	}

	@Override
	public String sendXML(Element element, XPath xpath) {
		Long calID = getLong(element, "calID", xpath);

		StringWriter xml = new StringWriter();

		PtoCalendar calendar = null;

		calendar = entityManager.find(PtoCalendar.class, calID);

		if (calendar != null) {
			try {
				JAXBContext context = JAXBContext.newInstance(calendar.getClass());

				Marshaller m = context.createMarshaller();

				m.marshal(calendar, xml);
			} catch (JAXBException je) {
				log.fatal("JAXB Exception: " + je.getMessage(), je);
			}
		} else {
			log.error("Calendar: " + calID + " is NULL!");
		}

		log.trace("XML: " + fixXML(xml));

		return fixXML(xml);
	}

	@Override
	public String selectList() {
		String sql = "" +
			"select c from PtoCalendar c " +
			"order by c.calendarName";

		Query query = entityManager.createQuery(sql);

		@SuppressWarnings("unchecked") List<PtoCalendar> results = query.getResultList();

		PtoCalendarList calendars = new PtoCalendarList();

		for (PtoCalendar cal : results) {
			calendars.addCalendar(cal);
		}

		StringWriter xml = new StringWriter();

		try {
			JAXBContext context = JAXBContext.newInstance(calendars.getClass());

			Marshaller m = context.createMarshaller();

			m.marshal(calendars, xml);
		} catch (JAXBException je) {
			log.fatal("JAXB Exception: " + je.getMessage(), je);
		}

		log.trace("XML: " + fixXML(xml));

		return fixXML(xml);
	}

	@Override
	public String sendDetails(Element element, XPath xpath) {
		Long yearID = this.getLong(element, "yearID", xpath);

		StringWriter xml = new StringWriter();

		PtoCalendarYear calendarYear = null;

		calendarYear = entityManager.find(PtoCalendarYear.class, yearID);

		if (calendarYear != null) {
			try {
				JAXBContext context = JAXBContext.newInstance(calendarYear.getClass());

				Marshaller m = context.createMarshaller();

				m.marshal(calendarYear, xml);
			} catch (JAXBException je) {
				log.fatal("JAXB Exception: " + je.getMessage(), je);
			}
		} else {
			log.error("Year: " + yearID + " is NULL!");
		}

		log.trace("XML: " + fixXML(xml));

		return fixXML(xml);
	}

	@Override
	public String update(Element element, XPath xpath) {
		long calID = this.getLong(element, "calID", xpath);
		int year = this.getInt(element, "yearEntry", xpath);

		PtoCalendar calendar = entityManager.find(PtoCalendar.class, calID);
		PtoCalendarYear calYear;

		if (calendar == null) {
			calendar = new PtoCalendar();
			entityManager.persist(calendar);

			calYear = new PtoCalendarYear();
			entityManager.persist(calYear);
			calendar.addYear(calYear);
		} else {
			calYear = calendar.getPtoYear(year);

			if (calYear == null) {
				calYear = new PtoCalendarYear();
				entityManager.persist(calYear);

				calendar.addYear(calYear);
			}
		}

		calendar.setCalendarName(this.getString(element, "calName", xpath));
		calYear.setCalendarYear(year);

		String xml = "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>" +
			"<calendar id='" + calendar.getId() + "'><calendarName>" +
			calendar.getCalendarName() + "</calendarName>" +
			"<years><year id='" + calYear.getId() + "'><calendarYear>" +
			calYear.getCalendarYear() +
			"</calendarYear></year></years></calendar>";

		return xml; // sendXML(element, xpath);
	}

	@Override
	public String remove(Element element, XPath xpath) {
		String retVal = "fail";

		long calID = this.getLong(element, "calID", xpath);

		PtoCalendar calendar = entityManager.find(PtoCalendar.class, calID);

		if (calendar != null) {
			entityManager.remove(calendar);
			retVal = "done";
		}

		return retVal;
	}

	@Override
	public String addHoliday(Element element, XPath xpath) {
		String retVal = "fail";

		try {
			System.out.println("Adding holiday.");

			// long calID = this.getLong(element, "calID", xpath);
			long yearID = this.getLong(element, "year", xpath);

			// PtoCalendar calendar = entityManager.find(PtoCalendar.class,
			// calID);
			PtoCalendarYear calendarYear = entityManager.find(PtoCalendarYear.class, yearID);

			if (calendarYear == null) {
				System.err.println("Calendar Year: " + yearID + " empty!");
				log.error("Calendar Year: " + yearID + " empty!");
				return retVal;
			}

			PtoCalendarDetail detail = new PtoCalendarDetail();
			System.out.println("Setting details.");

			detail.setHolidayName(this.getString(element, "name", xpath));
			detail.setHolidayDate(this.getDate(element, "date", xpath));
			detail.setSummerHours(this.getBoolean(element, "summerHours", xpath));

			System.out.println("Adding detail to the year: " + calendarYear.getCalendarYear());

			calendarYear.addDetail(detail);

			retVal = "done";
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return retVal;
	}

	@Override
	public String removeHoliday(Element element, XPath xpath) {
		int holID = this.getInt(element, "holID", xpath);

		PtoCalendarDetail det = entityManager.find(PtoCalendarDetail.class, holID);

		entityManager.remove(det);

		return "done";
	}
}
