package com.pubint.pto.session;

import java.io.File;
import java.io.Serializable;
import java.io.StringWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.xpath.XPath;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import com.pubint.pto.dataObjects.PtoRequestStatus;
import com.pubint.pto.dataObjects.PtoSummary;
import com.pubint.pto.entity.PtoCalendar;
import com.pubint.pto.entity.PtoCalendarDetail;
import com.pubint.pto.entity.PtoCalendarYear;
import com.pubint.pto.entity.PtoDepartment;
import com.pubint.pto.entity.PtoEmployee;
import com.pubint.pto.entity.PtoEmployeeEventType;
import com.pubint.pto.entity.PtoEmployeeHistory;
import com.pubint.pto.entity.PtoEmployeeType;
import com.pubint.pto.entity.PtoGrantedHistory;
import com.pubint.pto.entity.PtoLocation;
import com.pubint.pto.entity.PtoRequest;
import com.pubint.pto.entity.PtoTimeType;
import com.pubint.pto.entity.PtoTimeTypeCategory;
import com.pubint.pto.entityList.PtoEmployeeEventTypeList;
import com.pubint.pto.entityList.PtoEmployeeList;
import com.pubint.pto.entityList.PtoEmployeeTypeList;
import com.pubint.pto.sessionInterfaces.MessageLocal;
import com.pubint.pto.sessionInterfaces.PtoEmployeeLocal;
import com.pubint.pto.sessionInterfaces.PtoRequestLocal;
import com.pubint.pto.sessionInterfaces.PtoTimeTypeLocal;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PtoEmployeeBean extends PuppetBean implements PtoEmployeeLocal, Serializable {
	private static final long serialVersionUID = 3L;

	private static final String xmlHeader = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

	private static Logger log = Logger.getLogger("ptoEmployeeBean");

	@PersistenceContext(unitName = "ptoJPA")
	private EntityManager entityManager;

	@EJB
	PtoTimeTypeLocal timeTypeBean;

	@EJB
	MessageLocal messageBean;

	@EJB
	PtoRequestLocal requestBean;

	private static boolean escalationsSet = false;
	private static PtoDepartment it;
	private static PtoDepartment hr;

	public PtoEmployeeBean() {
		super();

		log.setLevel(Level.INFO);
	}

	private void setupEscalationDepartments() {
		if (escalationsSet) {
			log.trace("Escalation environment already set...");

			log.trace("HR: " + hr);
			log.trace("IT: " + it);

			return;
		}

		log.trace("Finding the department for HR...");

		String sql = "" +
			"select d from PtoDepartment d " +
			"where " +
			"(d.departmentName = :deptName)";

		Query query = entityManager.createQuery(sql);
		query.setMaxResults(1);
		query.setParameter("deptName", "Human Resources");

		if (!query.getResultList().isEmpty()) {
			hr = (PtoDepartment) query.getSingleResult();

			log.trace("HR: " + hr);
		}

		log.trace("Finding the department for IT...");

		query = entityManager.createQuery(sql);
		query.setMaxResults(1);
		query.setParameter("deptName", "Information Technology");

		if (!query.getResultList().isEmpty()) {
			it = (PtoDepartment) query.getSingleResult();

			log.trace("IT: " + it);
		}

		escalationsSet = true;
	}

	@Override
	public String sendXML(Element element, XPath xpath) {
		long empID = getLong(element, "employeeID", xpath);

		java.sql.Date dateSql = getDate(element, "date", xpath);

		return sendXML(empID, dateSql);
	}

	@Override
	public String sendXML(long employeeID) {
		GregorianCalendar today = new GregorianCalendar();

		java.sql.Date todaySql = new java.sql.Date(today.getTimeInMillis());

		return sendXML(employeeID, todaySql);
	}

	@Override
	public String sendXML(long employeeID, java.sql.Date dateSql) {
		setupEscalationDepartments();

		log.info("Building XML for employee: " + employeeID);

		PtoEmployee employee = entityManager.find(PtoEmployee.class, employeeID);

		if (employee == null) {
			log.error("No employee found for id: " + employeeID);

			return "";
		}

		GregorianCalendar dateCal = new GregorianCalendar();

		if ((dateSql != null) && (!("").equals(dateSql))) {
			dateCal.setTimeInMillis(dateSql.getTime());
		}

		PtoGrantedHistory grantedHistory = requestBean.getGrantedHistory(employee, dateCal.get(GregorianCalendar.YEAR));

		employee.setGrantedHistory(grantedHistory);

		log.trace("About to marshall...");

		StringWriter xml = new StringWriter();

		try {
			JAXBContext context = JAXBContext.newInstance(employee.getClass());

			Marshaller m = context.createMarshaller();

			m.marshal(employee, xml);
		} catch (JAXBException je) {
			log.fatal("JAXB Exception: " + je.getMessage(), je);
		} catch (Exception e) {
			log.fatal("General Exception in JAXB: " + e.getMessage(), e);
		}

		return fixXML(xml);
	}

	@Override
	public PtoEmployee getEmployee(String email, String password) {
		setupEscalationDepartments();

		PtoEmployee user = null;

		String sql = "" +
			"select e from PtoEmployee e where " +
			"(e.email = :uCode) and " +
			"(e.password = :uPass)";

		Query query = entityManager.createQuery(sql);

		query.setParameter("uCode", email.toLowerCase());
		query.setParameter("uPass", password);
		query.setMaxResults(1);

		try {
			user = (PtoEmployee) query.getSingleResult();

			if (!password.equals(user.getPassword())) {
				return null;
			}
		} catch (Exception e) {
			log.trace("Exception finding user: " + e.getMessage());
		}

		return user;
	}

	private PtoEmployee getEmployee(String email) {
		setupEscalationDepartments();

		PtoEmployee user = null;

		String sql = "" +
			"select e from PtoEmployee e where " +
			"(e.email = :uCode)";

		Query query = entityManager.createQuery(sql);

		query.setParameter("uCode", email.toLowerCase());
		query.setMaxResults(1);

		try {
			user = (PtoEmployee) query.getSingleResult();
		} catch (Exception e) {
			log.trace("Exception finding user: " + e.getMessage());
		}

		return user;
	}

	@Override
	public PtoEmployee getEmployee(long employeeID) {
		setupEscalationDepartments();

		return entityManager.find(PtoEmployee.class, employeeID);
	}

	@SuppressWarnings("unchecked")
	@Override
	public String receptionistList(long currentUserID) {
		if (isHR(currentUserID)) {
			return selectList(currentUserID);
		}

		log.info("Pulling the employee list for the full company (receptionist mode)...");

		PtoEmployeeList employees = new PtoEmployeeList();

		String sql = "" +
			"select e from PtoEmployee e " +
			"WHERE (e.terminationDate is null) " +
			"order by e.firstName";

		log.trace("Building the query...");

		Query query = entityManager.createQuery(sql);

		List<PtoEmployee> results = query.getResultList();

		log.trace("Spinning through the results list...");

		for (PtoEmployee emp : results) {
			log.trace("Adding: " + emp.getFullName());

			employees.addEmployee(emp);
		}

		log.trace("Build the XML...");

		StringWriter xml = new StringWriter();

		try {
			JAXBContext context = JAXBContext.newInstance(employees.getClass());

			Marshaller m = context.createMarshaller();

			m.marshal(employees, xml);
		} catch (JAXBException je) {
			log.fatal("JAXB Exception: " + je.getMessage(), je);
		} catch (Exception e) {
			log.error("Unexpected exception: " + e.getMessage(), e);
		}

		return fixXML(xml);
	}

	@SuppressWarnings("unchecked")
	@Override
	public String selectList(long currentUserID) {
		setupEscalationDepartments();

		PtoEmployeeList employees = new PtoEmployeeList();

		if (isHR(currentUserID) || currentUserID == 242) {
			String sql = "" +
				"select e from PtoEmployee e " +
				"WHERE (e.terminationDate is null) " +
				"order by e.firstName";

			Query query = entityManager.createQuery(sql);

			List<PtoEmployee> results = query.getResultList();

			for (PtoEmployee employee : results) {
				employees.addEmployee(employee);
			}

			PtoEmployee termLine = new PtoEmployee();

			termLine.setId(Long.valueOf(-100));
			termLine.setFirstName("------- Terminated Employees -------");
			termLine.setLastName("");

			employees.addEmployee(termLine);

			sql = "" +
				"select e from PtoEmployee e " +
				"WHERE (e.terminationDate is not null) " +
				"order by e.firstName";

			query = entityManager.createQuery(sql);

			results = query.getResultList();

			for (PtoEmployee employee : results) {
				employees.addEmployee(employee);
			}
		} else {
			for (PtoEmployee employee : getSubordinates(currentUserID)) {
				employees.addEmployee(employee);
			}
		}

		StringWriter xml = new StringWriter();

		try {
			JAXBContext context = JAXBContext.newInstance(employees.getClass());

			Marshaller m = context.createMarshaller();

			m.marshal(employees, xml);
		} catch (JAXBException je) {
			log.fatal("JAXB Exception: " + je.getMessage(), je);
		}

		return fixXML(xml);
	}

	@Override
	public String changePassword(Element element, XPath xpath) {
		setupEscalationDepartments();

		long employeeID = getLong(element, "currentEmployeeID", xpath);

		PtoEmployee employee = entityManager.find(PtoEmployee.class, employeeID);

		if (employee == null) {
			return "Could not locate employee record. Request failed.";
		}

		String currPass = getString(element, "currPass", xpath);

		if (getEmployee(employee.getEmail(), currPass) == null) {
			return "Current password does not match. Request failed.";
		}

		String newPass1 = getString(element, "newPass1", xpath);
		String newPass2 = getString(element, "newPass2", xpath);

		if (!newPass1.equals(newPass2)) {
			return "New passwords do not match. Request failed.";
		}

		if (currPass.equals(newPass1)) {
			return "New password is the same as current. Nothing changed.";
		}

		// if (! newPass1.matches("^(?=.*[A-Z])(?=.*[0-9])[A-Z0-9]+$")) {
		// return
		// "New password must contain at least one upper-case letter (A-Z) and one number (0-9)";
		// }

		employee.setPassword(newPass2);

		return "good";
	}

	/**
	 * getSubordinates currently searches through 4 layers of reportees.
	 * This will have to do until I (or someone else) comes up with
	 * a proper recursion.
	 * 
	 * @param whosAsking
	 * @return
	 */
	private List<PtoEmployee> getSubordinates(long employeeID) {
		PtoEmployee whosAsking = entityManager.find(PtoEmployee.class, employeeID);

		List<PtoEmployee> employees = new ArrayList<PtoEmployee>();

		employees.add(whosAsking);

		// getReportees returns a list of detached employees...
		// no need to re-detach
		List<PtoEmployee> level1 = getReportees(employeeID);

		for (PtoEmployee emp : level1) {
			if (emp.getTerminationDate() != null) {
				log.trace("Skip adding " + emp.getFullName() + " they were terminated: " + emp.getTerminationDate());

				continue;
			}

			log.trace("Add report: " + emp.getFullName());
			log.trace("And their reports...");

			employees.add(emp);

			List<PtoEmployee> level2 = getReportees(emp.getId());

			for (PtoEmployee emp2 : level2) {
				if (emp2.getTerminationDate() != null) {
					log.trace("Skip adding " + emp2.getFullName() + " they were terminated: " + emp2.getTerminationDate());

					continue;
				}

				log.trace("Add report: " + emp2.getFullName());
				log.trace("And their reports...");

				employees.add(emp2);

				List<PtoEmployee> level3 = getReportees(emp2.getId());

				for (PtoEmployee emp3 : level3) {
					if (emp3.getTerminationDate() != null) {
						log.trace("Skip adding " + emp3.getFullName() + " they were terminated: " + emp3.getTerminationDate());

						continue;
					}

					log.trace("Add report: " + emp3.getFullName());
					log.trace("And their reports...");

					employees.add(emp3);

					List<PtoEmployee> level4 = getReportees(emp3.getId());

					for (PtoEmployee emp4 : level4) {
						if (emp4.getTerminationDate() != null) {
							log.trace("Skip adding " + emp4.getFullName() + " they were terminated: " + emp4.getTerminationDate());

							continue;
						}

						log.trace("Add report: " + emp4.getFullName());

						employees.add(emp4);
					}
				}
			}
		}

		Collections.sort(employees);

		return employees;
	}

	public List<PtoEmployee> activeEmployeeListing() {
		return activeEmployeeListing(now());
	}

	public List<PtoEmployee> activeEmployeeListing(java.sql.Date asOfDate) {
		setupEscalationDepartments();

		String sql = "" +
			"select e from PtoEmployee e " +
			"where " +
			"(e.startDate <= :startDate) and " +
			"(" +
			"(e.terminationDate is null) or " +
			"(e.terminationDate > :asOf)" +
			") " +
			"order by e.department.departmentName, e.lastName";

		Query query = entityManager.createQuery(sql);

		query.setParameter("startDate", asOfDate);
		query.setParameter("asOf", asOfDate);

		@SuppressWarnings("unchecked") List<PtoEmployee> results = query.getResultList();

		List<PtoEmployee> employees = new ArrayList<PtoEmployee>();

		for (PtoEmployee employee : results) {
			employees.add(employee);
		}

		return employees;
	}

	public List<PtoEmployee> allEmployeeListing(int year) {
		setupEscalationDepartments();

		GregorianCalendar beginYear = new GregorianCalendar();
		GregorianCalendar endYear = new GregorianCalendar();

		beginYear.set(year, 0, 1);
		endYear.set(year, 11, 31);

		String sql = "" +
			"select e from PtoEmployee e " +
			"where " +
			"(e.startDate <= :endYear) and " +
			"(" +
			"(e.terminationDate is null) or " +
			"(e.terminationDate > :beginYear)" +
			") " +
			"order by e.department.departmentName, e.lastName";

		Query query = entityManager.createQuery(sql);

		query.setParameter("endYear", new java.sql.Date(endYear.getTimeInMillis()));
		query.setParameter("beginYear", new java.sql.Date(beginYear.getTimeInMillis()));

		@SuppressWarnings("unchecked") List<PtoEmployee> results = query.getResultList();

		List<PtoEmployee> employees = new ArrayList<PtoEmployee>();

		for (PtoEmployee employee : results) {
			employees.add(employee);
		}

		return employees;
	}

	@Override
	public List<PtoEmployee> getDepartmentList(long departmentID) {
		log.trace("Getting the employees in department: " + departmentID);

		log.trace("Building the query...");

		String sql = "" +
			"select e from PtoEmployee e where " +
			"(e.department.id = :deptID)";

		log.trace("Query: " + sql);

		log.trace("Executing query...");

		Query query = entityManager.createQuery(sql);

		query.setParameter("deptID", departmentID);

		List<PtoEmployee> members = new ArrayList<PtoEmployee>();

		log.trace("Retrieving result set...");

		if (query.getResultList().isEmpty()) {
			log.trace("There are no employees in this department..");

			return members;
		}

		@SuppressWarnings("unchecked") List<PtoEmployee> results = query.getResultList();

		log.trace("Adding employees to return list...");

		for (PtoEmployee employee : results) {
			members.add(employee);
		}

		return members;
	}

	@Override
	public String activeEmployeeXML() {
		setupEscalationDepartments();

		PtoEmployeeList employees = new PtoEmployeeList(activeEmployeeListing());

		StringWriter xml = new StringWriter();

		try {
			JAXBContext context = JAXBContext.newInstance(employees.getClass());

			Marshaller m = context.createMarshaller();

			m.marshal(employees, xml);
		} catch (JAXBException je) {
			log.fatal("JAXB Exception: " + je.getMessage(), je);
		}

		log.trace("XML: " + fixXML(xml));

		return fixXML(xml);
	}

	@Override
	public String supervisorSelectList() {
		setupEscalationDepartments();

		log.info("Getting the list of supervisors...");

		String sql = "" +
			"SELECT e FROM PtoEmployee e " +
			"WHERE (e.supervisor = true) AND " +
			"(e.terminationDate is null) " +
			"ORDER BY e.lastName";

		Query query = entityManager.createQuery(sql);

		@SuppressWarnings("unchecked") List<PtoEmployee> results = query.getResultList();

		PtoEmployeeList employees = new PtoEmployeeList();

		log.trace("Spinning through the query results...");

		for (PtoEmployee employee : results) {
			employees.addEmployee(employee);
		}

		StringWriter xml = new StringWriter();

		try {
			JAXBContext context = JAXBContext.newInstance(employees.getClass());

			Marshaller m = context.createMarshaller();

			m.marshal(employees, xml);
		} catch (JAXBException je) {
			log.fatal("JAXB Exception: " + je.getMessage(), je);
		}

		return fixXML(xml);
	}

	@Override
	public String update(Element element, XPath xpath) {
		setupEscalationDepartments();

		long empID = this.getLong(element, "empID", xpath);

		PtoEmployee employee = entityManager.find(PtoEmployee.class, empID);

		if (employee == null) {
			employee = new PtoEmployee();

			employee.setNewHireNoticeSent(false);

			entityManager.persist(employee);
		}

		try {
			PtoEmployeeType type = entityManager.find(PtoEmployeeType.class, this.getLong(element, "typeID", xpath));
			PtoDepartment dept = entityManager.find(PtoDepartment.class, this.getLong(element, "deptID", xpath));
			PtoLocation loc = entityManager.find(PtoLocation.class, this.getLong(element, "locID", xpath));

			PtoEmployee directReport = entityManager.find(PtoEmployee.class, this.getLong(element, "directReport", xpath));
			PtoEmployee ptoReport = entityManager.find(PtoEmployee.class, this.getLong(element, "ptoReport", xpath));
			PtoEmployee ptoBackup = entityManager.find(PtoEmployee.class, this.getLong(element, "ptoBackup", xpath));

			employee.setFirstName(this.getString(element, "firstName", xpath));
			employee.setMiddleInitial(this.getString(element, "midInitial", xpath));
			employee.setLastName(this.getString(element, "lastName", xpath));
			employee.setTitle(this.getString(element, "title", xpath));
			employee.setEmail(this.getString(element, "empEmail", xpath));
			employee.setExtension(this.getString(element, "extension", xpath));

			employee.setEmployeeType(type);
			employee.setDepartment(dept);
			employee.setLocation(loc);

			employee.setDirectReport(directReport);
			employee.setPtoReport(ptoReport);
			employee.setBackupPtoReport(ptoBackup);

			employee.setHourly(this.getBoolean(element, "hourly", xpath));
			employee.setStartTime(stringToTime(this.getString(element, "startTime", xpath)));
			employee.setEndTime(stringToTime(this.getString(element, "endTime", xpath)));

			employee.setHireDate(this.getDate(element, "hireDate", xpath));
			employee.setStartDate(this.getDate(element, "startDate", xpath));
			employee.setTerminationDate(this.getDate(element, "termDate", xpath));
			employee.setReviewDate(this.getDate(element, "reviewDate", xpath));
			employee.setSupervisor(this.getBoolean(element, "supervisor", xpath));
			employee.setSummerHours(this.getBoolean(element, "summerHours", xpath));
			employee.setReceptionRole(this.getBoolean(element, "isReception", xpath));

			int year = new GregorianCalendar().get(GregorianCalendar.YEAR);

			PtoGrantedHistory grantedHist = requestBean.getGrantedHistory(employee, year);

			if (grantedHist == null) {
				grantedHist = new PtoGrantedHistory(employee, year);
				entityManager.persist(grantedHist);
			}

			grantedHist.setRolloverDays(this.getReal(element, "rollover", xpath));
			grantedHist.setCompTimeGranted(this.getReal(element, "compTime", xpath));
			grantedHist.setCompTimeExpireDate(this.getDate(element, "compTimeExpire", xpath));
			grantedHist.setPtoAdjustment(this.getReal(element, "ptoAdjustment", xpath));
			grantedHist.setPtoAdjustmentExpireDate(this.getDate(element, "ptoAdjExpire", xpath));
			grantedHist.setBonusOneToThree(this.getInt(element, "bonusOneToThree", xpath));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sendXML(employee.getId());
	}

	@Override
	public String remove(Element element, XPath xpath) {
		setupEscalationDepartments();

		long empID = this.getLong(element, "empID", xpath);

		PtoEmployee employee = entityManager.find(PtoEmployee.class, empID);

		if (employee != null) {
			entityManager.remove(employee);

			return "done";
		}

		return "fail";
	}

	@Override
	public String empIDToEmployeeName(long empID) {
		setupEscalationDepartments();

		return entityManager.find(PtoEmployee.class, empID).getFullName();
	}

	@Override
	public String getSupervisor(Element element, XPath xpath) {
		setupEscalationDepartments();

		long serfID = this.getLong(element, "employeeID", xpath);
		long directID = getLong(element, "currentEmployeeID", xpath);

		return getSupervisor(serfID, directID);
	}

	@Override
	public String getSupervisor(long serfID, long directID) {
		setupEscalationDepartments();

		PtoEmployee vassal = entityManager.find(PtoEmployee.class, directID);
		PtoEmployee serf = entityManager.find(PtoEmployee.class, serfID);

		String retVal = "false";

		if (isHR(directID)) {
			return "HR";
		} else if ((serf.getDirectReport() == vassal)
			|| (serf.getPtoReport() == vassal)
			|| (serf.getBackupPtoReport() == vassal)) {
			retVal = "super";
		} else if (vassal.getId() == serf.getId()) {
			retVal = "user";
		}

		return retVal;
	}

	@Override
	public String typeSelectList() {
		setupEscalationDepartments();

		StringWriter xml = new StringWriter();

		try {
			String sql = "" +
				"select e from PtoEmployeeType e " +
				"order by e.id";

			Query query = entityManager.createQuery(sql);

			@SuppressWarnings("unchecked") List<PtoEmployeeType> results = query.getResultList();

			PtoEmployeeTypeList types = new PtoEmployeeTypeList();

			for (PtoEmployeeType type : results) {
				types.addType(type);
			}

			try {
				JAXBContext context = JAXBContext.newInstance(types.getClass());

				Marshaller m = context.createMarshaller();

				m.marshal(types, xml);
			} catch (JAXBException je) {
				log.fatal("JAXB Exception: " + je.getMessage(), je);
			}
		} catch (Exception e) {
			log.fatal("Unexpected exception: " + e.getMessage(), e);
		}

		return fixXML(xml);
	}

	@Override
	public String eventTypeSelectList() {
		setupEscalationDepartments();

		StringWriter xml = new StringWriter();

		try {
			String sql = "" +
				"select e from PtoEmployeeEventType e " +
				"order by e.id";

			Query query = entityManager.createQuery(sql);

			@SuppressWarnings("unchecked") List<PtoEmployeeEventType> results = query.getResultList();

			PtoEmployeeEventTypeList types = new PtoEmployeeEventTypeList();

			for (PtoEmployeeEventType type : results) {
				types.addType(type);
			}

			try {
				JAXBContext context = JAXBContext.newInstance(types.getClass());

				Marshaller m = context.createMarshaller();

				m.marshal(types, xml);
			} catch (JAXBException je) {
				log.fatal("JAXB Exception: " + je.getMessage(), je);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fixXML(xml);
	}

	public List<PtoEmployee> getReportees(long supervisorID) {
		return getReportees(supervisorID, true, true, true);
	}

	public List<PtoEmployee> getReportees(long supervisorID, boolean includeDirect, boolean includePto, boolean includeBackup) {
		setupEscalationDepartments();

		List<PtoEmployee> reports = new ArrayList<PtoEmployee>();

		PtoEmployee supervisor = entityManager.find(PtoEmployee.class, supervisorID);

		if (supervisor == null) {
			return reports;
		}

		log.trace("Building the query to find the people who report to employee: " + supervisor.getFullName());

		String sql = "" +
			"select e from PtoEmployee e where " +
			(includeDirect ? "(e.directReport = :direct)" : "(true)") +
			(includePto || includeBackup ? " or " : "") +
			(includePto ? "(e.ptoReport = :pto)" : "") +
			(includeBackup ? " or " : "") +
			(includeBackup ? "(e.backupPtoReport = :backup)" : "") +
			" order by e.firstName";

		log.trace("Executing query: " + sql);

		Query query = entityManager.createQuery(sql);

		log.trace("Setting parameters...");

		try {
			if (includeDirect) {
				log.trace("Setting direct report...");

				query.setParameter("direct", supervisor);
			}

			if (includePto) {
				query.setParameter("pto", supervisor);
			}

			if (includeBackup) {
				query.setParameter("backup", supervisor);
			}
		} catch (Exception e) {
			log.fatal("Unexpected exception: " + e.getMessage(), e);
		}

		if (query.getResultList().isEmpty()) {
			log.info("There are no results for employees that report to employee ID: " + supervisorID);
			log.info("Using criteria:");
			log.info("Direct Reports: " + includeDirect);
			log.info("PTO Reports: " + includePto);
			log.info("Backup PTO Reports: " + includeBackup);

			return reports;
		}

		@SuppressWarnings("unchecked") List<PtoEmployee> results = query.getResultList();

		for (PtoEmployee employee : results) {
			reports.add(employee);
		}

		return reports;
	}

	@Override
	public List<PtoEmployee> getTerminations(Date fromDate, Date toDate) {
		setupEscalationDepartments();

		List<PtoEmployee> employees = new ArrayList<PtoEmployee>();

		// GregorianCalendar firstOfYear = new GregorianCalendar();
		// // GregorianCalendar endOfYear = new GregorianCalendar();
		//
		// firstOfYear.setTime(asOfDate);
		// firstOfYear.set(GregorianCalendar.MONTH, 0);
		// firstOfYear.set(GregorianCalendar.DAY_OF_MONTH, 1);
		//
		// Date foy = new Date(firstOfYear.getTimeInMillis());

		String sql = "" +
			"SELECT e FROM PtoEmployee e WHERE " +
			"(e.terminationDate >= :fromDate) AND " +
			"(e.terminationDate <= :toDate) " +
			"order by e.terminationDate, e.department.departmentName, e.firstName";

		Query query = entityManager.createQuery(sql);

		query.setParameter("fromDate", fromDate);
		query.setParameter("toDate", toDate);

		@SuppressWarnings("unchecked") List<PtoEmployee> results = query.getResultList();

		for (PtoEmployee employee : results) {
			employees.add(employee);
		}

		return employees;
	}

	@Override
	public String addEvent(Element element, XPath xpath) {
		setupEscalationDepartments();

		String retVal = "fail";

		try {
			long eventID = getLong(element, "eventID", xpath);

			PtoEmployeeHistory event = entityManager.find(PtoEmployeeHistory.class, eventID);

			if (event == null) {
				log.trace("Creating a new event...");

				event = new PtoEmployeeHistory();
			}

			log.trace("Event: " + event);

			long deptID = getLong(element, "deptID", xpath);
			long grantorID = getLong(element, "grantor", xpath);
			long employeeID = getLong(element, "employeeID", xpath);
			long eventTypeID = getLong(element, "eventType", xpath);

			if (grantorID == 0) {
				grantorID = getLong(element, "currentEmployeeID", xpath);
			}

			PtoDepartment dept = entityManager.find(PtoDepartment.class, deptID);

			@SuppressWarnings("unused") PtoEmployee grantor = entityManager.find(PtoEmployee.class, grantorID);

			PtoEmployee employee = entityManager.find(PtoEmployee.class, employeeID);

			PtoEmployeeEventType eventType = entityManager.find(PtoEmployeeEventType.class, eventTypeID);

			Date eventDate = getDate(element, "date", xpath);

			event.setEventDate(eventDate);
			event.setDepartment(dept);
			event.setEventType(eventType);
			event.setComment(getString(element, "comment", xpath));
			event.setEmployee(employee);

			log.trace("Event: " + event);

			employee.addHistory(event);

			retVal = "success";
		} catch (NumberFormatException nfe) {
			log.error("Number format exception: " + nfe.getMessage(), nfe);
		} catch (Exception e) {
			log.error("Unexpected exception: " + e.getMessage(), e);
		}

		return retVal;
	}

	@Override
	public String deleteEvent(Element element, XPath xpath) {
		setupEscalationDepartments();

		long eventID = getLong(element, "eventID", xpath);

		log.trace("Searching for event to delete: " + eventID);

		String retVal = "failure";

		try {
			PtoEmployeeHistory event = entityManager.find(PtoEmployeeHistory.class, eventID);

			if (event != null) {
				log.trace("Event found...Deleting...");

				entityManager.remove(event);

				retVal = "success";
			}
		} catch (Exception e) {
			log.error("Unexpected exception: " + e.getMessage(), e);
		}

		return retVal;
	}

	@Override
	public double getTotalPTO(long employeeID) {
		PtoEmployee emp = getEmployee(employeeID);

		return getBasePTO(employeeID) + emp.getRolloverDays() + emp.getPtoAdjustment();
	}

	@Override
	public double getBasePTO(long employeeID) {
		return getBasePTO(employeeID, PtoTimeTypeCategory.PTO_CHARGEABLE);
	}
	
	@Override
	public double getBasePTO(long employeeID, long timeTypeCategoryID) {
		Calendar todayCal = Calendar.getInstance();
		
		return getBasePTO(employeeID, timeTypeCategoryID, todayCal.get(Calendar.YEAR));
	}

	@Override
	public double getBasePTO(long employeeID, long timeTypeCategoryID, int year) {
		setupEscalationDepartments();

		double retVal = 0;

		PtoEmployee emp = entityManager.find(PtoEmployee.class, employeeID);

		if (emp == null) {
			return 0.0;
		}

		if (emp.getEmployeeType().getDescription().contains("Temporary")) {
			return 0.0;
		}

		log.trace("Found an employee: " + emp.getFullName());

		PtoLocation loc = emp.getLocation();

		log.trace("Found a location: " + loc.getName());

		PtoTimeTypeCategory typeCat = entityManager.find(PtoTimeTypeCategory.class, timeTypeCategoryID);

		log.trace("Found a timeTypeCat: " + typeCat.getDescription());

		PtoTimeType timeType = null;

		try {
			timeType = timeTypeBean.getTimeType(typeCat, loc); // entityManager.find(PtoTimeType.class,
			// 5); //

			log.trace("Found a Time Type: " + timeType.getId());

			Calendar todayCal = Calendar.getInstance();
			todayCal.setTime(new Date(new java.util.Date().getTime()));

			Calendar startCal = Calendar.getInstance();
			startCal.setTime(emp.getStartDate());

			log.trace("" +
				"Created calendars, start date: " +
				startCal.get(Calendar.YEAR) + "-" +
				((int) startCal.get(Calendar.MONTH) + 1) + "-" +
				startCal.get(Calendar.DAY_OF_MONTH));

			// use start date and time type breaks to determine base PTO
			// then add comp and adjustment
			if ((startCal.get(Calendar.YEAR) == todayCal.get(Calendar.YEAR)) ||
				((timeTypeCategoryID == PtoTimeTypeCategory.PTO_CHARGEABLE) &&
					(todayCal.get(Calendar.YEAR) - startCal.get(Calendar.YEAR) == timeType.getYearBreak2()))) {
				log.trace("Hire date is this year or this is their anniversary bonus year.");

				// Started this year, use the breaks on day and month.
				Calendar break1Cal = Calendar.getInstance();
				Calendar break2Cal = Calendar.getInstance();
				Calendar break3Cal = Calendar.getInstance();
				Calendar break4Cal = Calendar.getInstance();
				Calendar break5Cal = Calendar.getInstance();
				Calendar break6Cal = Calendar.getInstance();
				Calendar break7Cal = Calendar.getInstance();
				Calendar break8Cal = Calendar.getInstance();
				Calendar break9Cal = Calendar.getInstance();
				Calendar break10Cal = Calendar.getInstance();
				Calendar break11Cal = Calendar.getInstance();
				Calendar break12Cal = Calendar.getInstance();

				if (timeType.getYear1Break1() != null) {
					break1Cal.setTime(timeType.getYear1Break1());
				}

				if (timeType.getYear1Break1() != null) {
					break2Cal.setTime(timeType.getYear1Break2());
				}

				if (timeType.getYear1Break3() != null) {
					break3Cal.setTime(timeType.getYear1Break3());
				}

				if (timeType.getYear1Break4() != null) {
					break4Cal.setTime(timeType.getYear1Break4());
				}

				if (timeType.getYear1Break5() != null) {
					break5Cal.setTime(timeType.getYear1Break5());
				}

				if (timeType.getYear1Break6() != null) {
					break6Cal.setTime(timeType.getYear1Break6());
				}

				if (timeType.getYear1Break7() != null) {
					break7Cal.setTime(timeType.getYear1Break7());
				}

				if (timeType.getYear1Break8() != null) {
					break8Cal.setTime(timeType.getYear1Break8());
				}

				if (timeType.getYear1Break9() != null) {
					break9Cal.setTime(timeType.getYear1Break9());
				}

				if (timeType.getYear1Break10() != null) {
					break10Cal.setTime(timeType.getYear1Break10());
				}

				if (timeType.getYear1Break11() != null) {
					break11Cal.setTime(timeType.getYear1Break11());
				}

				if (timeType.getYear1Break12() != null) {
					break12Cal.setTime(timeType.getYear1Break12());
				}

				if ((timeType.getYear1Break1() != null) && (startCal.get(Calendar.MONTH) <= break1Cal.get(Calendar.MONTH))) {
					retVal = timeType.getYear1Break1Days();
				} else if ((timeType.getYear1Break2() != null) && (startCal.get(Calendar.MONTH) <= break2Cal.get(Calendar.MONTH))) {
					retVal = timeType.getYear1Break2Days();
				} else if ((timeType.getYear1Break3() != null) && (startCal.get(Calendar.MONTH) <= break3Cal.get(Calendar.MONTH))) {
					retVal = timeType.getYear1Break3Days();
				} else if ((timeType.getYear1Break4() != null) && (startCal.get(Calendar.MONTH) <= break4Cal.get(Calendar.MONTH))) {
					retVal = timeType.getYear1Break4Days();
				} else if ((timeType.getYear1Break5() != null) && (startCal.get(Calendar.MONTH) <= break5Cal.get(Calendar.MONTH))) {
					retVal = timeType.getYear1Break5Days();
				} else if ((timeType.getYear1Break6() != null) && (startCal.get(Calendar.MONTH) <= break6Cal.get(Calendar.MONTH))) {
					retVal = timeType.getYear1Break6Days();
				} else if ((timeType.getYear1Break7() != null) && (startCal.get(Calendar.MONTH) <= break7Cal.get(Calendar.MONTH))) {
					retVal = timeType.getYear1Break7Days();
				} else if ((timeType.getYear1Break8() != null) && (startCal.get(Calendar.MONTH) <= break8Cal.get(Calendar.MONTH))) {
					retVal = timeType.getYear1Break8Days();
				} else if ((timeType.getYear1Break9() != null) && (startCal.get(Calendar.MONTH) <= break9Cal.get(Calendar.MONTH))) {
					retVal = timeType.getYear1Break9Days();
				} else if ((timeType.getYear1Break10() != null) && (startCal.get(Calendar.MONTH) <= break10Cal.get(Calendar.MONTH))) {
					retVal = timeType.getYear1Break10Days();
				} else if ((timeType.getYear1Break11() != null) && (startCal.get(Calendar.MONTH) <= break11Cal.get(Calendar.MONTH))) {
					retVal = timeType.getYear1Break11Days();
				} else if ((timeType.getYear1Break12() != null) && (startCal.get(Calendar.MONTH) <= break12Cal.get(Calendar.MONTH))) {
					retVal = timeType.getYear1Break12Days();
				}

				if ((timeTypeCategoryID == PtoTimeTypeCategory.PTO_CHARGEABLE) &&
					(todayCal.get(Calendar.YEAR) - startCal.get(Calendar.YEAR) == timeType.getYearBreak2())) {

					Calendar annivCal = Calendar.getInstance();
					annivCal.setTime(emp.getStartDate());
					annivCal.add(Calendar.YEAR, 5);

					if (annivCal.before(todayCal)) {
						log.trace("Today is on or after their anniversary, they get the bonus days.");
						retVal = retVal + timeType.getYear1MaxPTO();
					} else {
						log.trace("Today is before their anniversary, NO BONUS FOR YOU!");
						retVal = timeType.getYear1MaxPTO();
					}
				}
			} else if (((timeType.getYearBreak2() == null) || (timeType.getYearBreak2() == 0))
				|| (todayCal.get(Calendar.YEAR)
					- startCal.get(Calendar.YEAR) < timeType.getYearBreak2())) {
				log.trace("Start Date is in first break. "
					+ todayCal.get(Calendar.YEAR) + " - "
					+ startCal.get(Calendar.YEAR) + " < "
					+ timeType.getYearBreak2());
				// Started before current year, but less than second break
				retVal = timeType.getYear1MaxPTO();
				log.trace("RetVal after year calc: " + retVal);
			} else if (((timeType.getYearBreak3() == null) || (timeType.getYearBreak3() == 0))
				|| (todayCal.get(Calendar.YEAR)
					- startCal.get(Calendar.YEAR) > timeType.getYearBreak3())) {
				log.trace("" +
					"Start Date is in second break. " +
					todayCal.get(Calendar.YEAR) + " - " +
					startCal.get(Calendar.YEAR) + " < " +
					timeType.getYearBreak3());

				// Started before current year, but less than third break
				retVal = timeType.getYear2MaxPTO();
			} else {
				log.trace("Hire Date is in third break");

				retVal = timeType.getYear3MaxPTO();
			}

			if (retVal > timeType.getMaxDaysAllowed()) {
				retVal = timeType.getMaxDaysAllowed();
			}

			/**
			 * Add the bonus time.
			 */
			// PtoGrantedHistory grantedHistory =
			// requestBean.getGrantedHistory(emp, new
			// GregorianCalendar().get(GregorianCalendar.YEAR));
			// // empDetach.setGrantedHistory(grantedHistory);
			//
			// if (timeTypeCategoryID == PtoTimeTypeCategory.PTO_CHARGEABLE)
			// {
			// retVal = retVal + grantedHistory.getRolloverDays() +
			// grantedHistory.getPtoAdjustment();
			// } else if (timeTypeCategoryID ==
			// PtoTimeTypeCategory.ONE2THREE) {
			// log.trace("Retval before adding bonus one-to-three: " +
			// retVal);
			// retVal = retVal + grantedHistory.getBonusOneToThree();
			// log.trace("... and after: " + retVal);
			// }

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		log.info("Returning: " + retVal);

		return retVal;
	}

	@Override
	public int getBaseOneTo3(long empID) {
		setupEscalationDepartments();

		// PtoEmployee employee = entityManager.find(PtoEmployee.class, empID);
		// PtoLocation location = employee.getLocation();

		double oneToThrees = getBasePTO(empID, PtoTimeTypeCategory.ONE2THREE);

		if (oneToThrees % 1 != 0) {
			log.error("One to Three lookup returns a decimal! Value returned: " + oneToThrees);

			return -1;
		}

		return ((int) oneToThrees);
	}

	public void sendDailyAttendance() {
		sendDailyAttendance(null);
	}

	public void sendDailyAttendance(Serializable parameters) {
		GregorianCalendar todayCal = new GregorianCalendar();

		if (todayCal.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.SUNDAY) {
			log.info("No sending the daily attendance on Sunday...");

			return;
		}

		if (todayCal.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.SATURDAY) {
			log.info("No sending the daily attendance on Saturday...");

			return;
		}

		// Do not send it if the company is out on Holiday.
		GregorianCalendar ptoHoliday = new GregorianCalendar();
		PtoLocation loc = entityManager.find(PtoLocation.class, 3);
		PtoCalendar ptoCal = loc.getCalendar();
		PtoCalendarYear ptoYear = ptoCal.getPtoYear(todayCal.get(Calendar.YEAR));

		for (PtoCalendarDetail detail : ptoYear.getDetails()) {
			ptoHoliday.setTime(detail.getHolidayDate());
			ptoHoliday.set(Calendar.HOUR_OF_DAY, todayCal.get(Calendar.HOUR_OF_DAY));
			ptoHoliday.set(Calendar.MINUTE, todayCal.get(Calendar.MINUTE));
			ptoHoliday.set(Calendar.SECOND, todayCal.get(Calendar.SECOND));
			ptoHoliday.set(Calendar.MILLISECOND, todayCal.get(Calendar.MILLISECOND));

			// log.trace("Today: " + todayCal);
			// log.trace("Holiday: " + ptoHoliday);

			if (todayCal.compareTo(ptoHoliday) == 0) {
				if (detail.isSummerHours()) {
					if (todayCal.get(GregorianCalendar.AM_PM) == 1) {
						log.info("No sending the daily attendance on Summer Hours afternoons.");
						return;
					}
				} else {
					log.info("No sending the daily attendance on company holidays.");
					return;
				}
			}
		}

		List<PtoEmployee> supervisors = new ArrayList<PtoEmployee>();

		if (parameters != null) {
			log.info("Triggered by timer: " + parameters);

			GregorianCalendar twoDaysAgo = new GregorianCalendar();
			twoDaysAgo.add(GregorianCalendar.DAY_OF_MONTH, -2);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			log.trace("Generate the unapproved PTO notices...");

			String sql = "" +
				"select r from PtoRequest r where " +
				"(r.statusID = :pending) and " +
				"(r.startDate <= :today) and " +
				"(r.requestCreated < :creationThreshold)";

			Query unapproved = entityManager.createQuery(sql);

			unapproved.setParameter("pending", PtoRequestStatus.PENDING);
			unapproved.setParameter("today", getTodaySQL());
			unapproved.setParameter("creationThreshold", sdf.format(twoDaysAgo.getTime()));

			@SuppressWarnings("unchecked") List<PtoRequest> unapprovedRequests = unapproved.getResultList();

			for (PtoRequest request : unapprovedRequests) {
				if (request.getEmployee() == null) {
					// messageBean.notifyIT("Damaged PTO request: " +
					// request.getId(), "Request has no employee.", null);

					continue;
				}

				if (request.getEmployee().getTerminationDate() != null) {
					// messageBean.notifyIT("Request for terminated or transferred employee: "
					// + request.getId(),
					// "Employee is either terminated or transferred\nThis notification should be stopped",
					// null);

					continue;
				}

				String title = "Unapproved request for " + request.getEmployee().getFullName();
				String link = "" +
					PtoRequestBean.server + PtoRequestBean.port +
					"/pto/requests.jsp?requestID=" + request.getId();
				String message = "" +
					"There is an unapproved request for: " + request.getEmployee().getFullName() + "\n\n" +
					"The request date is: " + request.getStartDate() + " @ " + request.getStartTime() + "\n" +
					"The request was entered on: " + request.getRequestCreated() + " " +
					"by " + request.getCreator().getFullName() + "\n\n" +
					"As this request is already older than two days, it should have been approved by now.\n\n" +
					"Please take action on this request by following the link." +
					"";

				List<String> notifyList = new ArrayList<String>();

				boolean notifySelf = false;

				if (notifySelf) {
					log.trace("Adding employee to the send to list...");

					try {
						notifyList.add(request.getEmployee().getEmail());
					} catch (Exception e) {
						messageBean.notifyIT("Damaged PTO request: " + request.getId(), "Request employee has no email.\n" + e.getMessage(), null);
					}
				}

				log.trace("Adding report to the send to list...");

				try {
					notifyList.add(request.getEmployee().getPtoReport().getEmail());
				} catch (Exception e) {
					messageBean.notifyIT("Damaged PTO request: " + request.getId(), "Request DR has no email.\n" + e.getMessage(), null);
				}

				log.trace("Send to the list...");

				for (String emailAddress : notifyList) {
					messageBean.createEmail(emailAddress, title, link, message, null);
				}
			}
		} else {
			log.info("Manually triggered...");
		}

		Date today = getTodaySQL();
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");

		List<String> attachments = new ArrayList<String>();

		log.trace("Generating report for: " + today);

		String fileName = requestBean.createAttendancePDF(today);

		log.trace("Adding filename: " + fileName);

		attachments.add(fileName);

		log.trace("Testing whether this is the morning or afternoon report...");

		log.trace("Setting the email title...");

		String title = (todayCal.get(GregorianCalendar.AM_PM) == 1 ? "Afternoon" : "Morning") + " Daily Attendance Report - " + sdf.format(today);

		log.trace("Setting the email message...");

		String message = "Attached is the PDF of today's PIL attendance.";

		log.trace("Getting the list of people to send to...");

		{
			String sql = "" +
				"SELECT e FROM PtoEmployee e WHERE " +
				"(e.supervisor = true) AND " +
				"(e.terminationDate is null)";

			Query query = entityManager.createQuery(sql);

			@SuppressWarnings("unchecked") List<PtoEmployee> queryResults = query.getResultList();

			supervisors.addAll(queryResults);

			log.trace("Add the developer to the list so that he doesn't get lonely...");

			supervisors.add(getEmployee("smitchell@pubint.com"));

			log.info("Sending attendance report to " + supervisors.size() + " supervisors.");
		}

		log.trace("Spin through the list and send out the emails...");

		for (PtoEmployee emp : supervisors) {
			log.trace("Sending to: " + emp.getFullName());

			messageBean.createEmail(emp.getEmail(), title, "", message, attachments);
		}
	}

	public boolean isHR(long employeeID) {
		PtoEmployee employee = entityManager.find(PtoEmployee.class, employeeID);

		return isHR(employee);
	}

	public boolean isHR(PtoEmployee employee) {
		if (employee == null) {
			return false;
		}

		log.trace("Testing to see if: " + employee.getFullName() + " is in HR..." + employee.getDepartment().equals(hr));

		return (employee.getDepartment().equals(hr));
	}

	public boolean isIT(long employeeID) {
		PtoEmployee employee = entityManager.find(PtoEmployee.class, employeeID);

		return isIT(employee);
	}

	private boolean isIT(PtoEmployee employee) {
		if (employee == null) {
			return false;
		}

		log.trace("Testing to see if: " + employee.getFullName() + " is in IT...");

		return (employee.getDepartment().equals(it));
	}

	public boolean isReception(long employeeID) {
		PtoEmployee employee = entityManager.find(PtoEmployee.class, employeeID);

		return isReception(employee);
	}

	private boolean isReception(PtoEmployee employee) {
		if (employee == null) {
			return false;
		}

		log.trace("Testing to see if: " + employee.getFullName() + " has reception role...");

		return employee.hasReceptionRole();
	}

	private boolean escalationAllowed(PtoEmployee employee) {
		return (isHR(employee) || isIT(employee) || isReception(employee));
	}

	public String escalationPrivileges(Element element, XPath xpath) throws Exception {
		setupEscalationDepartments();

		log.trace("Testing what level to escalate to...");

		log.trace("Escalation testing environment set...");

		long employeeID = getLong(element, "currentEmployeeID", xpath);

		log.trace("currentEmployeeID: " + employeeID);

		String password = getString(element, "password", xpath);

		log.trace("password: " + password);

		log.trace("Looking for employee: " + employeeID);

		PtoEmployee employee = entityManager.find(PtoEmployee.class, employeeID);

		if (employee == null) {
			log.error("Got no employee!");

			throw new Exception("No record found for currently logged in employee!");
		}

		log.trace("Got employee: " + employee.getFullName());

		if (!password.equals(employee.getPassword())) {
			log.error("Bad password!!!!!!");

			throw new Exception("Bad password entered! Disallow escalation!");
		}

		log.trace("My department: " + employee.getDepartment());

		if (!escalationAllowed(employee)) {
			throw new Exception("No escalation allowed");
		}

		String escalation = "" +
			xmlHeader +
			"<privileges>" +
			"<escalation>" + (escalationAllowed(employee) ? "allowed" : "disallowed") + "</escalation>" +
			"<hr>" + (isHR(employee) ? "1" : "0") + "</hr>" +
			"<admin>" + (isIT(employee) || isHR(employee) ? "1" : "0") + "</admin>" +
			"<reception>" + ((isReception(employee) || isHR(employee)) ? "1" : "0") + "</reception>" +
			"</privileges>";

		return escalation;
	}

	@Deprecated
	public void rollover2013PTO() {
		List<PtoEmployee> emps = activeEmployeeListing();

		GregorianCalendar endOfYear = new GregorianCalendar();

		endOfYear.set(GregorianCalendar.YEAR, 2013);
		endOfYear.set(GregorianCalendar.MONTH, 11);
		endOfYear.set(GregorianCalendar.DAY_OF_MONTH, 31);

		Date reportDate = new Date(endOfYear.getTimeInMillis());

		for (PtoEmployee employee : emps) {
			log.trace("Getting summary for: " + employee.getFullName());

			PtoSummary summ = requestBean.getPtoSummary(employee, reportDate);

			@SuppressWarnings("unused") PtoEmployee writeEmployee = entityManager.find(PtoEmployee.class, employee.getId());

			log.trace("Got a summary: " + summ.getDaysRemaining() + " days remain.");

			Double rolloverDays = summ.getDaysRemaining() < 5 ? summ.getDaysRemaining() : 5.0;

			if (rolloverDays < 0) {
				rolloverDays = 0.0;
			}

			log.trace("Setting rollover to: " + rolloverDays);

			// writeEmployee.setRolloverDays(rolloverDays);
		}
	}

	@Deprecated
	public void rollover2014PTO() {
		log.info("Commencing 2014 automatic PTO rollover adjustments!");
		List<PtoEmployee> emps = activeEmployeeListing();

		GregorianCalendar endOfYear = new GregorianCalendar();

		endOfYear.set(GregorianCalendar.YEAR, 2014);
		endOfYear.set(GregorianCalendar.MONTH, 11);
		endOfYear.set(GregorianCalendar.DAY_OF_MONTH, 31);

		Date reportDate = new Date(endOfYear.getTimeInMillis());

		for (PtoEmployee employee : emps) {
			log.info("Getting summary for: " + employee.getFullName());

			PtoSummary summ = requestBean.getPtoSummary(employee, reportDate);

			PtoEmployee writeEmployee = entityManager.find(PtoEmployee.class, employee.getId());

			log.info("Got a summary: " + summ.getDaysRemaining() + " days remain.");

			Double rolloverDays = summ.getDaysRemaining() < 3 ? summ.getDaysRemaining() : 3.0;

			if (rolloverDays < 0) {
				rolloverDays = 0.0;
			}

			log.info("Setting rollover to: " + rolloverDays);

			PtoGrantedHistory hist = requestBean.getGrantedHistory(writeEmployee, 2015);
			hist.setRolloverDays(rolloverDays);
		}
	}
	
	public void rollover2015PTO() {
		log.info("Commencing 2015 automatic PTO rollover adjustments!");
		List<PtoEmployee> emps = activeEmployeeListing();

		GregorianCalendar endOfYear = new GregorianCalendar();

		endOfYear.set(GregorianCalendar.YEAR, 2016);
		endOfYear.set(GregorianCalendar.MONTH, 11);
		endOfYear.set(GregorianCalendar.DAY_OF_MONTH, 31);

		Date reportDate = new Date(endOfYear.getTimeInMillis());

		for (PtoEmployee employee : emps) {
			log.info("Getting summary for: " + employee.getFullName());

			PtoSummary summ = requestBean.getPtoSummary(employee, reportDate);

			PtoEmployee writeEmployee = entityManager.find(PtoEmployee.class, employee.getId());

			log.info("Got a summary: " + summ.getDaysRemaining() + " days remain.");

			Double rolloverDays = summ.getDaysRemaining() < 3 ? summ.getDaysRemaining() : 3.0;

			if (rolloverDays < 0) {
				rolloverDays = 0.0;
			}

			log.info("Setting rollover to: " + rolloverDays);

			PtoGrantedHistory hist = requestBean.getGrantedHistory(writeEmployee, 2016);
			hist.setRolloverDays(rolloverDays);
		}
	}

	@Override
	@Deprecated
	public void sendNewSystemEmails() {
		System.out.println("Attempting to send the new system notification email!");

		String title = "Using the New PTO System";
		String link = "http://pilpto.pubint.com";
		List<String> attachments = new ArrayList<String>();

		String localPath = "/home/scottm/ptoSystemImages/";
		File localDir = new File(localPath);
		File[] localFiles = localDir.listFiles();

		for (File file : localFiles) {
			log.trace("Adding: " + file.getAbsolutePath());
			attachments.add(file.getAbsolutePath());
		}

		Collections.sort(attachments);

		PtoEmployeeList emps;

		boolean production = false;

		if (production) {
			emps = null; // activeEmployeeListing();
		} else {
			// PtoEmployee scott = entityManager.find(PtoEmployee.class, 242);
			// PtoEmployee jay = entityManager.find(PtoEmployee.class, 114);
			// PtoEmployee diann = entityManager.find(PtoEmployee.class, 79);

			emps = new PtoEmployeeList(); // Add only developer for testing

			// emps.addEmployee(diann);
			// emps.addEmployee(scott);
			// emps.addEmployee(jay);

		}

		log.info("Sending new system email to: " + emps.getEmployees().size() + " employees!");

		for (PtoEmployee employee : emps.getEmployees()) {

			String message = "" +
				"(This is an automatically generated email, please do not reply.)\n\n" +
				"Dear " + employee.getFullName() + ",\n\n" +
				"\tWelcome to 2014! " +
				"We are announcing a new PTO system today that will replace the old system in Lotus Notes. " +
				"An advantage of the new system is that you will also be able to access it from outside the " +
				"office via a home computer, smart phone or tablet with internet access. " +
				"You may log in to this system using this link: " + link + " in your web browser. " +
				"At this point it is suggested that you bookmark the page for easier access later. " +
				"Please begin using this system on Monday morning (1/6) as access to Notes will be " +
				"restricted as of then. Instructions follow for basic use of the system. PTO Reports: " +
				"instructions for acting upon requests will follow in a separate email.\n\n" +

				"\tYou log into the system by using using your email address and a password. " +
				"The password is initially set to a random value as noted below:\n\n" +
				"User name: " + employee.getEmail() + "\n" +
				"Password: " + employee.getPassword() + "\n\n" +
				"(See below image: 1-loggingIn.png)\n\n" +

				"\tWhen you log in you will see a menu on the left side of the screen. The options on this menu will " +
				"allow you to change your password, check the daily attendance, and create requests. You can also click " +
				"the button marked 'Load Employees' to view a list of current employees, and click their first name to " +
				"pull up basic information about that person.\n" +
				"(See below image: 2-mainMenu.png)\n\n" +

				"\tThe first thing you should probably do in the new system is change your password. Click on 'Change Password' " +
				"from the Administration menu. (See below image: 3-changePass.png) Enter your current password (shown above in this " +
				"email) in the first box, then " +
				"enter your new password in both of the following two boxes. Remember, this is an internet application so " +
				"refrain from using any obvious passwords (such as 'password'), and you must use letters and numbers in " +
				"combination. You will need a new password having a minimum length of 6 characters, and a maximum of 14, and " +
				"the password must contain at least one upper-case letter (A-Z) and at least one number (0-9). \n\n" +

				"\tTo request PTO, choose 'Request PTO' from the menu. (See below image:  4-chooseRequest.png) " +
				"This will take you to the request " +
				"screen, where you can enter the type of request and the starting and ending dates and times. " +
				"First, choose a request type from the drop-down menu under your name. " +
				"(See below image: 5-requestingPTO-step1.png) Once the " +
				"request type is chosen, enter the date and time you will first be absent from the office, and then the " +
				"date and time when you will return. (See below image: 6-requestingPTO-step2.png) If you wish to add a comment " +
				"to your request, you can enter " +
				"your notes in the box labeled 'Comment'. Once you are satisfied with the request you have entered click " +
				"the button marked 'Send Request'. This will process the request and send email notifications to you and " +
				"your PTO report for approval. Once you get confirmation of your request being sent " +
				"(See below image: 7-requestingPTO-step3.png), click 'Close  Window' which will take you " +
				"back to the menu screen.\n\n" +

				"\tTo view the daily attendance, hover over the menu labeled 'Reports', and then click on the option for " +
				"'Daily Attendance'. Select a date using the date picker drop-down and click 'Generate Report'. This " +
				"will list all absences for the date you selected.\n" +
				"(See below image: 8-chooseDailyAtt.png)\n\n" +

				"\t If you do not need to enter a request at this time, log out of the system by hovering over the " +
				"'Logout' menu and clicking 'Exit the PTO System'. (See below image: 9-logout.png)\n\n" +

				"Thank you for your attention,\nPIL Information Technology Staff";

			log.trace("Sending new system notification to: " + employee.getFullName());
			messageBean.createEmail(employee.getEmail(), title, link, message, attachments);
		}
	}
	
	@Override
	public String resetPassword(Element element, XPath xpath) {
		long empID = getLong(element, "employeeID", xpath);
		String email = getString(element, "email", xpath);
		
		PtoEmployee employee = entityManager.find(PtoEmployee.class, empID);
		
		if (! email.equals(employee.getEmail())) {
			employee.setEmail(email);
		}
		
		return sendNewPasswordEmail(employee);
	}

	@Override
	public String sendNewPasswordEmail(PtoEmployee employee) {
		String retVal = "failure";
		
		System.out.println("Resetting password for " + employee.getFullName() + 
			" and sending notification email!");

		String title = "PTO System";
		String link = "http://pilpto.pubint.com:8090";
		List<String> attachments = new ArrayList<String>();

		employee.generatePassword();

		String message = "" +
			"(This is an automatically generated email, please do not reply.)\n\n" +
			"Greetings " + employee.getFullName() + ", and welcome to Publications International!\n\n" +
			"You now have access to the PIL PTO System. You can reach it through this link: " + link +"\n\n"+
			"Your Login Name is " + employee.getEmail() + " and your password is " + employee.getPassword() + "\n\n"+
			"Once you log in, you may change your password to something more memorable through the Administration menu.\n\n" +
			"More comprehensive instructions can be found in the current PIL Policy/Guidelines document.\n\n" +
			"If you have any trouble, please do not hesitate to contact the IT department.\n";


		log.info("Sending new password notification to: " + employee.getFullName());
		try {
			messageBean.createEmail(employee.getEmail(), title, link, message, attachments);
			// include a copy to developer for now.
			messageBean.createEmail("smitchell@pubint.com", title, link, message, attachments);
			messageBean.createEmail("eheidemann@pubint.com", title, link, message, attachments);
			retVal = "Success";
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return retVal;
	}

	public void populateGrantedHistory(Date effectiveDate) {
		GregorianCalendar effectiveCal = null;

		try {
			effectiveCal = new GregorianCalendar();
			effectiveCal.setTimeInMillis(effectiveDate.getTime());
			effectiveCal.set(GregorianCalendar.MONTH, 11);
			effectiveCal.set(GregorianCalendar.DAY_OF_MONTH, 31);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		Integer year = effectiveCal.get(GregorianCalendar.YEAR);

		List<PtoEmployee> employees = activeEmployeeListing();

		for (PtoEmployee employee : employees) {
			log.trace("Building PTO history record for: " + employee.getFullName());

			try {
				PtoGrantedHistory hist = requestBean.getGrantedHistory(employee, year);

				if (hist == null) {
					hist = new PtoGrantedHistory();
				}

				PtoSummary summ = requestBean.getPtoSummary(employee, effectiveDate);

				hist.setEmployee(employee);
				hist.setYear(year);

				hist.setBasePTO(summ.getStartingDaysPTO() - employee.getRolloverDays() - employee.getPtoAdjustment());
				hist.setCompTimeGranted(employee.getCompTimeGranted());
				hist.setCompTimeExpireDate(employee.getCompTimeExpireDate());
				hist.setPtoAdjustment(employee.getPtoAdjustment());
				hist.setPtoAdjustmentExpireDate(employee.getPtoAdjustmentExpireDate());
				hist.setRolloverDays(employee.getRolloverDays());

				hist.setBaseOneToThree(summ.getOneToThreeGranted());
				hist.setBonusOneToThree(employee.getBonusOneToThree());

				if (employee.getBonusOneToThree() > 0) {
					hist.setBonusOneToThreeExpireDate(new java.sql.Date(effectiveCal.getTimeInMillis()));
				}

				log.trace("About to persist new history!");

				entityManager.persist(hist);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
	}
}
