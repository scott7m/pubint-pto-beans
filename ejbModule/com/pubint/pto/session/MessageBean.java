package com.pubint.pto.session;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.pubint.pto.entity.Notification;
import com.pubint.pto.entity.NotificationRecipient;
import com.pubint.pto.entity.PtoDepartment;
import com.pubint.pto.entity.PtoEmployee;
import com.pubint.pto.entity.PtoRequest;
import com.pubint.pto.exceptions.InvitationException;
import com.pubint.pto.sessionInterfaces.MessageLocal;
import com.threepillar.labs.meeting.Invite;
import com.threepillar.labs.meeting.Participant;
import com.threepillar.labs.meeting.ParticipantType;
import com.threepillar.labs.meeting.email.EmailInviteImpl;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class MessageBean extends PuppetBean implements MessageLocal {
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger("messageBean");

	private static String relay = "172.16.1.13";
	private static String source = "PTO-System@pubint.com";
	private static String notify = "jmchugh@pubint.com";
	private static String debugger = "smitchell@pubint.com";

	private boolean liveEmail = true;

	@PersistenceContext(unitName = "ptoJPA")
	private EntityManager entityManager;

	public MessageBean() {
		super();

		liveEmail = (getHostName().equalsIgnoreCase("pil-pto"));

		log.setLevel(Level.TRACE);
		log.trace("Hostname: " + getHostName());

		log.info("Send live email: " + liveEmail);
	}

	private void createEmail(String emailTo, String title, String message, List<String> fileList) {
		if (isEmptyString(emailTo)) {
			log.warn("Cannot send message - no send to...");

			return;
		}

		Properties properties = System.getProperties();
		Transport transport = null;

		properties.put("mail.smtp.host", relay);

		if (! emailTo.toUpperCase().contains("PUBINT.COM")) {
			properties.put("mail.smtp.dsn.notify", "FAILURE ORCPT=rfc822;" + notify);
		}

		properties.put("mail.smtp.dsn.ret", "HDRS");

		Session session = Session.getInstance(properties, null);
		session.setDebug(false);

		MimeMessage mailMsg = new MimeMessage(session);
		InternetAddress[] addresses = null;

		try {
			transport = session.getTransport("smtp");
			transport.connect(relay, "", "");
		} catch (MessagingException cme) {
			log.error("Exception connecting to the mail server: " + cme.getMessage(), cme);
		}

		try {
			log.trace("Attempting to send email to:");
			log.trace("Send To: " + emailTo);
			log.trace("Subject: " + title);
			log.trace("Attachments:");

			if (fileList != null) {
				for (String name : fileList) {
					log.trace(name);
				}
			}

			log.trace("Message: " + message);

			addresses = InternetAddress.parse(emailTo, false);
			mailMsg.setRecipients(Message.RecipientType.TO, addresses);

			mailMsg.setFrom(new InternetAddress(source));

			mailMsg.setSubject(title);

			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(message);
			Multipart multiPart = new MimeMultipart();
			multiPart.addBodyPart(messageBodyPart);

			if (fileList != null) {
				DataSource source;

				for (String fileName : fileList) {
					if (new File(fileName).exists()) {
						messageBodyPart = new MimeBodyPart();
						source = new FileDataSource(fileName);
						messageBodyPart.setDataHandler(new DataHandler(source));
						messageBodyPart.setFileName(fileName);

						multiPart.addBodyPart(messageBodyPart);
					}
				}
			}

			mailMsg.setContent(multiPart);

			if (! emailTo.toUpperCase().contains("PUBINT.COM")) {
				mailMsg.setHeader("Disposition-Notification-To", notify);
			}

			transport.sendMessage(mailMsg, mailMsg.getAllRecipients());
		} catch (AddressException ae) {
			log.warn("Address error: " + ae.getMessage());
		} catch (MessagingException me) {
			log.error("Messaging error: " + me.getMessage(), me);
		} catch (Exception e) {
			log.error("Unhandled exception in message send: " + e.getMessage(), e);
		}

		try {
			transport.close();
		} catch (MessagingException cme) {
			log.error("Exception closing the mail server: " + cme.getMessage(), cme);
		}
	}

	// create email to email address
	@Override
	public void createEmail(String email, String title, String link, String message, List<String> fileList) {
		if (isEmptyString(link) && isEmptyString(message)) {
			return;
		}

		String combinedMessage = "" +
			(notEmptyString(link) ? message + "\n\n" + link : message);

		if (liveEmail) {
			createEmail(email, title, combinedMessage, fileList);
		} else {
			createEmail(debugger, "debugging: " + title + " (target: " + email + ")", combinedMessage, fileList);
			createEmail(notify, "debugging: " + title + " (target: " + email + ")", combinedMessage, fileList);
		}
	}

	@Override
	public void createEmail(Notification notice) {
		for (NotificationRecipient recipient : notice.getTemplate().getRecipients()) {
			for (String email : NotificationRecipient.getRecipientList(entityManager, recipient)) {
				if (liveEmail) {
					createEmail(email, notice.getTitle(), notice.getMessage(), null);
				} else {
					createEmail(debugger, notice.getTitle(), notice.getMessage(), null);
					createEmail(notify, notice.getTitle(), notice.getMessage(), null);
				}
			}
		}
	}

	public void notifyIT(String title, String message, String link) {
		PtoDepartment it = entityManager.find(PtoDepartment.class, - 2);

		String sql = "" +
			"select e from PtoEmployee e " +
			"where " +
			"(e.department = :it)";

		Query getIT = entityManager.createQuery(sql);
		getIT.setParameter("it", it);

		@SuppressWarnings("unchecked") List<PtoEmployee> itWonks = getIT.getResultList();

		for (PtoEmployee itWonk : itWonks) {
			createEmail(itWonk.getEmail(), title, message, null);
		}
	}

	public void sendInvitation(PtoRequest request) throws InvitationException {
		boolean skip = true;

		if (skip) {
			return;
		}

		Properties properties = System.getProperties();

		properties.put("mail.smtp.host", relay);
		properties.put("mail.smtp.port", 25);
		properties.put("mail.smtp.auth", "false");

		properties.put("mail.smtp.dsn.ret", "HDRS");

		Invite invite = new EmailInviteImpl(properties);

		String subject = "PTO Request for " + request.getStartDate();

		String description = "" +
			"This is a PTO request entered by: " + request.getCreator() + "\n" +
			"With a starting date of: " + request.getStartDate() + " " + request.getStartTime() + "\n" +
			"And and end date of: " + request.getEndDate() + " " + request.getEndTime() + "\n" +
			"\n" +
			"The note attached to this request is: \n" +
			request.getComment() + "\n" +
			"And the request was approved by: " + "Your boss" + "\n" +
			"On: " + "Approval date " + " at " + "approval time" + "\n" +
			"\n" +
			"Notification sent: " + now() +
			"";

		Participant inviter = new Participant("PTO System", "ptoAdmin@pubint.com", ParticipantType.OPTIONAL);

		List<Participant> attendeeList = new ArrayList<Participant>();

		Participant requestFor = new Participant(request.getEmployee().getFullName(), request.getEmployee().getEmail(), ParticipantType.REQUIRED);

		attendeeList.add(requestFor);

		try {
			invite.sendInvite(subject, description, inviter, attendeeList, request.getStartDate(), request.getEndDate(), "PIL");
		} catch (Exception e) {
			throw new InvitationException("Unable to send invitations: " + e.getMessage(), e);
		}
	}
}
