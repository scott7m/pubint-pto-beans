package com.pubint.pto.session;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.xpath.XPath;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import com.pubint.pto.base.PTOBase;
import com.pubint.pto.entity.Notification;
import com.pubint.pto.entity.NotificationRecipient;
import com.pubint.pto.entity.NotificationTemplate;
import com.pubint.pto.entity.PtoDepartment;
import com.pubint.pto.entity.PtoEmployee;
import com.pubint.pto.entityList.NotificationTemplateList;
import com.pubint.pto.sessionInterfaces.MessageLocal;
import com.pubint.pto.sessionInterfaces.NotificationLocal;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class NotificationBean extends PTOBase implements NotificationLocal {
	private static final long serialVersionUID = 1L;

	public static final String NOTIFICATION_TEMPLATE_NOT_FOUND = "<error isError='true'><errorReason>Notification template not found!</errorReason><errorMessage>Cannot find the selected notification template.</errorMessage></error>";

	private static final String xmlHeader = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

	private static Logger log = Logger.getLogger("notificationBean");

	@PersistenceContext(unitName = "ptoJPA")
	private EntityManager entityManager;

	@EJB
	private MessageLocal messageBean;

	public NotificationBean() {
		super();

		log.setLevel(Level.TRACE);

		log.trace("Creating notification bean...");
	}

	@Override
	public String sendXML(long id) {
		NotificationTemplate template = entityManager.find(NotificationTemplate.class, id);

		if (template == null) {
			return NOTIFICATION_TEMPLATE_NOT_FOUND;
		}

		StringWriter xml = new StringWriter();

		try {
			JAXBContext context = JAXBContext.newInstance(template.getClass());

			Marshaller m = context.createMarshaller();

			m.marshal(template, xml);
		} catch (JAXBException je) {
			log.fatal("JAXB Exception: " + je.getMessage(), je);
		}

		log.trace("XML: " + fixXML(xml));

		return fixXML(xml);
	}

	@Override
	public String selectList() {
		String sql = "" +
			"select t from NotificationTemplate t " +
			"order by t.key";

		Query query = entityManager.createQuery(sql);

		@SuppressWarnings("unchecked") List<NotificationTemplate> results = query.getResultList();

		NotificationTemplateList templates = new NotificationTemplateList();

		for (NotificationTemplate template : results) {
			templates.addTemplate(template);
		}

		StringWriter xml = new StringWriter();

		try {
			JAXBContext context = JAXBContext.newInstance(templates.getClass());

			Marshaller m = context.createMarshaller();

			m.marshal(templates, xml);
		} catch (JAXBException je) {
			log.fatal("JAXB Exception: " + je.getMessage(), je);
		}

		log.info("XML: " + fixXML(xml));

		return fixXML(xml);
	}

	@Override
	public String update(Element element, XPath xpath) {
		long templateID = getLong(element, "templateID", xpath);

		String notificationKey = getString(element, "notificationKey", xpath);
		String templateText = getString(element, "template", xpath);

		NotificationTemplate template = entityManager.find(NotificationTemplate.class, templateID);

		if (template == null) {
			log.trace("Creating a new template...");

			template = new NotificationTemplate(notificationKey, templateText);

			entityManager.persist(template);
		} else {
			log.trace("Updating template...");

			template.setKey(notificationKey);
			template.setText(templateText);
		}

		return sendXML(template.getId());
	}

	@Override
	public String addRecipient(Element element, XPath xpath) {
		String retXML = xmlHeader;

		long templateID = getLong(element, "templateID", xpath);

		long employeeID = getLong(element, "empID", xpath);
		long departmentID = getLong(element, "deptID", xpath);

		PtoEmployee employee = entityManager.find(PtoEmployee.class, employeeID);
		PtoDepartment department = entityManager.find(PtoDepartment.class, departmentID);

		NotificationTemplate template = entityManager.find(NotificationTemplate.class, templateID);

		if ((employee == null) && (department == null)) {
			log.warn("Cannot add recipient...neither employee nor department found...");

			return retXML + "<xmlRet><errorMsg>Unable to add recipient. Not found.</errorMsg></xmlRet>";
		}

		String sql = "" +
			"SELECT r from NotificationRecipient r WHERE " +
			"(r.template.id = :templateID) AND " +
			"(r.employee.id = :employeeID) AND " +
			"(r.department.id = :departmentID)";

		Query query = entityManager.createQuery(sql);
		query.setParameter("templateID", templateID);
		query.setParameter("employeeID", employeeID == 0 ? null : employeeID);
		query.setParameter("departmentID", departmentID == 0 ? null : departmentID);

		NotificationRecipient recipient = null;

		try {
			recipient = (NotificationRecipient) query.getSingleResult(); 
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		if (recipient != null) {
			log.warn("Recipient already exists in this template!");
			return retXML + "<errorMsg>Unable to add recipient. They are already in the list.</errorMsg>";
		}

		recipient = new NotificationRecipient(template, employee, department);
		template.addRecipient(recipient);

		String retVal = retXML + 
			"<xmlRet>" + 
			"<errorMsg></errorMsg>" + 
			"<empID>" + employeeID + "</empID>" + 
			"<empName>" + recipient.getEmployeeName() + "</empName>" +
			"</xmlRet>";

		return retVal;
	}

	@Override
	public String dropRecipient(Element element, XPath xpath) {
		String retXML = xmlHeader;

		log.trace("Dropping a notification recipient.");

		long employeeID = 0;
		try {
			long templateID = getLong(element, "templateID", xpath);

			employeeID = getLong(element, "empID", xpath);
			long departmentID = getLong(element, "departmentID", xpath);

			log.trace("Temp: " + templateID + " Dept: " + departmentID + " Emp: " + employeeID);

			PtoEmployee employee = entityManager.find(PtoEmployee.class, employeeID);
			PtoDepartment department = entityManager.find(PtoDepartment.class, departmentID);

			NotificationTemplate template = entityManager.find(NotificationTemplate.class, templateID);

			log.trace("Template Recips: " + template.getRecipients());

			if ((employee == null) && (department == null)) {
				log.warn("Cannot drop recipient...neither employee nor department found...");

				return retXML + "<errorMsg>Unable to drop recipient.</errorMsg>";
			}

			String sql = "" +
				"SELECT r from NotificationRecipient r WHERE " +
				"(r.template.id = :templateID) AND " +
				"(r.employee.id = :employeeID) AND " +
				"(r.department.id = :departmentID)";

			Query query = entityManager.createQuery(sql);
			query.setParameter("templateID", templateID);
			query.setParameter("employeeID", employeeID == 0 ? null : employeeID);
			query.setParameter("departmentID", departmentID == 0 ? null : departmentID);

			NotificationRecipient recipient = null;

			try {
				recipient = (NotificationRecipient) query.getSingleResult(); 
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}

			if ((recipient == null) || (recipient.getId() == null)) {
				log.warn("Recipient not found!");
				return retXML + "<errorMsg>Unable to drop recipient. Not found.</errorMsg>";
			}

			log.trace("Got the recipient: " + recipient.getId());

			template.dropRecipient(recipient);

			log.trace("Recipient dropped!");
		} catch (NumberFormatException e) {
			log.error(e.getMessage(), e);
		}

		log.trace(retXML + "<xmlRet><errorMsg></errorMsg><pilID>" + employeeID + "</pilID></xmlRet>");
		return retXML + "<xmlRet><errorMsg></errorMsg><pilID>" + employeeID + "</pilID></xmlRet>";
	}

	private List<NotificationTemplate> getTemplatesWithLikeKey(String key) {
		log.info("Grabbing the list of templates that have a key like: " + key);

		List<NotificationTemplate> templates = new ArrayList<NotificationTemplate>();

		String sql = "" +
			"select t from NotificationTemplate t " +
			"where " +
			"(t.key like :key)";

		Query query = entityManager.createQuery(sql);
		query.setParameter("key", "%" + key + "%");

		if (query.getResultList().isEmpty()) {
			log.warn("There are no templates that match this key: " + key);
		}

		@SuppressWarnings("unchecked") List<NotificationTemplate> results = query.getResultList();

		for (NotificationTemplate template : results) {
			log.trace("Found template: " + template.getKey() + " - " + template.getTitle());

			templates.add(template);
		}

		return templates;
	}

	@Override
	public void sendNewHireNotifications(Serializable parameters) {
		log.info("Attempting to process new hire notices...");

		String sql = "" +
			"select e from PtoEmployee e " +
			"where " +
			"(e.terminationDate is null) and " +
			"(e.newHireNoticeSent = false)";

		log.info("Performing query of employees that have not yet been notified for...");

		Query query = entityManager.createQuery(sql);

		if (! query.getResultList().isEmpty()) {
			log.info("Found employees to notify for...");

			@SuppressWarnings("unchecked") List<PtoEmployee> employees = query.getResultList();

			log.trace("Grab list of new hire notice templates...");

			List<NotificationTemplate> templates = getTemplatesWithLikeKey("newHire");

			log.trace("Spin through new employees...");

			for (PtoEmployee newEmployee : employees) {
				log.info("Creating new hire notices for: " + newEmployee.getFullName());

				for (NotificationTemplate template : templates) {
					log.trace("Building notification and substituting the text...");

					Notification notice = new Notification(template, newEmployee);

					log.trace("Message: " + notice.getMessage());

					entityManager.persist(notice);
				}

				newEmployee.setNewHireNoticeSent(true);
			}
		}

		sendNotifications();
	}

	private void sendNotifications() {
		String sql = "" +
			"select n from Notification n " +
			"where " +
			"(n.processed = false)";

		Query query = entityManager.createQuery(sql);

		if (query.getResultList().isEmpty()) {
			log.warn("No notifications waiting to be processed...Exiting...");

			return;
		}

		@SuppressWarnings("unchecked") List<Notification> queryResults = query.getResultList();

		log.trace("Spin through the list and send out the emails...");

		for (Notification newHire : queryResults) {
			log.trace("Sending notice: " + newHire.getId());

			messageBean.createEmail(newHire);

			newHire.setProcessed(true);
		}
	}
}
