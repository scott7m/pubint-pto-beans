package com.pubint.pto.session;

import java.io.Serializable;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.xpath.XPath;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import com.pubint.pto.entity.PtoLocation;
import com.pubint.pto.entity.PtoTimeType;
import com.pubint.pto.entity.PtoTimeTypeCategory;
import com.pubint.pto.entityList.PtoTimeTypeCategoryList;
import com.pubint.pto.entityList.PtoTimeTypeList;
import com.pubint.pto.sessionInterfaces.PtoTimeTypeLocal;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PtoTimeTypeBean extends PuppetBean implements PtoTimeTypeLocal, Serializable {
	private static final long serialVersionUID = 3L;

	private static Logger log = Logger.getLogger("ptoTimeTypeBean");

	@PersistenceContext(unitName = "ptoJPA")
	private EntityManager entityManager;

	private SimpleDateFormat monthOnlyFormat = new SimpleDateFormat("MMMM");

	public PtoTimeTypeBean() {
		super();

		log.setLevel(Level.INFO);
	}

	@Override
	public String sendXML(long timeID) {
		StringWriter xml = new StringWriter();

		PtoTimeType timeType = entityManager.find(PtoTimeType.class, timeID);

		if (timeType != null) {
			try {
				JAXBContext context = JAXBContext.newInstance(timeType.getClass());

				Marshaller m = context.createMarshaller();

				m.marshal(timeType, xml);
			} catch (JAXBException je) {
				log.fatal("JAXB Exception: " + je.getMessage(), je);
			}
		}

		log.trace("XML: " + fixXML(xml));

		return fixXML(xml);
	}

	@Override
	public String selectList() {
		StringWriter xml = null;

		try {
			String sql = "select t from PtoTimeType t ";

			Query query = entityManager.createQuery(sql);

			@SuppressWarnings("unchecked") List<PtoTimeType> results = query.getResultList();

			PtoTimeTypeList timeTypes = new PtoTimeTypeList();

			for (PtoTimeType time : results) {
				timeTypes.addTimeType(time);
			}

			xml = new StringWriter();

			try {
				JAXBContext context = JAXBContext.newInstance(timeTypes.getClass());

				Marshaller m = context.createMarshaller();

				m.marshal(timeTypes, xml);
			} catch (JAXBException je) {
				log.fatal("JAXB Exception: " + je.getMessage(), je);
			}

			log.info("XML: " + fixXML(xml));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fixXML(xml);
	}

	private java.sql.Date parseBreakDate(String workDateString) {
		try {
			return new java.sql.Date(monthOnlyFormat.parse(workDateString).getTime());
		} catch (ParseException e) {
			log.error("Parse exception converting break date (" + workDateString + "): " + e.getMessage(), e);

			return null;
		}
	}

	@Override
	public String update(Element element, XPath xpath) {
		long timeID = this.getLong(element, "timeID", xpath);

		PtoTimeType timeType = entityManager.find(PtoTimeType.class, timeID);

		if (timeType == null) {
			timeType = new PtoTimeType();

			entityManager.persist(timeType);
		}

		long locID = this.getLong(element, "locID", xpath);

		if (locID > 0) {
			log.trace("Valid location ID (" + locID + ")...looking up location...");

			PtoLocation loca = entityManager.find(PtoLocation.class, locID);

			log.trace("Found location: " + loca.getName());

			timeType.setOffice(loca);
		} else {
			timeType.setOffice(null);
		}

		long typeTypeID = this.getLong(element, "typeTypeID", xpath);

		if (typeTypeID > 0) {
			log.trace("Valid time type category ID (" + typeTypeID + ")...locking up time type category...");

			PtoTimeTypeCategory typeType = entityManager.find(PtoTimeTypeCategory.class, typeTypeID);

			log.trace("Found type category: " + typeType.getDescription());

			timeType.setTimeTypeCategory(typeType);
		} else {
			timeType.setTimeTypeCategory(null);
		}

		timeType.setDescription(this.getString(element, "description", xpath));

		timeType.setYearBreak1(this.getLong(element, "yrsBrk1", xpath));
		timeType.setYear1MaxPTO(this.getLong(element, "yrsBrk1Max", xpath));
		timeType.setYearBreak2(this.getLong(element, "yrsBrk2", xpath));
		timeType.setYear2MaxPTO(this.getLong(element, "yrsBrk2Max", xpath));
		timeType.setYearBreak3(this.getLong(element, "yrsBrk3", xpath));
		timeType.setYear3MaxPTO(this.getLong(element, "yrsBrk3Max", xpath));

		timeType.setMaxDaysAllowed(this.getLong(element, "maximum", xpath));
		timeType.setMaxRollover(this.getLong(element, "maximumRollover", xpath));

		timeType.setYear1Break1(parseBreakDate(getString(element, "brk1Date", xpath)));
		timeType.setYear1Break1Days(this.getLong(element, "brk1Qty", xpath));

		timeType.setYear1Break2(parseBreakDate(getString(element, "brk2Date", xpath)));
		timeType.setYear1Break2Days(this.getLong(element, "brk2Qty", xpath));

		timeType.setYear1Break3(parseBreakDate(getString(element, "brk3Date", xpath)));
		timeType.setYear1Break3Days(this.getLong(element, "brk3Qty", xpath));

		timeType.setYear1Break4(parseBreakDate(getString(element, "brk4Date", xpath)));
		timeType.setYear1Break4Days(this.getLong(element, "brk4Qty", xpath));

		timeType.setYear1Break5(parseBreakDate(getString(element, "brk5Date", xpath)));
		timeType.setYear1Break5Days(this.getLong(element, "brk5Qty", xpath));

		timeType.setYear1Break6(parseBreakDate(getString(element, "brk6Date", xpath)));
		timeType.setYear1Break6Days(this.getLong(element, "brk6Qty", xpath));

		timeType.setYear1Break7(parseBreakDate(getString(element, "brk7Date", xpath)));
		timeType.setYear1Break7Days(this.getLong(element, "brk7Qty", xpath));

		timeType.setYear1Break8(parseBreakDate(getString(element, "brk8Date", xpath)));
		timeType.setYear1Break8Days(this.getLong(element, "brk8Qty", xpath));

		timeType.setYear1Break9(parseBreakDate(getString(element, "brk9Date", xpath)));
		timeType.setYear1Break9Days(this.getLong(element, "brk9Qty", xpath));

		timeType.setYear1Break10(parseBreakDate(getString(element, "brk10Date", xpath)));
		timeType.setYear1Break10Days(this.getLong(element, "brk10Qty", xpath));

		timeType.setYear1Break11(parseBreakDate(getString(element, "brk11Date", xpath)));
		timeType.setYear1Break11Days(this.getLong(element, "brk11Qty", xpath));

		timeType.setYear1Break12(parseBreakDate(getString(element, "brk12Date", xpath)));
		timeType.setYear1Break12Days(this.getLong(element, "brk12Qty", xpath));

		return sendXML(timeType.getId());
	}

	@Override
	public String remove(Element element, XPath xpath) {
		String retVal = "fail";

		long timeID = this.getLong(element, "timeID", xpath);

		PtoTimeType timeType = entityManager.find(PtoTimeType.class, timeID);

		if (timeType != null) {
			entityManager.remove(timeType);
			retVal = "done";
		}

		return retVal;
	}

	@Override
	public PtoTimeType getTimeType(PtoTimeTypeCategory type, PtoLocation loc) {
		PtoTimeType retVal = null;

		try {
			String sql = "" +
				"SELECT t from PtoTimeType t " +
				"WHERE " +
				"(t.timeTypeCategory.id = :typeID) AND " +
				"(t.office.id = :locID)";

			Query query = entityManager.createQuery(sql);
			query.setParameter("typeID", type.getId());
			query.setParameter("locID", loc.getId());

			retVal = (PtoTimeType) query.getSingleResult();

			if (retVal == null) {
				sql = "" +
					"SELECT t from PtoTimeType t " +
					"WHERE " +
					"(t.timeTypeCategory.id = :typeID) AND " +
					"(t.PtoLocation is null)";
			}
		} catch (Exception e) {
			log.error(e);
		}

		return retVal;
	}

	@Override
	public String typeSelectList() {
		StringWriter xml = null;

		try {
			String sql = "" +
				"select t from PtoTimeTypeCategory t " +
				"order by t.id";

			Query query = entityManager.createQuery(sql);

			@SuppressWarnings("unchecked") List<PtoTimeTypeCategory> results = query.getResultList();

			PtoTimeTypeCategoryList typeCategories = new PtoTimeTypeCategoryList();

			for (PtoTimeTypeCategory category : results) {
				typeCategories.addTimeTypeCategory(category);
			}

			xml = new StringWriter();

			try {
				JAXBContext context = JAXBContext.newInstance(typeCategories.getClass());

				Marshaller m = context.createMarshaller();

				m.marshal(typeCategories, xml);
			} catch (JAXBException je) {
				log.fatal("JAXB Exception: " + je.getMessage(), je);
			}

			log.trace("XML: " + fixXML(xml));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fixXML(xml);
	}
}
