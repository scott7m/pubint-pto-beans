package com.pubint.pto.session;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.xpath.XPath;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import com.pubint.pto.entity.PtoRequest;
import com.pubint.pto.entity.PtoRequestType;
import com.pubint.pto.entity.PtoTimeType;
import com.pubint.pto.entityList.PtoRequestTypeList;
import com.pubint.pto.sessionInterfaces.PtoEmployeeLocal;
import com.pubint.pto.sessionInterfaces.PtoRequestTypeLocal;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PtoRequestTypeBean extends PuppetBean implements PtoRequestTypeLocal, Serializable {
	private static final long serialVersionUID = 3L;

	private static Logger log = Logger.getLogger("ptoRequestTypeBean");

	@PersistenceContext(unitName = "ptoJPA")
	private EntityManager entityManager;

	@EJB
	private PtoEmployeeLocal employeeBean;

	public PtoRequestTypeBean() {
		super();

		log.setLevel(Level.INFO);
	}

	@Override
	public String sendXML(long requestID) {
		StringWriter xml = new StringWriter();

		try {
			PtoRequestType requestType = entityManager.find(PtoRequestType.class, requestID);

			if (requestType != null) {
				try {
					JAXBContext context = JAXBContext.newInstance(requestType.getClass());

					Marshaller m = context.createMarshaller();

					m.marshal(requestType, xml);
				} catch (JAXBException je) {
					log.fatal("JAXB Exception: " + je.getMessage(), je);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// log.trace("XML: " + fixXML(xml));

		return fixXML(xml);
	}

	@Override
	public String selectList(Element element, XPath xpath) {
		long employeeID = getLong(element, "currentEmployeeID", xpath);
		long requestID = getLong(element, "requestID", xpath);

		PtoRequest request = entityManager.find(PtoRequest.class, requestID);

		StringWriter xml = null;

		try {
			String sql = "select t from PtoRequestType t ";

			log.trace("Current user's employeeID: " + employeeID);

			log.trace("Checking whether the current user is in HR...");

			if (! employeeBean.isHR(employeeID)) {
				log.trace("Current employee not in HR!");

				sql += "WHERE (t.hrOnly = false) ";
			}

			sql += "order by t.description";

			Query query = entityManager.createQuery(sql);

			@SuppressWarnings("unchecked") List<PtoRequestType> results = query.getResultList();

			PtoRequestTypeList requestTypes = new PtoRequestTypeList();

			for (PtoRequestType requestType : results) {
				requestTypes.addRequestType(requestType);
			}

			if (request != null) {
				requestTypes.addRequestType(request.getRequestType());
			}

			xml = new StringWriter();

			try {
				JAXBContext context = JAXBContext.newInstance(requestTypes.getClass());

				Marshaller m = context.createMarshaller();

				m.marshal(requestTypes, xml);
			} catch (JAXBException je) {
				log.fatal("JAXB Exception: " + je.getMessage(), je);
			}

			// log.info("XML: " + fixXML(xml));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fixXML(xml);
	}

	@Override
	public String update(Element element, XPath xpath) {
		long requestID = this.getLong(element, "requestID", xpath);

		PtoRequestType requestType = entityManager.find(PtoRequestType.class, requestID);

		if (requestType == null) {
			requestType = new PtoRequestType();

			entityManager.persist(requestType);
		}

		long timeID = this.getLong(element, "timeID", xpath);

		if (timeID > 0) {
			System.out.println("timeID > 0, looking up time type!");

			PtoTimeType ttype = entityManager.find(PtoTimeType.class, timeID);

			System.out.println("Found time type: " + ttype.getTimeTypeCategory().getDescription());

			requestType.setTimeType(ttype);
		} else {
			requestType.setTimeType(null);
		}

		requestType.setDescription(this.getString(element, "description", xpath));
		requestType.setShortDescription(getString(element, "shortDescription", xpath));
		requestType.setRequireComment(this.getBoolean(element, "commentRequired", xpath));
		requestType.setSpecialRouting(this.getBoolean(element, "specialRouting", xpath));
		// requestType.setUnpaidTime(this.getBoolean(element, "unpaid", xpath));
		requestType.setHrOnly(this.getBoolean(element, "hrOnly", xpath));

		return sendXML(requestType.getId());
	}

	@Override
	public String remove(Element element, XPath xpath) {
		long requestID = this.getLong(element, "requestID", xpath);

		PtoRequestType requestType = entityManager.find(PtoRequestType.class, requestID);

		if (requestType != null) {
			entityManager.remove(requestType);

			return "done";
		}

		return "fail";
	}
}
