package com.pubint.pto.sessionTest;

import java.sql.Date;
import java.sql.Time;
import java.util.GregorianCalendar;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.pubint.pto.entity.PtoEmployee;
import com.pubint.pto.entity.PtoRequest;
import com.pubint.pto.entity.PtoTimeTypeCategory;
import com.pubint.pto.session.PtoRequestBean;

import static org.junit.Assert.*;

public class PtoRequestBeanTest {
	final PtoRequestBean tester = new PtoRequestBean();
	static GregorianCalendar startCal;
	static GregorianCalendar endCal;

	static Date startDate;
	static Date endDate;

	static Time startTime;
	static Time endTime;

	static long empID;
	static long type;

	@BeforeClass
	public static void testSetup() {
		startCal = new GregorianCalendar();
		endCal = new GregorianCalendar();

		startCal.set(GregorianCalendar.YEAR, 2013);
		startCal.set(GregorianCalendar.MONTH, 6);
		startCal.set(GregorianCalendar.DATE, 17);

		endCal.set(GregorianCalendar.YEAR, 2013);
		endCal.set(GregorianCalendar.MONTH, 6);
		endCal.set(GregorianCalendar.DATE, 18);

		startDate = new Date(startCal.getTimeInMillis());
		endDate = new Date(endCal.getTimeInMillis());

		startTime = Time.valueOf("08:30:00");
		endTime = Time.valueOf("08:30:00");

		empID = 3;
		type = PtoTimeTypeCategory.PTO_CHARGEABLE;
	}

	@AfterClass
	public static void testCleanup() {

	}

	@Test
	public void testEmptyString() {
		fail("Handled by PaLMBase testing.");
		// assertTrue("Empty String should return true.",
		// tester.isEmptyString(""));
		// assertTrue("Empty String should return true.",
		// tester.isEmptyString(null));
		// assertTrue("Empty String should return true.",
		// tester.isEmptyString("            "));
		//
		// assertFalse("Non-empty String should return false",
		// tester.isEmptyString("Not empty"));
	}

	@SuppressWarnings("deprecation")
	@Test
	// (expected = NullPointerException.class)
	public void testDaysCalc() {
		fail("Not implemented with data."); // TODO
		// GregorianCalendar startCal = new GregorianCalendar();
		// GregorianCalendar endCal = new GregorianCalendar();
		//
		// startCal.set(GregorianCalendar.YEAR, 2013);
		// startCal.set(GregorianCalendar.MONTH, 6);
		// startCal.set(GregorianCalendar.DATE, 17);
		//
		// endCal.set(GregorianCalendar.YEAR, 2013);
		// endCal.set(GregorianCalendar.MONTH, 6);
		// endCal.set(GregorianCalendar.DATE, 18);
		//
		// Date startDate = new Date(startCal.getTimeInMillis());
		// Date endDate = new Date(endCal.getTimeInMillis());
		//
		// Time startTime = Time.valueOf("08:30:00");
		// Time endTime = Time.valueOf("08:30:00");
		//
		// long empID = 3;
		// long type = PtoTimeTypeCategory.PTO;

		boolean isOneToThree = false;
		PtoEmployee employee = new PtoEmployee();

		employee.setStartTime(startTime);
		employee.setEndTime(endTime);

		double numDays = PtoRequest.calculateNumDays(
			employee,
			isOneToThree,
			startDate,
			endDate,
			startTime,
			endTime,
			employee.getStartTime(),
			employee.getEndTime());

		assertEquals("Should return one day of PTO.", 1.0, numDays);
	}

	@Test
	public void testStringToTime() {
		fail("Implemented in PaLMBaseTest.");
		// Time testTime = Time.valueOf("12:00:00");
		//
		// assertEquals("True if return time is noon.", testTime,
		// tester.stringToTime("12:00 PM"));
	}
}
