package com.pubint.pto.entityList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.pubint.pto.entity.PtoTimeTypeCategory;

@XmlRootElement(name = "timeTypeCategories")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoTimeTypeCategoryList implements Serializable {
	private static final long serialVersionUID = 1L;

	List<PtoTimeTypeCategory> timeCategories;

	public PtoTimeTypeCategoryList() {
		super();

		timeCategories = new ArrayList<PtoTimeTypeCategory>();
	}

	public PtoTimeTypeCategoryList(List<PtoTimeTypeCategory> list) {
		this();

		for (PtoTimeTypeCategory timeType : list) {
			addTimeTypeCategory(timeType);
		}
	}

	@XmlElement(name = "timeTypeCategory")
	public List<PtoTimeTypeCategory> getTimeCategories() {
		return timeCategories;
	}

	public void setTimeTypeCategories(List<PtoTimeTypeCategory> timeCategories) {
		this.timeCategories = timeCategories;
	}

	public void addTimeTypeCategory(PtoTimeTypeCategory timeType) {
		getTimeCategories().add(timeType);
	}
}
