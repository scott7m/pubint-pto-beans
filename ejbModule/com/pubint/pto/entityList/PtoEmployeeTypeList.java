package com.pubint.pto.entityList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.pubint.pto.entity.PtoEmployeeType;

@XmlRootElement(name = "empTypes")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoEmployeeTypeList implements Serializable {
	private static final long serialVersionUID = 1L;

	List<PtoEmployeeType> types;

	public PtoEmployeeTypeList() {
		super();

		types = new ArrayList<PtoEmployeeType>();
	}

	public PtoEmployeeTypeList(List<PtoEmployeeType> list) {
		this();

		for (PtoEmployeeType type : list) {
			addType(type);
		}
	}

	@XmlElement(name = "empType")
	public List<PtoEmployeeType> getTypes() {
		return types;
	}

	public void setTypes(List<PtoEmployeeType> types) {
		this.types = types;
	}

	public void addType(PtoEmployeeType type) {
		getTypes().add(type);
	}
}