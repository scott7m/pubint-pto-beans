package com.pubint.pto.entityList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.pubint.pto.entity.PtoRequestType;

@XmlRootElement(name = "requestTypes")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoRequestTypeList implements Serializable {
	private static final long serialVersionUID = 1L;

	List<PtoRequestType> requestTypes;

	public PtoRequestTypeList() {
		super();

		requestTypes = new ArrayList<PtoRequestType>();
	}

	public PtoRequestTypeList(List<PtoRequestType> list) {
		this();

		for (PtoRequestType requestType : list) {
			addRequestType(requestType);
		}
	}

	@XmlElement(name = "requestType")
	public List<PtoRequestType> getRequestTypes() {
		return requestTypes;
	}

	public void setRequestTypes(List<PtoRequestType> requestTypes) {
		this.requestTypes = requestTypes;
	}

	public void addRequestType(PtoRequestType requestType) {
		getRequestTypes().add(requestType);
	}
}
