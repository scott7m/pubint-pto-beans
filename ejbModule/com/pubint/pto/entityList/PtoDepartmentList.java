package com.pubint.pto.entityList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.pubint.pto.entity.PtoDepartment;

@XmlRootElement(name = "departments")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoDepartmentList implements Serializable {
	private static final long serialVersionUID = 1L;

	List<PtoDepartment> departments;

	public PtoDepartmentList() {
		super();

		departments = new ArrayList<PtoDepartment>();
	}

	public PtoDepartmentList(List<PtoDepartment> list) {
		this();

		for (PtoDepartment department : list) {
			addDepartment(department);
		}
	}

	@XmlElement(name = "department")
	public List<PtoDepartment> getDepartments() {
		return departments;
	}

	public void setDepartments(List<PtoDepartment> departments) {
		this.departments = departments;
	}

	public void addDepartment(PtoDepartment department) {
		getDepartments().add(department);
	}
}