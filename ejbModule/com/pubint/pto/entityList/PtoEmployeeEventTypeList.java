package com.pubint.pto.entityList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.pubint.pto.entity.PtoEmployeeEventType;

@XmlRootElement(name = "empEventTypes")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoEmployeeEventTypeList implements Serializable {
	private static final long serialVersionUID = 1L;

	List<PtoEmployeeEventType> types;

	public PtoEmployeeEventTypeList() {
		super();

		types = new ArrayList<PtoEmployeeEventType>();
	}

	public PtoEmployeeEventTypeList(List<PtoEmployeeEventType> list) {
		this();

		for (PtoEmployeeEventType type : list) {
			addType(type);
		}
	}

	@XmlElement(name = "empEventType")
	public List<PtoEmployeeEventType> getTypes() {
		return types;
	}

	public void setTypes(List<PtoEmployeeEventType> types) {
		this.types = types;
	}

	public void addType(PtoEmployeeEventType type) {
		getTypes().add(type);
	}
}