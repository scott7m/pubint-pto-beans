package com.pubint.pto.entityList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.pubint.pto.entity.PtoCalendar;

@XmlRootElement(name = "calendars")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoCalendarList implements Serializable {
	private static final long serialVersionUID = 1L;

	List<PtoCalendar> calendars;

	public PtoCalendarList() {
		super();

		calendars = new ArrayList<PtoCalendar>();
	}

	public PtoCalendarList(List<PtoCalendar> list) {
		this();

		for (PtoCalendar calendar : list) {
			addCalendar(calendar);
		}
	}

	@XmlElement(name = "calendar")
	public List<PtoCalendar> getCalendars() {
		return calendars;
	}

	public void setCalendars(List<PtoCalendar> calendars) {
		this.calendars = calendars;
	}

	public void addCalendar(PtoCalendar calendar) {
		getCalendars().add(calendar);
	}
}