package com.pubint.pto.entityList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.pubint.pto.entity.PtoEmployee;

@XmlRootElement(name = "employees")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoEmployeeList implements Serializable {
	private static final long serialVersionUID = 1L;

	List<PtoEmployee> employees;

	public PtoEmployeeList() {
		super();

		employees = new ArrayList<PtoEmployee>();
	}

	public PtoEmployeeList(List<PtoEmployee> list) {
		this();

		for (PtoEmployee employee : list) {
			addEmployee(employee);
		}
	}

	@XmlElement(name = "employee")
	public List<PtoEmployee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<PtoEmployee> employees) {
		this.employees = employees;
	}

	public void addEmployee(PtoEmployee employee) {
		if (! getEmployees().contains(employee)) {
			getEmployees().add(employee);
		}
	}
}