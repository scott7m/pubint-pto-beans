package com.pubint.pto.entityList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.pubint.pto.entity.NotificationTemplate;

@XmlRootElement(name = "templates")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class NotificationTemplateList implements Serializable {
	private static final long serialVersionUID = 1L;

	List<NotificationTemplate> templates;

	public NotificationTemplateList() {
		super();

		templates = new ArrayList<NotificationTemplate>();
	}

	public NotificationTemplateList(List<NotificationTemplate> list) {
		this();

		for (NotificationTemplate template : list) {
			addTemplate(template);
		}
	}

	@XmlElement(name = "template")
	public List<NotificationTemplate> getTemplates() {
		return templates;
	}

	public void setTemplates(List<NotificationTemplate> templates) {
		this.templates = templates;
	}

	public void addTemplate(NotificationTemplate template) {
		getTemplates().add(template);
	}
}