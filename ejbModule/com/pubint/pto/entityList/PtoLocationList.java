package com.pubint.pto.entityList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.pubint.pto.entity.PtoLocation;

@XmlRootElement(name = "locations")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoLocationList implements Serializable {
	private static final long serialVersionUID = 1L;

	List<PtoLocation> locations;

	public PtoLocationList() {
		super();

		locations = new ArrayList<PtoLocation>();
	}

	public PtoLocationList(List<PtoLocation> list) {
		this();

		for (PtoLocation location : list) {
			addLocation(location);
		}
	}

	@XmlElement(name = "location")
	public List<PtoLocation> getLocations() {
		return locations;
	}

	public void setLocations(List<PtoLocation> locations) {
		this.locations = locations;
	}

	public void addLocation(PtoLocation location) {
		getLocations().add(location);
	}
}