package com.pubint.pto.entityList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.pubint.pto.entity.PtoGrantedHistory;
import com.pubint.pto.entity.PtoRequest;

@XmlRootElement(name = "requests")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoRequestList implements Serializable {
	private static final long serialVersionUID = 1L;

	PtoGrantedHistory grantedHistory;

	List<PtoRequest> requests;

	public PtoRequestList() {
		super();

		requests = new ArrayList<PtoRequest>();
	}

	public PtoRequestList(List<PtoRequest> list) {
		this();

		for (PtoRequest request : list) {
			addRequest(request);
		}
	}

	@XmlElement(name = "request")
	public List<PtoRequest> getRequests() {
		return requests;
	}

	public void setRequests(List<PtoRequest> requests) {
		this.requests = requests;
	}

	public void addRequest(PtoRequest request) {
		getRequests().add(request);
	}

	@XmlElement(name = "summary")
	public PtoGrantedHistory getGrantedHistory() {
		return grantedHistory;
	}

	public void setGrantedHistory(PtoGrantedHistory grantedHistory) {
		this.grantedHistory = grantedHistory;
	}

	@XmlElement(name = "daysRequested")
	public Double getDaysRequested() {
		double days = 0;

		for (PtoRequest request : requests) {
			if (request.isDead()) {
				continue;
			}

			if (! request.isChargeable()) {
				continue;
			}

			if (request.isOneToThree()) {
				continue;
			}

			days += request.getDaysTaken();
		}

		return days;
	}

	@XmlElement(name = "daysRemaining")
	public Double getDaysRemaining() {
		try {
			return getGrantedHistory().getTotalAvailablePTO() - getDaysRequested();
		} catch (NullPointerException npe) {
			return 0.0;
		}
	}

	@XmlElement(name = "oneToThreeRequested")
	public Integer getOneToThreeRequested() {
		int days = 0;

		for (PtoRequest request : requests) {
			if (request.isDead()) {
				continue;
			}

			if (! request.isChargeable()) {
				continue;
			}

			if (! request.isOneToThree()) {
				continue;
			}

			days += request.getDaysTaken();
		}

		return days;
	}

	@XmlElement(name = "oneToThreeRemaining")
	public Integer getOneToThreeRemaining() {
		try {
			return getGrantedHistory().getTotalAvailableOneToThree() - getOneToThreeRequested();
		} catch (NullPointerException npe) {
			return 0;
		}
	}

	@XmlElement(name = "bonusOneToThree")
	public Integer getBonusOneToThree() {
		return null;
	}

	@XmlElement(name = "unpaidTimeTaken")
	public Double getUnpaidTimeTaken() {
		double days = 0;

		for (PtoRequest request : requests) {
			if (request.isDead()) {
				continue;
			}

			if (! request.isUnpaid()) {
				continue;
			}

			if (request.isOneToThree()) {
				continue;
			}

			days += request.getDaysTaken();
		}

		return days;
	}

	@XmlElement(name = "unpaidOneToThreeTaken")
	public Integer getUnpaidOneToThreeTaken() {
		int days = 0;

		for (PtoRequest request : requests) {
			if (request.isDead()) {
				continue;
			}

			if (! request.isUnpaid()) {
				continue;
			}

			if (! request.isOneToThree()) {
				continue;
			}

			days += request.getDaysTaken();
		}

		return days;
	}

	@XmlElement(name = "nonChargeableTaken")
	public Double getNonChargeableTaken() {
		double days = 0;

		for (PtoRequest request : requests) {
			if (request.isDead()) {
				continue;
			}

			if (request.isChargeable()) {
				continue;
			}

			if (request.isOneToThree()) {
				continue;
			}

			days += request.getDaysTaken();
		}

		return days;
	}

	@XmlElement(name = "unpaidOneToThreeTaken")
	public Integer getUnpaidOneToThreeTakenTaken() {
		int days = 0;

		for (PtoRequest request : requests) {
			if (request.isDead()) {
				continue;
			}

			if (! request.isUnpaid()) {
				continue;
			}

			if (! request.isOneToThree()) {
				continue;
			}

			days += request.getDaysTaken();
		}

		return days;
	}
}