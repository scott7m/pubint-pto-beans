package com.pubint.pto.entityList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.pubint.pto.entity.PtoTimeType;

@XmlRootElement(name = "timeTypes")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PtoTimeTypeList implements Serializable {
	private static final long serialVersionUID = 1L;

	List<PtoTimeType> timeTypes;

	public PtoTimeTypeList() {
		super();

		timeTypes = new ArrayList<PtoTimeType>();
	}

	public PtoTimeTypeList(List<PtoTimeType> list) {
		this();

		for (PtoTimeType timeType : list) {
			addTimeType(timeType);
		}
	}

	@XmlElement(name = "timeType")
	public List<PtoTimeType> getTimeTypes() {
		return timeTypes;
	}

	public void setTimeTypes(List<PtoTimeType> timeTypes) {
		this.timeTypes = timeTypes;
	}

	public void addTimeType(PtoTimeType timeType) {
		getTimeTypes().add(timeType);
	}
}
