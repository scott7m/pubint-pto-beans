package com.pubint.pto.exceptions;

public class InvitationException extends Exception {
	private static final long serialVersionUID = 1L;

	String message;

	public InvitationException() {
		super("Unable to send approved PTO invitations");
	}

	public InvitationException(String message) {
		super(message);
	}

	public InvitationException(String message, Throwable parent) {
		super(message, parent);
	}

	@Override
	public void printStackTrace() {
		System.out.println(getMessage());

		super.printStackTrace();
	}
}
