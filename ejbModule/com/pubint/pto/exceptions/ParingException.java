package com.pubint.pto.exceptions;

public class ParingException extends Exception {
	private static final long serialVersionUID = 1L;

	String message;

	public ParingException() {
		super("Unable to pare down data");
	}

	public ParingException(String message) {
		super(message);
	}

	public ParingException(String message, Throwable parent) {
		super(message, parent);
	}

	@Override
	public void printStackTrace() {
		System.out.println(getMessage());

		super.printStackTrace();
	}
}