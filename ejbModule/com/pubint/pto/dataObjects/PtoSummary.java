package com.pubint.pto.dataObjects;

import com.pubint.pto.entity.PtoEmployee;

public class PtoSummary {
	private PtoEmployee employee;

	private Double unpaidTimeTaken;
	private Integer unpaidOneToThreeTaken;

	private Double compTimeGranted;
	private Double compTimeTaken;
	private Double compTimeRemaining;

	private Double nonChargeableTaken;

	private Double daysRequested;

	private Integer oneToThreeTaken;

	public PtoEmployee getEmployee() {
		return employee;
	}

	public void setEmployee(PtoEmployee employee) {
		this.employee = employee;
	}

	public Double getUnpaidTimeTaken() {
		if (unpaidTimeTaken == null) {
			return 0.0;
		}

		return unpaidTimeTaken;
	}

	public void setUnpaidTimeTaken(Double unpaidTimeTaken) {
		this.unpaidTimeTaken = unpaidTimeTaken;
	}

	public Integer getUnpaidOneToThreeTaken() {
		if (unpaidOneToThreeTaken == null) {
			return 0;
		}

		return unpaidOneToThreeTaken;
	}

	public void setUnpaidOneToThreeTaken(Integer unpaidOneTo3Taken) {
		this.unpaidOneToThreeTaken = unpaidOneTo3Taken;
	}

	public Double getCompTimeGranted() {
		if (compTimeGranted == null) {
			return 0.0;
		}

		return compTimeGranted;
	}

	public void setCompTimeGranted(Double compTimeGranted) {
		this.compTimeGranted = compTimeGranted;
	}

	public Double getCompTimeTaken() {
		if (compTimeTaken == null) {
			return 0.0;
		}

		return compTimeTaken;
	}

	public void setCompTimeTaken(Double compTimeTaken) {
		this.compTimeTaken = compTimeTaken;
	}

	public Double getCompTimeRemaining() {
		if (compTimeRemaining == null) {
			return 0.0;
		}

		return compTimeRemaining;
	}

	public void setCompTimeRemaining(Double compTimeRemaining) {
		this.compTimeRemaining = compTimeRemaining;
	}

	public Double getNonChargeableTaken() {
		if (nonChargeableTaken == null) {
			return 0.0;
		}

		return nonChargeableTaken;
	}

	public void setNonChargeableTaken(Double nonChargeableTaken) {
		this.nonChargeableTaken = nonChargeableTaken;
	}

	public Double getStartingDaysPTO() {
		if (employee == null) {
			return 0.0;
		}

		if (employee.getGrantedHistory() == null) {
			return 0.0;
		}

		return employee.getGrantedHistory().getTotalAvailablePTO();
	}

	public Double getDaysRequested() {
		if (daysRequested == null) {
			return 0.0;
		}

		return daysRequested;
	}

	public void setDaysRequested(Double daysRequested) {
		this.daysRequested = daysRequested;
	}

	public Double getDaysRemaining() {
		return getStartingDaysPTO() - daysRequested;
	}

	public Integer getOneToThreeTaken() {
		if (oneToThreeTaken == null) {
			return 0;
		}

		return oneToThreeTaken;
	}

	public void setOneToThreeTaken(Integer oneTo3Taken) {
		this.oneToThreeTaken = oneTo3Taken;
	}

	public Integer getOneToThreeGranted() {
		if (employee == null) {
			return 0;
		}

		if (employee.getGrantedHistory() == null) {
			return 0;
		}

		return employee.getGrantedHistory().getTotalAvailableOneToThree();
	}

	public Integer getOneTo3Remaining() {
		return getOneToThreeGranted() - getOneToThreeTaken();
	}
}
