package com.pubint.pto.dataObjects;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.pubint.pto.entity.reporting.Request;

@XmlRootElement(name = "attendanceRange")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class AttendanceRange {
	private static Logger log = Logger.getLogger("AttendanceRange");

	private List<AttendanceSingle> dates;

	public AttendanceRange() {
		super();

		dates = new ArrayList<AttendanceSingle>();

		log.setLevel(Level.TRACE);
	}

	public AttendanceRange(List<AttendanceSingle> dates) {
		this();

		for (AttendanceSingle date : dates) {
			this.dates.add(date);
		}
	}

	public List<AttendanceSingle> getDates() {
		return dates;
	}

	public void setDates(List<AttendanceSingle> days) {
		dates = days;
	}

	public void addDate(AttendanceSingle day) {
		AttendanceSingle work = null;

		log.trace("Check to see if date is already in the list...");

		if (! dates.isEmpty()) {
			for (AttendanceSingle test : dates) {
				log.trace("Testing: " + test.getDate());

				if (test.getDate().equals(day.getDate())) {
					log.trace("Found it!");

					work = test;

					break;
				}
			}
		} else {
			log.trace("No dates yet...");
		}

		if (work != null) {
			log.trace("Remove the original record from the list...");

			dates.remove(work);

			log.trace("Add in the additional new request information...");

			for (Request request : day.getRequests()) {
				work.addRequest(request);
			}
		} else {
			log.trace("New date...Prepare to insert into the list...");

			work = day;
		}

		log.trace("Insert updated record into list...");

		dates.add(work);
	}
}
