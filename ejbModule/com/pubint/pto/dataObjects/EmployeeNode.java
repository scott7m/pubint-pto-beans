package com.pubint.pto.dataObjects;

import java.util.ArrayList;
import java.util.List;

import com.pubint.pto.entity.PtoEmployee;

public class EmployeeNode {
	private PtoEmployee employee = null;

	private List<EmployeeNode> reports = new ArrayList<EmployeeNode>();

	public EmployeeNode() {
		super();
	}

	public EmployeeNode(PtoEmployee employee) {
		this();

		this.employee = employee;
	}

	public PtoEmployee getEmployee() {
		return employee;
	}

	public void setEmployee(PtoEmployee employee) {
		this.employee = employee;
	}

	public List<EmployeeNode> getReports() {
		return reports;
	}

	public void setReports(List<EmployeeNode> reports) {
		this.reports = reports;
	}

	public void addReport(EmployeeNode employee) {
		if (! reports.contains(employee)) {
			reports.add(employee);
		}
	}
}
