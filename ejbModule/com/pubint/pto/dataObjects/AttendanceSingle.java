package com.pubint.pto.dataObjects;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.pubint.pto.base.XmlDateAdapter;
import com.pubint.pto.entity.PtoRequest;
import com.pubint.pto.entity.reporting.Request;

@XmlRootElement(name = "attendanceDate")
@XmlType(propOrder = {})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class AttendanceSingle {
	private Date date;

	private List<Request> requests;

	public AttendanceSingle() {
		super();

		requests = new ArrayList<Request>();
	}

	public AttendanceSingle(Date date) {
		this();

		this.date = date;
	}

	@XmlJavaTypeAdapter(XmlDateAdapter.class)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@XmlElementWrapper(name = "requests")
	@XmlElement(name = "request")
	public List<Request> getRequests() {
		return requests;
	}

	public void setRequests(List<Request> requests) {
		this.requests = requests;
	}

	public void addRequest(PtoRequest request) {
		Request checkRequest = new Request(request);

		if (requests.contains(checkRequest)) {
			return;
		}

		requests.add(checkRequest);
	}

	public void addRequest(Request request) {
		if (requests.contains(request)) {
			return;
		}

		requests.add(request);
	}
}
