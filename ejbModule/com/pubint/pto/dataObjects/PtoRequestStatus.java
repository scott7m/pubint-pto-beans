package com.pubint.pto.dataObjects;

public class PtoRequestStatus {
	public static final int NOT_REQUESTED = 0;
	public static final int PENDING = 1;
	public static final int APPROVED = 2;
	public static final int REJECTED = 3;
	public static final int CANCELED = 4;
	
	private static String[] REQUEST_STATUS = {
	    "Not Requested",
		"Pending",
		"Approved",
		"Rejected",
		"Canceled"
		};
	
	public static String getRequestStatus(int rStatus) {
		String status = "";

		try {
			status = REQUEST_STATUS[rStatus];
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}
	
	public static String getSelectList() {
		StringBuilder retVal = new StringBuilder();
		
		retVal.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><requestStatuses>");
		
		int count = 0;
		
		for (String reqStat : REQUEST_STATUS) {
			retVal.append("<requestStatus id=\"" + count + "\"><description>" + reqStat + "</description></requestStatus>");
			count++;
		}
		
		retVal.append("</requestStatuses>");
		
		return retVal.toString();
	}
}
