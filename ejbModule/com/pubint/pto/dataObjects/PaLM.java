package com.pubint.pto.dataObjects;

import java.util.List;

public class PaLM {
	// actually only 5 seconds
	public static final long TIMER_INTERVAL = 1000 * 5 * 1;

	public static final long ERROR = 0;
	public static final long WARN = 10;
	public static final long INFO = 20;
	public static final long TRACE = 30;

	public static final long DEBUG_LEVEL = INFO;

	public static final long SYSTEM_USER_ID = - 10;

	public static final long QUOTE_COMPONENT_TYPE = - 4;

	public static final long STOCK_COMPONENT_TYPE = - 2;
	public static final long STOCK_VENDOR_NAME = 115;
	public static final long STOCK_UNIT_PRICE = 159;
	public static final long STOCK_MOQ = 117;
	public static final long STOCK_PIL_ITEM = 273;
	public static final long STOCK_JOB_NUMBER = 304;
	public static final long STOCK_COST_CODE = 305;

	public static final long STICKER_CLASS = 7;
	public static final long INSTRUCTION_CLASS = 14;

	public static final long UNKNOWN_ORDER = 0;
	public static final long FINAL_ORDER = 1;
	public static final long MODULE_ORDER = 2;
	public static final long CHIP_ORDER = 3;
	public static final long OTHER_ORDER = 4;

	public static final long PRINTER_VENDOR = - 10;
	public static final long MODULE_VENDOR = - 11;
	public static final long CHIP_VENDOR = - 12;
	public static final long OTHER_VENDOR = - 13;
	public static final long PRINTER_VENDOR_DEPARTMENT = 20;
	public static final long MODULE_VENDOR_DEPARTMENT = 21;
	public static final long CHIP_VENDOR_DEPARTMENT = 22;
	public static final long COMPONENT_VENDOR_DEPARTMENT = 19;

	// final order
	public static final long HAS_TEXT = 1;

	// module order
	public static final long HAS_MODULE1 = 8;
	public static final long HAS_MODULE2 = 33;
	public static final long HAS_MODULE3 = 34;
	public static final long HAS_MODULE4 = 35;
	public static final long HAS_MODULE5 = 36;
	public static final long HAS_MODULE6 = 37;

	public static final long MODULE_TITLE_CODE_ATTRIBUTE = 47;
	public static final long MODULE_IC_CODE_ATTRIBUTE = 180;

	// chip order
	public static final long HAS_CHIP = 9;

	public static final long CHIP_TITLE_CODE_ATTRIBUTE = 174;

	// other order
	public static final long HAS_STOCK = - 2;

	// default oldest po date range
	public static final long DEFAULT_OLDEST_PO_DATE_FINAL = 180;
	public static final long DEFAULT_OLDEST_PO_DATE_MODULE = 28;
	public static final long DEFAULT_OLDEST_PO_DATE_CHIP = 28;
	public static final long DEFAULT_OLDEST_PO_DATE_OTHER = 28;

	public static final long DEFAULT_OLDEST_COMPONENT_SHIPMENT = 120;

	public static final int TA_ORDER_STATUS_REPORT_DAYS = 30;

	// only allow the extended background processing to occur before...
	public static final long EXTENDED_BACKGROUND_PROCESSING_CUTOFF = 3;

	// notification groups
	public static final long NOTIFY_REVISED_COST = 33;

	public static final long PURCHASE_ORDER_SIGNERS = 24;

	public static final long TA_NOTIFY_INTERNATIONAL_DEPARTMENT = 40;
	public static final long TA_NOTIFY_COOKBOOK_DEPARTMENT = 37;
	public static final long TA_NOTIFY_CHILDRENS_DEPARTMENT = 41;
	public static final long TA_NOTIFY_AUTO_GENERAL_DEPARTMENT = 38;
	public static final long TA_NOTIFY_STATIONARY_DEPARTMENT = 39;

	public static final long TA_NOTIFY_PUBLISHING_OPERATIONS = 53;
	public static final long INCOMPLETE_VENDOR_SETUP_NOTIFICATION = 59;
	public static final long SCHEDULING_OVERDUE_GROUP = 52;

	public static final long PO_LISTING_NOTIFICATION = - 102;

	public static final long PO_CANCELLATION_NOTICE = 60;
	public static final long DEMAND_ADMINISTRATORS = 45;
	public static final long TA_CANCELLATION_NOTICE = 61;

	public static final long NOTIFY_ACCOUNTING_OF_PO = - 11;

	// messageBean constants
	public static final List<String> NO_ATTACHMENTS = null;
	public static final String NO_LINK = null;

	// projectBean constants
	public static final long PROJECT_ADMINISTRATORS_DEPARTMENT_ID = 25;
	public static final long PROJECT_TEAM_ID = 31;
	public static final long PROJECT_TEAM_ROLE = 335;
	public static final long PROJECT_TEAM_MEMBER = 336;
	public static final long PROJECT_TASK = 338;
	public static final long PROJECT_TASK_RATE = 339;
	public static final long PROJECT_BUDGET_COMPONENT = 66;

}