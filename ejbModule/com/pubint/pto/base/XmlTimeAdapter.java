package com.pubint.pto.base;

import java.sql.Time;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class XmlTimeAdapter extends XmlAdapter<String, Time> {
	@Override
	public String marshal(Time time) throws Exception {
		try {
			return time.toString();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Time unmarshal(String time) throws Exception {
		try {
			return Time.valueOf(time);
		} catch (Exception e) {
			return null;
		}
	}
}