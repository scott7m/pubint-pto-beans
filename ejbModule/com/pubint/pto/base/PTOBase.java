package com.pubint.pto.base;

import java.io.Serializable;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Element;

public class PTOBase implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String CELL_HEADER = "header";
	public static final String CELL_HEADER_LOCKED = "headerLocked";
	public static final String CELL_NORMAL = "normal";
	public static final String CELL_DATE = "normalDate";
	public static final String CELL_NORMAL_LOCKED = "normalLocked";
	public static final String CELL_NORMAL_DATE_LOCKED = "normalDateLocked";

	public String[] splitID(String var) {
		return (notEmptyString(var) ? var.split("\\" + ".") : null);
	}

	public Object getBean(String beanName) {
		Object bean = null;

		try {
			InitialContext initialContext;

			Properties properties = new Properties();
			properties.setProperty(Context.INITIAL_CONTEXT_FACTORY,
				"org.apache.openejb.client.LocalInitialContextFactory");

			initialContext = new InitialContext(properties);

			bean = initialContext.lookup(beanName + "Local");
		} catch (javax.naming.NamingException ne) {
			System.out.println("The lookup of " + beanName + " failed!");

			ne.printStackTrace();
		}

		return bean;
	}

	public String rjlzf(String data, int length) {
		// only processes number strings
		// non-numeric data is returned unchanged
		String retVal;

		if (length == 0) {
			return null;
		}

		if (! isEmptyString(data)) {
			retVal = data;
		} else {
			retVal = "";
		}

		if (data.trim().length() < length) {
			try {
				@SuppressWarnings("unused") long numericData = Long.parseLong(data);

				retVal = data.trim();

				while (retVal.length() < length) {
					retVal = "0" + retVal;
				}
			} catch (Exception ex) {

			}
		}

		return retVal;
	}

	public String fnPack(java.util.Date date) {
		return fnPack(new Date(date.getTime()));
	}

	public String fnPack(Date date) {
		String result = "";

		if (date != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);

			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH) + 1;
			int day = cal.get(Calendar.DAY_OF_MONTH);

			int one = (year / 64);

			int two = (year - (one * 64));

			result += (char) (one + 32);
			result += (char) (two + 32);
			result += (month < 10 ? "0" + month : "" + month);
			result += (day < 10 ? "0" + day : "" + day);
		} else {
			result = "      ";
		}

		return result;
	}

	public String pad(String s, int length) {
		if (length <= 0) {
			return null;
		}

		return pad(s, length, ' ').substring(0, length);
	}

	public String pad(String s, int length, char padChar) {
		if (length <= 0) {
			return null;
		}

		if (isEmptyString(s)) {
			s = "";
		}

		String result = s;

		if (length > s.length()) {
			for (int loop = 0; loop < (length - s.length()); loop++) {
				result += padChar;
			}
		} else {
			result = result.substring(0, length);
		}

		return result;
	}

	public java.sql.Date now() {
		return new java.sql.Date((new java.util.Date()).getTime());
	}

	public java.sql.Date getTodaySQL() {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		java.util.Date date = null;

		try {
			date = df.parse(df.format(Calendar.getInstance().getTime()));
		} catch (ParseException e) {
			e.printStackTrace();

			return null;
		}

		return new java.sql.Date(date.getTime());
	}

	public String currentDateTime() {
		SimpleDateFormat sdfDate = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss.SSS");// dd/MM/yyyy

		java.util.Date now = new java.util.Date();

		String strDate = sdfDate.format(now);

		return strDate;
	}

	public String toEscaped(String replaceString) {
		if (isEmptyString(replaceString)) {
			return "";
		}

		String work = replaceString.replaceAll("\\\\", "\\\\\\\\");

		work = work.replaceAll("'", "\\\\'");
		work = work.replaceAll("\"", "\\\\\"");

		return work;
	}

	public String fixXML(String xml) {
		return xml.replace("&", "&amp;");
	}

	public String fixXML(StringWriter xml) {
		return xml.toString().replace("&", "&amp;");
	}

	public String fixXML(StringBuilder xml) {
		return xml.toString().replace("&", "&amp;");
	}

	public String toNumericEscaped(String replaceString) {
		if (isEmptyString(replaceString)) {
			return "";
		}

		String work = "";

		for (char x : replaceString.toCharArray()) {
			if (".0123456789-".indexOf(x) != - 1) {
				work += x;
			}
		}

		return work;
	}

	public long getLong(Element node, String elementName, XPath xpath) throws NumberFormatException {
		try {
			String attribute = toNumericEscaped(xpath.evaluate("//" + elementName, node)).trim();

			// if the value is sent, but blank
			// return 0;
			if (attribute.equals("")) {
				return 0;
			}

			return Long.parseLong(attribute);
		} catch (XPathExpressionException e) {
			throw new NumberFormatException("The requested value is not present in the command: " + elementName);
		}
	}

	public int getInt(Element node, String elementName, XPath xpath) throws NumberFormatException {
		try {
			String attribute = toNumericEscaped(xpath.evaluate("//" + elementName, node)).trim();

			// if the value is sent, but blank
			// return 0;
			if (attribute.equals("")) {
				return 0;
			}

			return Integer.parseInt(attribute);
		} catch (XPathExpressionException e) {
			throw new NumberFormatException("The requested value is not present in the command: " + elementName);
		}
	}

	public String getString(Element node, String elementName, XPath xpath) {
		String attribute = "";

		try {
			String raw = xpath.evaluate("//" + elementName, node);
			String data = "";

			if (raw != null) {
				int plusPos = raw.indexOf("+");

				while (plusPos >= 0) {
					raw = "" +
						raw.substring(0, plusPos) + "%2B" +
						raw.substring(plusPos + 1);

					plusPos = raw.indexOf("+");
				}

				int slashPos = raw.indexOf("~1~");

				while (slashPos >= 0) {
					raw = raw.substring(0, slashPos) + "\\"
						+ raw.substring(slashPos + 3);
					slashPos = raw.indexOf("~1~");
				}

				int pctPos = raw.indexOf("~2~");

				while (pctPos >= 0) {
					raw = raw.substring(0, pctPos) + "%25"
						+ raw.substring(pctPos + 3);
					pctPos = raw.indexOf("~2~");
				}

				int aposPos = raw.indexOf("~3~");

				while (aposPos >= 0) {
					raw = raw.substring(0, aposPos) + "\'"
						+ raw.substring(aposPos + 3);
					aposPos = raw.indexOf("~3~");
				}

				int rposPos = raw.indexOf("~4~");

				while (rposPos >= 0) {
					raw = raw.substring(0, rposPos) + "\n"
						+ raw.substring(rposPos + 3);
					rposPos = raw.indexOf("~4~");
				}
			}

			try {
				data = URLDecoder.decode(raw, "UTF-8");
			} catch (Exception e) {

			}

			attribute = data;
		} catch (Exception e) {

		}

		attribute = attribute.trim();

		return attribute;
	}

	public double getReal(Element node, String elementName, XPath xpath) {
		try {
			String attribute = xpath.evaluate("//" + elementName, node);
			String work = toNumericEscaped(attribute);

			return Double.parseDouble(work);
		} catch (Exception e) {
			return 0;
		}
	}

	public boolean getBoolean(Element node, String elementName, XPath xpath) {
		try {
			String attribute = xpath.evaluate("//" + elementName, node);

			if (("true".equalsIgnoreCase(attribute))
				|| ("yes".equalsIgnoreCase(attribute))) {
				return true;
			}

			return false;
		} catch (Exception e) {
			return false;
		}
	}

	public Date getDate(Element element, String attribute, XPath xpath) {
		return toDate(getString(element, attribute, xpath));
	}

	public Object getObject(Element element, String attribute, XPath xpath) {
		Object obj = null;

		try {
			obj = xpath.evaluate("//" + attribute, element);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		return obj;
	}

	public long toLong(String stringLong) {
		try {
			String work = toNumericEscaped(stringLong);

			return (isEmptyString(work) ? 0 : Long.parseLong(work));
		} catch (NumberFormatException nfex) {
			return 0;
		}
	}

	public int toInt(String stringInt) {
		try {
			String work = toNumericEscaped(stringInt);

			return (isEmptyString(work) ? 0 : Integer.parseInt(work));
		} catch (NumberFormatException nfex) {
			return 0;
		}
	}

	public double toDouble(String stringDouble) {
		try {
			String work = toNumericEscaped(stringDouble);

			return (isEmptyString(work) ? 0 : Double.parseDouble(work));
		} catch (NumberFormatException nfex) {
			return 0;
		}
	}

	public String toString(String someString, String defaultValue) {
		return (someString == null) ? defaultValue.trim() : someString.trim();
	}

	public String toString(String someString) {
		return toString(someString, "");
	}

	public Date toDate(String date) {
		String dateFormat = "MM/dd/yy";

		if (date.indexOf("-") >= 0) {
			dateFormat = "yyyy-MM-dd";
		}

		try {
			SimpleDateFormat format = new SimpleDateFormat(dateFormat);

			return new Date((format.parse(date)).getTime());
		} catch (Exception e) {
			return null;
		}
	}

	public boolean notEmptyString(String string) {
		return (! isEmptyString(string));
	}

	public boolean isEmptyString(String string) {
		return ((string == null) || "".equals(string.trim()));
	}

	public static long daysBetween(Calendar startDate, Calendar endDate) {
		Calendar date = (Calendar) startDate.clone();
		long daysBetween = 0;

		while (date.before(endDate)) {
			date.add(Calendar.DAY_OF_MONTH, 1);
			daysBetween++;
		}

		return daysBetween;
	}

	// "##:## ?M" format
	public Time stringToTime(String t) {
		int hour = 0;
		int min = 0;

		String[] timeSplit = t.split(":");

		hour = Integer.parseInt(timeSplit[0]);

		String[] minSplit = timeSplit[1].split(" ");

		min = Integer.parseInt(minSplit[0]);

		if ((hour < 12) && (minSplit[1].equals("PM"))) {
			hour += 12;
		} else if ((hour == 12) && (minSplit[1].equals("AM"))) {
			hour = 0;
		}

		return Time.valueOf(hour + ":" + min + ":" + 0);
	}
}
