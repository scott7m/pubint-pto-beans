package com.pubint.pto.base;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class XmlDateAdapter extends XmlAdapter<String, Date> {
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public String marshal(Date date) throws Exception {
		String result = "";

		try {
			result = df.format(date);
		} catch (Exception e) {

		}

		return result;
	}

	@Override
	public Date unmarshal(String string) throws Exception {
		Date result = new java.sql.Date((new java.util.Date()).getTime());

		try {
			result = new java.sql.Date(df.parse(string).getTime());
		} catch (Exception e) {

		}

		return result;
	}
}