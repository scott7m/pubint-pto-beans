package com.pubint.pto.base;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.sql.Date;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PTOBaseTest {
	final PTOBase base = new PTOBase();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSplitID() {
		assertTrue(base.splitID(null) == null);
		assertTrue(base.splitID("") == null);

		assertTrue(base.splitID("1.2.3")[0].equals("1"));
		assertTrue(base.splitID("1.2.3")[1].equals("2"));
		assertTrue(base.splitID("1.2.3")[2].equals("3"));
	}

	@Test
	public void testRjlzf() {
		// length of zero forces return of null
		assertTrue(base.rjlzf(null, 0) == null);
		assertTrue(base.rjlzf("", 0) == null);
		assertTrue(base.rjlzf("This will get changed into null", 0) == null);

		// non-numeric data is returned unchanged
		// (so long as length is non-zero)
		assertTrue(base.rjlzf("abcd", 1).equals("abcd"));
		assertTrue(base.rjlzf("a1b2c3", 1).equals("a1b2c3"));

		// test real function
		// right-justify and left zero fill
		assertTrue(base.rjlzf("1", 1).equals("1"));
		assertTrue(base.rjlzf("1", 10).equals("0000000001"));
	}

	@Test
	public void testPadStringInt() {
		// pad to length with space
		// truncate if longer than length
		// zero length returns null

		assertEquals("This should return a null value.", null, base.pad(null, 0));
		assertEquals("This should return a null value.", null, base.pad("Not empty", 0));

		assertEquals("This should return a space.", " ", base.pad(null, 1));
		assertEquals("This should return a space.", " ", base.pad("", 1));
		
		assertEquals("This should return a string 'Not'.", "Not", base.pad("Not empty", 3));
		assertEquals("This should return a string 'Not empty '.", "Not empty ", base.pad("Not empty", 10));
	}

	@Test
	public void testPadStringIntChar() {
		// pad to length with specified char
		// truncate if longer than length
		// zero length returns null

		assertEquals("This should return a null value.", null, base.pad(null, 0, '*'));
		assertEquals("This should return a null value.", null, base.pad("Not empty", 0, '*'));

		assertEquals("This should return an asterix.", "*", base.pad(null, 1, '*'));
		assertEquals("This should return an asterix.", "*", base.pad("", 1, '*'));

		assertEquals("This should return 'Not'.", "Not", base.pad("Not empty", 3, '*'));
		assertEquals("This should return 'Not empty*'.", "Not empty*", base.pad("Not empty", 10, '*'));
		
		assertEquals("This should return a string 'Not'.", "Not", base.pad("Not empty", 3, '*'));
		assertEquals("This should return a string 'Not empty*'.", "Not empty*", base.pad("Not empty", 10, '*'));
	}

	@Test
	public void testToEscaped() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testFixXMLString() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testFixXMLStringWriter() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testFixXMLStringBuilder() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testToNumericEscaped() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetLong() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetInt() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetString() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetReal() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetBoolean() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetDate() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testToLong() {
		assertEquals("Test should return 123456789.", 123456789, base.toLong("123456789"));
	}

	@Test
	public void testToInt() {
		assertEquals("Test should return 10", 10, base.toInt("10"));
	}

	@Test
	public void testToDouble() {
		assertEquals("Test should return 142.7", 142.7, base.toDouble("142.7"), .1);
	}

	@Test
	public void testToStringStringString() {
		assertEquals("Test should return '*'.", "*", base.toString(null, "*"));
	}

	@Test
	public void testToStringString() {
		assertEquals("Test should return String '12345'.", "12345", base.toString("12345"));
	}

	@Test
	public void testToDate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		try {
			Date testDate = new Date((format.parse("2013-07-01")).getTime());
			assertEquals("Date should be the same despite different format.", testDate, base.toDate("07/01/2013"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testNotEmptyString() {
		assertTrue("Non-Empty String should return true.", base.notEmptyString("Not empty"));

		assertFalse("Empty String should return false.", base.notEmptyString("       "));
		assertFalse("Empty String should return false.", base.notEmptyString(""));
		assertFalse("Empty String should return false.", base.notEmptyString(null));
	}

	@Test
	public void testIsEmptyString() {
		assertTrue("Empty String should return true.", base.isEmptyString(null));
		assertTrue("Empty String should return true.", base.isEmptyString(""));
		assertTrue("Empty String should return true.", base.isEmptyString("           "));

		assertFalse("Non-Empty String should return false.", base.isEmptyString("Not empty"));
	}

	@Test
	public void testDaysBetween() {
		Calendar startCal = Calendar.getInstance();
		Calendar endCal = Calendar.getInstance();
		
		startCal.set(Calendar.YEAR, 2012);
		startCal.set(Calendar.MONTH, 0);
		startCal.set(Calendar.DATE, 15);
		
		endCal.set(Calendar.YEAR, 2012);
		endCal.set(Calendar.MONTH, 0);
		endCal.set(Calendar.DATE, 19);
		
		assertEquals("Should return 4 days between.", 4, PTOBase.daysBetween(startCal, endCal));
	}

	@Test
	public void testCreateStyles() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testStringToTime() {
		Time testTime = Time.valueOf("12:00:00");
		
		assertEquals("True if return time is noon.", testTime, base.stringToTime("12:00 PM"));
	}
}
